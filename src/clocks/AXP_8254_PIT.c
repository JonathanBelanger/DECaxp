/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 31-Oct-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include "clocks/AXP_8254_PIT.h"
#include "clocks/AXP_8259A_PICDefs.h"

/*
 * Counter and Control Structures
 */
static AXP_8254_Counter counter[AXP_8254_NUM_COUNTERS];
static bool initialized = false;
static bool thread_running;
static pthread_t pitThreadID;

/*
 * Forward function definitions.
 */
static void
AXP_8254_Decrement(AXP_8254_Counter *counter);

static void
*AXP_8254_Main(void);

/*
 * AXP_8254_Initialize
 *  Initialize the control and counter register.
 *
 * Input Parameters:
 *  None.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  true:   Normal successful completion.
 *  false:  Failed to completely initialize the chip.
 */
void
AXP_8254_Initialize(void)
{
    int ii;

    /*
     * We cannot allow this function to be called more than once.
     */
    if (initialized == true)
    {
        return;
    }

    for (ii = 0; ii < AXP_8254_NUM_COUNTERS; ii++)
    {
        counter[ii].count_register.value = 0;
        counter[ii].count_register.lsb_done = false;
        counter[ii].count_register.read_pending = false;
        counter[ii].output_latch.value = 0;
        counter[ii].output_latch.lsb_done = false;
        counter[ii].output_latch.read_pending = false;
        counter[ii].status.byte = 0;
        counter[ii].status.null_count = 1;
        counter[ii].counter = 0;
        counter[ii].saved_msb = 0;
        counter[ii].status_latch = 0;
        counter[ii].sl_read_pending = false;
        counter[ii].lsb_done = false;
    }
    thread_running = false;
    initialized = true;

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_8254_Write
 *  This function is called to perform a write to one of the registers on an Intel 8254 Programmable Interval Timer
 *  chip.  This write operation may cause the thread to update this counter, as well as the individual counter, status,
 *  and latches.
 *
 * Input Parameters:
 *  index:
 *      An integer representing the register this write is to be accessing.
 *  data:
 *      A byte containing the data to be written.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void
AXP_8254_Write(u32 index, u8 data)
{
    bool start_thread = false;

    /*
     * We cannot allow this function to be called until everything has been initialized.
     */
    if (initialized == false)
    {
        return;
    }

    if (index == 3)     /* Control Word Register */
    {
        AXP_8254_Control_Word cw = {.cw = data};
        u32 reg = cw.select_counter;

        /* Read-Back Command */
        if (reg == 3)
        {
            if (!cw.not_count)
            {
                if (cw.cnt0)
                {
                    counter[0].output_latch.value = counter[0].counter;
                    counter[0].output_latch.read_pending = true;
                }
                if (cw.cnt1)
                {
                    counter[1].output_latch.value = counter[1].counter;
                    counter[1].output_latch.read_pending = true;
                }
                if (cw.cnt2)
                {
                    counter[2].output_latch.value = counter[2].counter;
                    counter[2].output_latch.read_pending = true;
                }
            }
            if (!cw.not_status)
            {
                if (cw.cnt0)
                {
                    counter[0].status_latch = counter[0].status.byte;
                    counter[0].sl_read_pending = true;
                }
                if (cw.cnt1)
                {
                    counter[1].status_latch = counter[1].status.byte;
                    counter[1].sl_read_pending = true;
                }
                if (cw.cnt2)
                {
                    counter[2].status_latch = counter[2].status.byte;
                    counter[2].sl_read_pending = true;
                }
            }
        }

        /* Counter Latch Command */
        else if (cw.read_write == 0)
        {
            counter[reg].output_latch.value = counter[reg].counter;
            counter[reg].output_latch.read_pending = true;
        }

        /* Set the Counter's Status Register */
        else
        {
            counter[reg].status.byte = cw.cw & 0x3f;
            counter[reg].status.null_count = 1;
            counter[reg].sl_read_pending = false;

            /*
             * Determine the initial value of OUT (low or high)
             */
            switch(counter[reg].status.mode)
            {

                /*
                 * For Mode 0, after the control word is written, OUT is initially low, and will remain low until the
                 * counter reaches zero.  OUT will remain high until a new count or a new Mode 0 control word is written
                 * into the counter.
                 */
                case 0:
                    counter[reg].status.output = 0;
                    break;

                /*
                 * For Mode 1, OUT will be initially be high.  OUT will go low upon the trigger to begin the one-shot
                 * pulse and will remain low until the counter reaches zero.  OUT will then go high and remain high
                 * until the next trigger.
                 *
                 * For Mode 2 (6), OUT will initially be high.  Out will go low when the counter reaches one and will go
                 * back to high when the counter reaches zero.
                 *
                 * For Mode 3 (7), OUT will initially be high.  OUT will remain high until half the initial counter is
                 * reached, where it goes low until the count reaches zero.
                 */
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    counter[reg].status.output = 1;
                    break;
            }
        }
    }

    /* One of the 3 possible counters */
    else if (index < 2)
    {
        switch (counter[index].status.read_write)
        {
            /* Not a valid write mode */
            case 0:
                break;

            /* Write least significant byte only */
            case 1:
                counter[index].count_register.lsb = data;
                counter[index].count_register.msb = 0;
                counter[index].counter = counter[index].count_register.value;
                counter[index].sl_read_pending = false;
                counter[index].lsb_done = true;
                counter[index].output_latch.read_pending = false;
                counter[index].status.null_count = 0;
                start_thread = !thread_running;
                break;

            /* Write most significant byte only */
            case 2:
                counter[index].count_register.lsb = 0;
                counter[index].count_register.msb = data;
                counter[index].counter = counter[index].count_register.value;
                counter[index].sl_read_pending = false;
                counter[index].lsb_done = true;
                counter[index].output_latch.read_pending = false;
                counter[index].status.null_count = 0;
                start_thread = !thread_running;
                break;

            /* Write least significant byte followed by most significant byte */
            case 3:
                if (counter[index].count_register.lsb_done)
                {
                    counter[index].count_register.msb = data;
                    counter[index].count_register.lsb_done = false;
                    counter[index].output_latch.read_pending = false;
                    counter[index].counter = counter[index].count_register.value;
                    counter[index].sl_read_pending = false;
                    counter[index].lsb_done = true;
                    counter[index].status.null_count = 0;
                    if (counter[index].status.mode == 1)
                    {
                        counter[index].status.output = 0;
                    }
                    start_thread = !thread_running;
                }
                else
                {
                    counter[index].count_register.lsb = data;
                    counter[index].count_register.lsb_done = true;
                    counter[index].status.null_count = 1;
                    if (counter[index].status.mode == 0)
                    {
                        counter[index].status.output = 0;
                    }
                }
                break;
        }
    }

    /*
     * If we are in a position to start the PIT thread, then do so now.
     */
    if (start_thread)
    {
        if (pthread_create(&pitThreadID, NULL, &AXP_8254_Main, NULL) == 0)
        {
            thread_running = true;
        }
        else
        {
            /* TODO: Need to log the failure and probably exit the image */
        }
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_8254_Read
 *  This function is called to perform a read from one of the Counter Registers on an Intel 8254 Programmable Interval
 *  Timer chip.
 *
 * Input Parameters:
 *  index:
 *      An integer representing the register this read is to be accessing.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  The one byte result of the read.
 */
u8
AXP_8254_Read(u32 index)
{
    u8 retVal = 0;

    /*
     * We cannot allow this function to be called until everything has been initialized.
     */
    if (initialized == false)
    {
        return(retVal);
    }

    /*
     * We cannot read from the control word register.  So, don't try.
     */
    if (index < 3)
    {

        /*
         * If the status latch register has been initialized for reading, then do so now.  Indicate that it is no longer
         * available for reading when done.
         */
        if (counter[index].sl_read_pending)
        {
            retVal = counter[index].status_latch;
            counter[index].sl_read_pending = false;
        }

        /*
         * If the output latch register has been initialized for reading, then perform the appropriate read based on the
         * read/write mode in play for this register.  When finished reading everything of value in the output latch,
         * indicate that it is no longer available for reading.
         */
        else if (counter[index].output_latch.read_pending)
        {
            switch (counter[index].status.read_write)
            {
                /* Not valid read mode */
                case 0:
                    break;

                /* Read least significant byte only */
                case 1:
                    retVal = counter[index].output_latch.lsb;
                    counter[index].output_latch.read_pending = false;
                    break;

                /* Read most significant byte only */
                case 2:
                    retVal = counter[index].output_latch.msb;
                    counter[index].output_latch.read_pending = false;
                    break;

                /* Read least significant byte only */
                case 3:
                    if (counter[index].lsb_done)
                    {
                        retVal = counter[index].output_latch.msb;
                        counter[index].output_latch.read_pending = false;
                        counter[index].lsb_done = true;
                    }
                    else
                    {
                        retVal = counter[index].output_latch.lsb;
                        counter[index].lsb_done = true;
                    }
                    break;
            }
        }

        /*
         * OK, we are reading directly from the counter (this is analogous to the output latch "following" the counter
         * element.  So, read the counter based off of the read/write mode.
         */
        else
        {
            switch (counter[index].status.read_write)
            {
                /* Not valid read mode */
                case 0:
                    break;

                /* Read least significant byte only */
                case 1:
                    retVal = (u8) (counter[index].counter & 0x0f);
                    break;

                /* Read most significant byte only */
                case 2:
                    retVal = (u8) ((counter[index].counter & 0xf0) >> 4);
                    break;

                /* Read least significant byte followed by most significant byte */
                case 3:
                    if (counter[index].count_register.lsb_done)
                    {
                        retVal = counter[index].saved_msb;
                        counter[index].lsb_done = false;
                    }
                    else
                    {
                        u16 curr_counter = counter[index].counter;

                        retVal = (u8) (curr_counter & 0x0f);
                        counter[index].saved_msb = (u8) ((curr_counter & 0xf0) >> 4);
                        counter[index].lsb_done = true;
                    }
                    break;
            }
        }
    }

    /*
     * Return the result of the read back to the caller
     */
    return(retVal);
}

/*
 * AXP_8254_GetStatusOut
 *  This function is called to get the value of the status out field for the specified timer.
 *
 * Input Parameters:
 *  index:
 *      An unsigned 32-bit index for the counter status to be returned.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  true:   out is set
 *  false:  out is clear
 */
bool
AXP_8254_GetStatusOut(u32 index)
{
    bool retVal = false;

    /*
     * We cannot allow this function to be called until everything has been initialized.
     */
    if (initialized == false)
    {
        return(retVal);
    }

    /*
     * We cannot read from the control word register.  So, don't try.
     */
    if (index < 3)
    {
        retVal = counter[index].status.output == 1;
    }

    /*
     * Return the results back to the caller.
     */
    return(retVal);
}

/*
 * AXP_8254_Decrement
 *  This function is called to decrement the counter based on mode and setting of BCD flag.
 *
 * Input Parameters:
 *  counter:
 *      A pointer to the counter structure to be decremented.
 *
 * Output Parameters:
 *  counter:
 *      A pointer to the decremented counter structure.
 *
 * Return Values:
 *  None.
 */
static void
AXP_8254_Decrement(AXP_8254_Counter *counter)
{
    u16 decrementValue = counter->status.mode == 3 ? 2 : 1;

    /*
     * The conditions are just decremented and will wrap from 0 to 0xffff/0x9999.
     */
    if (((counter->status.mode == 0) || (counter->status.mode == 1) || (counter->status.mode == 4) ||
        (counter->status.mode == 5)) ||
        (((counter->status.mode == 2) || (counter->status.mode == 3) || (counter->status.mode == 6) ||
         (counter->status.mode == 7)) && (counter->counter != 0)))
    {
        counter->counter -= decrementValue;
    }

    /*
     * The remaining will be set to their initial value.
     */
    else
    {
        counter->counter = counter->count_register.value;

        /*
         * If we are in Mode 3, then we will be decrementing by 2.  In this case we need to make sure we have an even
         * initial counter value.
         */
        if (((counter->status.mode == 3) || (counter->status.mode == 7)) && (counter->counter & 1))
        {
            counter->counter--;
        }
    }

    /*
     * If we are in BCD mode, then we need to convert any 0xf values to 0x9.
     */
    if (counter->status.bcd == 1)
    {
        u16 mask = 0xf000;
        u16 nine = 0x9000;

        while (mask)
        {

            /*
             * If the current nibble in the counter is a value of 0xf, then mask in the other nibbles and set the
             * current one to 0x9.
             */
            if ((counter->counter & mask) == mask)
            {
                counter->counter = nine | (counter->counter & !mask);
            }
            mask >>= 4;
            nine >>= 4;
        }
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_8254_Main
 *  This function is the thread entry point in which the counters are decremented according to their mode.
 *
 * Input Parameters:
 *  None.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
static void
*AXP_8254_Main(void *arg)
{
    /* TODO: We need to determine a similar triggering using a timer or other processing schemes */
    while(true)
    {
        int ii;

        for (ii = 0; ii < AXP_8254_NUM_COUNTERS; ii++)
        {
            if (!counter[ii].status.null_count)
            {
                AXP_8254_Decrement(&counter[ii]);

                /*
                 * Determine what we are supposed to do with the decremented count value for this counter.
                 */
                switch(counter[ii].status.mode)
                {

                    /*
                     * Mode 0: Interrupt on terminal count
                     */
                    case 0:

                        /*
                         * If OUT is not high and the counter reaches zero, then set OUT to high.
                         */
                        if ((counter[ii].status.output == 0) && (counter[ii].counter == 0))
                        {
                            counter[ii].status.output = 1;
                        }
                        break;

                    /*
                     * Mode 1: Hardware Retriggerable One-shot
                     *
                     * Mode 3: Square Wave Mode
                     */
                    case 1:
                    case 3:
                    case 7:     /* The high bit is a don't care */

                        /*
                         * If the counter reaches zero, then OUT is inverted.
                         */
                        if (counter[ii].counter == 0)
                        {
                            counter[ii].status.output = counter[ii].status.output ? 0 : 1;
                        }
                        AXP_8259A_WriteIRQ(true, 0, true);
                        break;

                    /*
                     * Mode 2: Rate Generator
                     */
                    case 2:
                    case 6:     /* The high bit is a don't care */
                        if (counter[ii].counter == 1)
                        {
                            counter[ii].status.output = 0;
                        }
                        else
                        {
                            counter[ii].status.output = 1;
                        }
                        break;

                    /*
                     * Mode 4: Software Triggered Strobe
                     *
                     * Mode 5: Hardware Triggered Strobe
                     */
                    case 4:
                    case 5:
                        if (counter[ii].counter == 0)
                        {
                            counter[ii].status.output = 0;
                        }
                        else
                        {
                            counter[ii].status.output = 1;
                        }
                        break;
                }
            }
        }
    }

    /*
     * Return back to the caller.
     */
    return(NULL);
}
