/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 01-Nov-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_8259A_PIC_H_
#define _AXP_8259A_PIC_H_

#include "com-util/AXP_Common_Include.h"

typedef struct
{
    u8 ic4 : 1;
    u8 sngl : 1;
    u8 adi : 1;
    u8 ltim : 1;
    u8 mb1 : 1;
    u8 a5 : 1;
    u8 a6 : 1;
    u8 a7 : 1;
} AXP_8259A_ICW1;

#define AXP_8259A_ICW4_NOT_NEEDED 0
#define AXP_8259A_ICW4_NEEDED 1
#define AXP_8259A_CASCADE_MODE 0
#define AXP_8259A_SINGLE_MODE 1
#define AXP_8259A_INTERVAL_8 0
#define AXP_8259A_INTERVAL_4 1
#define AXP_8259A_EDGE_TRIGGER_MODE 0
#define AXP_8259A_LEVEL_TRIGGER_MODE 1

typedef struct
{
    u8 interrupt_vector_addr;
} AXP_8259A_ICW2;
#define AXP_8259A_INTER_VEC_MASK 0xf8

typedef struct
{
    u8 s0 : 1;
    u8 s1 : 1;
    u8 s2 : 1;
    u8 s3 : 1;
    u8 s4 : 1;
    u8 s5 : 1;
    u8 s6 : 1;
    u8 s7 : 1;
} AXP_8259A_ICW3_MASTER;

#define AXP_8259A_HAS_NO_SLAVE 0
#define AXP_8259A_HAS_A_SLAVE 1

typedef struct
{
    u8 id0 : 1;
    u8 id1 : 1;
    u8 id2 : 1;
    u8 mbz : 5;
} AXP_8259A_ICW3_SLAVE;

typedef struct
{
    u8 upm : 1;
    u8 aeoi : 1;
    u8 m_s : 1;
    u8 buf : 1;
    u8 sfnm : 1;
    u8 mbz : 3;
} AXP_8259A_ICW4;

#define AXP_8259A_MCS_8x_MODE 0
#define AXP_8259A_808x_MODE 1
#define AXP_8259A_NORMAL_EOI 0
#define AXP_8259A_AUTO_EOI 1
#define AXP_8259A_NON_BUFFERED_MODE 0
#define AXP_8259A_BUFFERED_MODE 1
#define AXP_8259A_SLAVE 0
#define AXP_8259A_MASTER 1
#define AXP_8259A_NOT_NESTED 0
#define AXP_8259A_NESTED 1

typedef struct
{
    u8 mask;
} AXP_8259A_OCW1;

#define AXP_8259A_MASK_RESET 0
#define AXP_8259A_MASK_SET 1

typedef struct
{
    u8 ir_level_action : 3;
    u8 mbz : 2;
    u8 command : 3; /* R, SL, EOI */
} AXP_8259A_OCW2;

#define AXP_8259A_ROTATE_AUTO_EOI_CLR 0
#define AXP_8259A_NON_SPECIFIC_EOI_CMD 1
#define AXP_8259A_NOOP_CMD 2
#define AXP_8259A_SPECIFIC_EOI_CMD 3
#define AXP_8259A_ROTATE_AUTO_EOI_SET 4
#define AXP_8259A_ROTATE_NON_SPECIFIC_CMD 5
#define AXP_8259A_SET_PRIORITY_CMD 6
#define AXP_8259A_ROTATE_SPECIFIC_CMD 7

typedef struct
{
    u8 ris : 1;
    u8 rr : 1;
    u8 p : 1;
    u8 mbo : 1;
    u8 mbz0 : 1;
    u8 smm : 1;
    u8 essm : 1;
    u8 mbz1 : 1;
} AXP_8259A_OCW3;

typedef union
{
    u8 data;
    AXP_8259A_ICW1 icw1;
    AXP_8259A_ICW2 icw2;
    AXP_8259A_ICW3_MASTER m_icw3;
    AXP_8259A_ICW3_SLAVE s_icw3;
    AXP_8259A_ICW4 icw4;
    AXP_8259A_OCW1 ocw1;
    AXP_8259A_OCW2 ocw2;
    AXP_8259A_OCW3 ocw3;
} AXP_8259A_Commands;

typedef enum
{
    PICRunning,
    InitCmdWord1,
    InitCmdWord2,
    InitCmdWord3
} AXP_8259A_States;

typedef enum
{
    IRR,
    ISR,
    IMR
} AXP_8259A_ReadReg;

typedef struct
{
    u8 irr;
    u8 isr;
    u8 imr;
    u8 bottom_priority_dev;
    u8 irq_base;
    u8 piix_edge_selection;
    u8 piix_edge_mask;
    u8 last_irr;
    AXP_8259A_States init_state;
    /* irq int_out; */
    bool init_4;
    bool single_mode;
    bool level_sensitive;
    bool polling;
    bool special_mask_mode;
    bool auto_eoi;
    bool auto_rotate_on_eoi;
    bool fully_nested_mode;
    bool microprocessor_808x_mode;
    bool master;
    AXP_8259A_ReadReg reg_selected;
    /* u32 iobase;  0x20 (master), 0xa0 (slave)*/
    /* u32 piix_edge_addr; 0x4d0 (master), 0x4d1 (slave)*/
} AXP_8259A;

#define AXP_8259A_EDGE_MASK_PRI 0xf8
#define AXP_8259A_EDGE_MASK_SLA 0xde

#define AXP_8259A_NO_PRIORITY_SET 8
#define AXP_8259A_NUM_CHIPS 2

#endif /* _AXP_8259A_PIC_H_ */
