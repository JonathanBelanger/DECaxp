/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 31-Oct-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_8254_PIT_H_
#define _AXP_8254_PIT_H_

#include "com-util/AXP_Common_Include.h"

/*
 * Data is read in in 8 bit increments, and then the 2 bytes are latched directly into the 16-bit counter.  The control
 * register determines how bytes are written into the counter word.
 */
typedef union
{
    u16 value;
    struct
    {
        u8 lsb;
        u8 msb;
    };
    bool lsb_done;
    bool read_pending;
} AXP_8254_Latches;

/*
 * The following structure defines the information needed to manage a single counter element.
 */
typedef struct
{
    AXP_8254_Latches count_register;
    AXP_8254_Latches output_latch;
    u16 counter;
    u8 saved_msb;
    u8 status_latch;
    bool sl_read_pending;
    bool lsb_done;
    union
    {
        u8 byte;
        struct
        {
            u8 bcd : 1;
            u8 mode : 3;
            u8 read_write : 2;
            u8 null_count : 1;
            u8 output : 1;
        };
    } status;
} AXP_8254_Counter;

/*
 * The following structure defines the information needed to control each of the counters.  Some of the information in
 * this register finds its way into the counter's status register.
 */
typedef union
{
    u8 cw;
    struct
    {
        u8 bcd : 1;
        u8 mode : 3;
        u8 read_write : 2;
        u8 select_counter : 2;
    };
    struct
    {
        u8 mbz : 1;
        u8 cnt0 : 1;
        u8 cnt1 : 1;
        u8 cnt2 : 1;
        u8 not_status : 1;
        u8 not_count : 1;
        u8 mbo : 2;
    };
} AXP_8254_Control_Word;

#define AXP_8254_NUM_COUNTERS 3

#endif /* _AXP_8254_PIT_H_ */
