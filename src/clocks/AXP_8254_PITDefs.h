/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 31-Oct-2020 Jonathan D. Belanger
 *  Initially written.
 */

#ifndef _AXP_8254_PITDEFS_H_
#define _AXP_8254_PITDEFS_H_

#include "com-util/AXP_Common_Include.h"

/*
 * Interface entry points
 */
void
AXP_8254_Initialize(void);

void
AXP_8254_Write(u32 index, u8 data);

u8
AXP_8254_Read(u32 index);

bool
AXP_8254_GetStatusOut(u32 index);

#endif /* _AXP_8254_PITDEFS_H_ */
