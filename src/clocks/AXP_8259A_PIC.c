/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 01-Nov-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include "clocks/AXP_8259A_PIC.h"
#include "com-util/AXP_Blocks.h"

static AXP_8259A info[AXP_8259A_NUM_CHIPS];

static u8
AXP_8259A_GetPriority(AXP_8259A *chip, u8 mask);
static void
AXP_8259A_UpdateIRQ(AXP_8259A *chip);
static u8
AXP_8259A_GetIRQ(AXP_8259A *chip);
static void
AXP_8259A_IntA(AXP_8259A *chip, u8 irq);

/*
 * AXP_8259A_Initialize
 *  This function ...
 */
bool
AXP_8259A_Initialize(void)
{
    bool retVal = true;

    /*
     * Initialize both the master and slave chips.
     */
    memset(&info[AXP_8259A_SLAVE], 0, sizeof(AXP_8259A));
    info[AXP_8259A_SLAVE].piix_edge_mask = AXP_8259A_EDGE_MASK_SLA;
    info[AXP_8259A_SLAVE].master = false;
    memset(&info[AXP_8259A_MASTER], 0, sizeof(AXP_8259A));
    info[AXP_8259A_MASTER].piix_edge_mask = AXP_8259A_EDGE_MASK_PRI;
    info[AXP_8259A_MASTER].master = true;

    /*
     * Return the result back to the caller.
     */
    return(retVal);
}

/*
 * AXP_8259A_Write
 *  This function ...
 */
void
AXP_8259A_Write(bool master, bool a0, u8 data)
{
    AXP_8259A *chip = &info[master ? AXP_8259A_MASTER : AXP_8259A_SLAVE];
    AXP_8259A_Commands cmd = {.data = data};
    bool updateIRQ = false;

    if (a0 == false)
    {

        /*
         * ICW1
         */
        if (cmd.icw1.mb1)
        {
            /* interrupt reset */
            chip->init_state = InitCmdWord1;
            chip->init_4 = cmd.icw1.ic4 == AXP_8259A_ICW4_NEEDED;
            chip->single_mode = cmd.icw1.sngl == AXP_8259A_SINGLE_MODE;
            chip->level_sensitive = cmd.icw1.ltim == AXP_8259A_LEVEL_TRIGGER_MODE;
        }

        /*
         * OCW3
         */
        else if (cmd.ocw3.mbz1)
        {
            if (cmd.ocw3.p)
            {
                chip->polling = true;
            }
            if (cmd.ocw3.rr)
            {
                chip->reg_selected = cmd.ocw3.ris ? ISR : IRR;
            }
            if (cmd.ocw3.essm)
            {
                chip->special_mask_mode = cmd.ocw3.smm == 1;
            }
        }

        /*
         * OCW2
         */
        else
        {
            u8 priority;
            u8 irq;

            switch(cmd->ocw2.command)
            {
                case AXP_8259A_ROTATE_AUTO_EOI_CLR:
                    chip->auto_rotate_on_eoi = false;
                    break;

                case AXP_8259A_NON_SPECIFIC_EOI_CMD:
                    priority = AXP_8259A_GetPriority(chip, chip->isr);
                    if (priority < AXP_8259A_NO_PRIORITY_SET)
                    {
                        irq = (priority + chip->bottom_priority_dev) & 0x07;
                        chip->isr &= ~(1 << irq);
                        updateIRQ = true;
                    }
                    break;

                case AXP_8259A_NOOP_CMD:
                    break;

                case AXP_8259A_SPECIFIC_EOI_CMD:
                    irq = cmd.ocw2.ir_level_action;
                    chip->isr &= ~(1 << irq);
                    updateIRQ = true;
                    break;

                case AXP_8259A_ROTATE_AUTO_EOI_SET:
                    chip->auto_rotate_on_eoi = true;
                    break;

                case AXP_8259A_ROTATE_NON_SPECIFIC_CMD:
                    priority = AXP_8259A_GetPriority(chip, chip->isr);
                    if (priority < AXP_8259A_NO_PRIORITY_SET)
                    {
                        irq = (priority + chip->bottom_priority_dev) & 0x07;
                        chip->isr &= ~(1 << irq);
                        chip->bottom_priority_dev = (irq + 1) & 0x07;
                        updateIRQ = true;
                    }
                    break;

                case AXP_8259A_SET_PRIORITY_CMD:
                    chip->bottom_priority_dev = cmd.ocw2.ir_level_action;
                    updateIRQ = true;
                    break;

                case AXP_8259A_ROTATE_SPECIFIC_CMD:
                    irq = cmd.ocw2.ir_level_action;
                    chip->isr &= ~(1 << irq);
                    chip->bottom_priority_dev = (irq + 1) & 0x07;
                    updateIRQ = true;
                    break;
            }
        }
    }
    else
    {

        /*
         * OCW1, ICW2, ICW3 (master and slave), ICW4
         */
        switch (chip->init_state)
        {

            /*
             * OCW1
             */
            case PICRunning:
                chip->imr = cmd.ocw1.mask;
                updateIRQ = true;
                break;

            /*
             * ICW2
             */
            case InitCmdWord1:
                chip->irq_base = cmd.icw2.interrupt_vector_addr & AXP_8259A_INTER_VEC_MASK;
                chip->init_state = chip->single_mode ? (chip->init_4 ? InitCmdWord3 : PICRunning) : InitCmdWord2;
                break;

            /*
             * ICW3 (master and slave)
             *
             * We don't really care about the settings in this initialization command word.
             */
            case InitCmdWord2:
                chip->init_state = chip->init_4 ? InitCmdWord3 : PICRunning;
                break;

             /*
              * ICW4
              */
            case InitCmdWord3:
                chip->fully_nested_mode = cmd.icw4.sfnm == 1;
                chip->auto_eoi = cmd.icw4.aeoi == 1;
                chip->microprocessor_808x_mode = cmd.icw4.upm == 1;
                chip->init_state = PICRunning;
                break;
        }
    }
    if (updateIRQ)
    {
        AXP_8259A_UpdateIRQ(chip);
    }

    /*
     * Return back to the caller
     */
    return;
}

/*
 * AXP_8259A_Read
 *  This function ...
 */
u8
AXP_8259A_Read(bool master, bool a0)
{
    u8 retVal = 0;
    AXP_8259A *chip = &info[master ? AXP_8259A_MASTER : AXP_8259A_SLAVE];

    if (chip->polling)
    {
        retVal = AXP_8259A_GetIRQ(chip);
        if (retVal < AXP_8259A_NO_PRIORITY_SET)
        {
            AXP_8259A_IntA(chip, retVal);
            retVal |= 0x80;
        }
    }
    else
    {
        retVal = a0 ? (chip->reg_selected ? chip->isr : chip->irr) : chip->imr;
    }

    /*
     * Return the results back to the caller.
     */
    return(retVal);
}

/*
 * AXP_8259A_WriteIRQ
 *  This function ...
 */
void
AXP_8259A_WriteIRQ(bool master, u8 irq, bool level)
{
    AXP_8259A *chip = &info[master ? AXP_8259A_MASTER: AXP_8259A_SLAVE];
    u8 mask = 1 << irq;

    if (chip->piix_edge_selection && mask)
    {
        /* level triggered */
        if (level)
        {
            chip->irr = chip->last_irr |= mask;
        }
        else
        {
            chip->irr = chip->last_irr &= ~mask;
        }
    }
    else
    {
        /* edge triggered */
        if (level)
        {
            if ((chip->last_irr & mask) == 0)
            {
                chip->irr |= mask;
            }
            chip->last_irr |= mask;
        }
        else
        {
            chip->last_irr &= ~mask;
        }
    }
    AXP_8259A_UpdateIRQ(chip);

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_8259A_ReadIRQ
 *  This function ...
 */
u8
AXP_8259A_ReadIRQ(bool master)
{
    AXP_8259A *chip = &info[master ? AXP_8259A_MASTER : AXP_8259A_SLAVE];
    u8 irq1 = AXP_8259A_GetIRQ(chip);
    u8 irq2;
    u8 retVal;

    if (irq1 < AXP_8259A_NO_PRIORITY_SET)
    {
        if (irq1 == 2)
        {
            irq2 = AXP_8259A_GetIRQ(&info[AXP_8259A_SLAVE]);
            if (irq2 < AXP_8259A_NO_PRIORITY_SET)
            {
                AXP_8259A_IntA(&info[AXP_8259A_SLAVE], irq2);
            }
            else
            {
                irq2 = 7;
            }
            retVal = info[AXP_8259A_SLAVE].irq_base + irq2;
        }
        else
        {
            retVal = chip->irq_base + irq1;
        }
        AXP_8259A_IntA(chip, irq2);
    }
    else
    {
        irq1 = 7;
        retVal = chip->irq_base + irq1;
    }

    /*
     * Return the results back to the caller.
     */
    return(retVal);
}

/*
 * AXP_8259A_WriteEdge
 *  This function ...
 */
void
AXP_8259A_WriteEdge(bool master, u8 data)
{
    AXP_8259A *chip = &info[master ? AXP_8259A_MASTER : AXP_8259A_SLAVE];

    chip->piix_edge_selection = data & chip->piix_edge_mask;

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_8259A_ReadEdge
 *  This function ...
 */
u8
AXP_8259A_ReadEdge(bool master)
{
    AXP_8259A *chip = &info[master ? AXP_8259A_MASTER : AXP_8259A_SLAVE];

    /*
     * Return the results back to the caller.
     */
    return(chip->piix_edge_selection);
}

/*
 * AXP_8259A_Reset
 *  This function ...
 */
void
AXP_8259A_Reset(bool master)
{
    AXP_8259A *chip = &info[master ? AXP_8259A_MASTER : AXP_8259A_SLAVE];

    chip->piix_edge_selection = 0;
    AXP_8259A_InitReset(chip);
}

/*
 * AXP_8259A_GetPriority
 *  This function is called to return the highest priority found in the mask parameter
 */
static u8
AXP_8259A_GetPriority(AXP_8259A *chip, u8 mask)
{
    u8 retVal = AXP_8259A_NO_PRIORITY_SET;

    if (mask == 0)
    {
        retVal = 0;
        while (mask & (1 << ((retVal + chip->bottom_priority_dev) & 0x07)) == 0)
        {
            retVal++;
        }
    }

    /*
     * Return the results back to the caller.
     */
    return(retVal);
}

/*
 * AXP_8259A_UpdateIRQ
 *  This function ...
 */
static void
AXP_8259A_UpdateIRQ(AXP_8259A *chip)
{
    u8 irq = AXP_8259A_GetIRQ(chip);

    if (irq < AXP_8259A_NO_PRIORITY_SET)
    {
        /* TODO: Raise IRQ */
    }
    else
    {
        /* TODO: Lower IRQ */
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_8259A_GetIRQ
 *  This function ...
 */
static u8
AXP_8259A_GetIRQ(AXP_8259A *chip)
{
    u8 retVal = AXP_8259A_NO_PRIORITY_SET;
    u8 mask = chip->irr & ~chip->imr;
    u8 priority = AXP_8259A_GetPriority(chip, mask);

    if (priority < AXP_8259A_NO_PRIORITY_SET)
    {
        u8 current_priority;

        mask = chip->isr;
        if (chip->special_mask_mode)
        {
            mask &= ~chip->imr;
        }
        if (chip->fully_nested_mode && chip->master)
        {
            mask &= ~(1 << 2);
        }
        current_priority = AXP_8259A_GetPriority(chip, mask);
        if (priority < current_priority)
        {
            retVal = (priority + chip->bottom_priority_dev) & 0x07;
        }
    }

    /*
     * Return the results back to the caller.
     */
    return(retVal);
}

/*
 * AXP_8259A_IntA
 *  This function ...
 */
static void
AXP_8259A_IntA(AXP_8259A *chip, u8 irq)
{
    if (chip->auto_eoi)
    {
        if (chip->auto_rotate_on_eoi)
        {
            chip->bottom_priority_dev = (irq + 1) & 0x07;
        }
    }
    else
    {
        chip->isr |= (1 << irq);
    }
    if (!(chip->level_sensitive & (1 << irq)))
    {
        chip->irr &= ~(1 << irq);
    }
    AXP_UpdateIRQ(chip);
}

static void
AXP_8259A_ResetCommon(AXP_8259A *chip)
{
    chip->irr = chip->piix_edge_selection;
    chip->isr = 0;
    chip->imr = 0;
    chip->bottom_priority_dev = 0;
    chip->irq_base = 0;
    chip->last_irr = 0;
    chip->init_state = PICRunning;
    chip->init_4 = false;
    chip->single_mode = false;
    chip->level_sensitive = false;
    chip->polling = false;
    chip->special_mask_mode = false;
    chip->auto_eoi = false;
    chip->auto_rotate_on_eoi = false;
    chip->fully_nested_mode = false;
    chip->microprocessor_808x_mode = false;
    chip->reg_selected = IRR;
}

/*
 * AXP_8259A_InitReset
 *  This function ...
 */
static void
AXP_8259A_InitReset(AXP_8259A *chip)
{
    AXP_8259A_ResetCommon(chip);
    AXP_8259A_UpdateIRQ(chip);
}
