/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used to emulate IDE
 *  devices in the Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 13-Sep-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_IDE_H_
#define _AXP_IDE_H_
#include "com-util/AXP_Common_Include.h"
#include "com-util/AXP_PCIAPI.h"
#include "vhd/AXP_VirtualDisk.h"

typedef enum
{
    ATA_Nop = 0x00,
    ATA_CFAReqExtendErr = 0x03,
    ATA_DeviceReset = 0x08,
    ATA_CalibrateDrive = 0x10,      /* not in ATA Extensions Documentation */
    ATA_ReadRetry = 0x20,
    ATA_ReadNoRetry = 0x21,         /* not in ATA Extensions Documentation */
    ATA_WriteRetry = 0x30,
    ATA_WriteNoRetry = 0x31,        /* not in ATA Extensions Documentation */
    ATA_CFAWriteWOErase = 0x38,
    ATA_ReadVerifyRetry = 0x40,
    ATA_ReadVerifyNoRetry = 0x41,   /* not in ATA Extensions Documentation */
    ATA_Seek = 0x70,
    ATA_CFAXlateSector = 0x87,
    ATA_ExecuteDevDiags = 0x90,
    ATA_InitDevParams = 0x91,
    ATA_DownloadMicrocode = 0x92,
    ATA_Packet = 0xa0,
    ATA_IdentifyPacketDev = 0xa1,
    ATA_Service = 0xa2,
    ATA_Smart = 0xb0,
    ATA_CFAErase = 0xc0,
    ATA_ReadMultiple = 0xc4,
    ATA_WriteMultiple = 0xc5,
    ATA_SetMultipleMode = 0xc6,
    ATA_ReadDMAQ = 0xc7,
    ATA_ReadDMA = 0xc8,
    ATA_ReadDMAOrig = 0xc9,
    ATA_WriteDMA = 0xca,
    ATA_WriteDMAOrig = 0xcb,
    ATA_WriteDMAQ = 0xcc,
    ATA_CFAWriteMultWOErase = 0xcd,
    ATA_GetMediaStatus = 0xda,
    ATA_MediaLock = 0xde,
    ATA_MediaUnlock = 0xdf,
    ATA_StandbyImmediate = 0xe0,
    ATA_IdleImmediate = 0xe1,
    ATA_Standby = 0xe2,
    ATA_Idle = 0xe3,
    ATA_ReadBuffer = 0xe4,
    ATA_CheckPowerMode = 0xe5,
    ATA_Sleep = 0xe6,
    ATA_FlushCache = 0xe7,
    ATA_WriteBuffer = 0xe8,
    ATA_FlushCacheExt = 0xea,
    ATA_IdentifyDevice = 0xec,
    ATA_MediaEject = 0xed,
    ATA_SetFeatures = 0xef,
    ATA_SecuritySetPwd = 0xf1,
    ATA_SecurityUnlock = 0xf2,
    ATA_SecurityErasePrepare = 0xf3,
    ATA_SecurityEraseUnit = 0xf4,
    ATA_SecurityFreezeLock = 0xf5,
    ATA_SecurityDisablePwd = 0xf6,
    ATA_ReadNativeMaxAddr = 0xf8,
    ATA_SetMaxAddr = 0xf9
} AXP_ATA_Commands;

typedef enum
{
    ATAPI_Read = 0xab,
    ATAPI_Eject = 0x1b
} AXP_ATAPI_Commands;

/*
 * The following structures are used to maintain information about the status
 * of a single drive within a single controller.
 */
#define AXP_IDE_PACKET_CMD_SIZE 12
typedef union
{
    u8 status;
    struct
    {
        u8 busy : 1;                /* BSY  7 */
        u8 deviceReady : 1;         /* DRDY 6 */
        u8 deviceFault : 1;         /* DF   5 */
        u8 seekComplete : 1;        /* DSC  4 */
        u8 dataXferRq : 1;          /* DRQ  3 */
        u8 dataCorrected : 1;       /* CORR 2 */
        u8 indexMark : 1;           /* IDX  1 */
        u8 error : 1;               /* ERR  0 */
    };
} AXP_IDE_StatusReg;
typedef union
{
    u8 error;
    struct
    {
        u8 badBlock : 1;            /* BBK   7 */
        u8 uncorrectDataErr : 1;    /* UNC   6 */
        u8 mediaChanged : 1;        /* MC    5 */
        u8 IDMarkNotFound : 1;      /* IDNF  4 */
        u8 mediaChangeRqd : 1;      /* MCR   3 */
        u8 commandAborted : 1;      /* ABRT  2 */
        u8 track0NotFound : 1;      /* TK0NF 1 */
        u8 addrMarkNotFound : 1;    /* AMNF  0 */
    };
} AXP_IDE_ErrorReg;
typedef union
{
    u16 cylinderNo;
    struct
    {
        u8 lbaLow;
        u8 lbaHigh;
    };
} AXP_IDE_LBAReg;
typedef enum
{
    packetNoneDp0,
    packetDp1,
    packetDp2,
    packetDp3Dp4,
    packetDi
} AXP_PacketPhase;
typedef struct
{
    AXP_VHD_HANDLE diskHandle;
    u32 commandCycle;
    bool commandInProgress;
    bool packetDMA;
    bool lbaMode;
    bool readOnly;
    u8 packetCommand[AXP_IDE_PACKET_CMD_SIZE];
    AXP_PacketPhase packetPhase;
    AXP_IDE_LBAReg lba;
    u8 data;
    AXP_IDE_ErrorReg error;
    u8 features;
    u8 sectorCount;
    u8 sectorNumber;
    u8 head;
    AXP_IDE_StatusReg status;
    u8 command;
    AXP_IDE_StatusReg altStatus;
    u8 deviceControl;
    u8 indexMarkCount;
    u8 multipleSize;
} AXP_IDE_Drive;
#define AXP_DRIVE_SELECT 0x10
#define AXP_LBA_MODE 0x40
#define AXP_DRIVE_SELECTED(data)    (((data) & AXP_DRIVE_SELECT) == AXP_DRIVE_SELECT)
#define AXP_DRIVE_LBAMODE(data)     (((data) & AXP_LBA_MODE) == AXP_LBA_MODE)

/*
 * Busmaster IDE Command Register
 */
#define AXP_BMD_COMMAND_OFFSET_PRI 0x00
#define AXP_BMD_COMMAND_OFFSET_SEC 0x08
typedef union
{
    u8 reg;
    struct
    {
        u8 startStopBM : 1;  /* 1 = enable; 0 = disable */
        u8 res_1 : 2;
        u8 readWriteControl : 1;
        u8 res_2 :4;
    };
} AXP_BM_Command;

/*
 * Busmaster IDE Status Register
 */
#define AXP_BMD_IDE_STATUS_OFFSET_PRI 0x02
#define AXP_BMD_IDE_STATUS_OFFSET_SET 0x0a
typedef union
{
    u8 reg;
    struct
    {
        u8 bmIdeActive : 1;
        u8 error : 1;
        u8 interrupt : 1;
        u8 res : 2;
        u8 drive0DmaCapable : 1;
        u8 drive1DmaCapable : 1;
        u8 simplexonly : 1;
    };
} AXP_BM_Status;

/*
 * Descriptor Table Pointer Register
 */
#define AXP_DESC_TABLE_PTR_OFFSET_PRI 0x04
#define AXP_DESC_TABLE_PTR_OFFSET_SEC 0x0c
typedef union
{
    u32 reg;
    struct
    {
        u32 res : 2;
        u32 baseAddress : 30;
    };
} AXP_DescTablePointer;

#define AXP_DEV_SPEC_1_OFFSET_PRI 0x01
#define AXP_DEV_SPEC_1_OFFSET_SEC 0x09
#define AXP_DEV_SPEC_2_OFFSET_PRI 0x03
#define AXP_DEV_SPEC_2_OFFSET_SEC 0x0b

/* Primary */
#define AXP_REG_00_MASK  0xffe7ff09
#define AXP_REG_01_MASK  0x00ffe7ff
#define AXP_REG_02_MASK  0x0000ffe7
#define AXP_REG_03_MASK  0x000000ff
#define AXP_REG_04_MASK  0xffffffff
/* Secondary */
#define AXP_REG_08_MASK  0xffe7ff09
#define AXP_REG_09_MASK  0x00ffe7ff
#define AXP_REG_10_MASK  0x0000ffe7
#define AXP_REG_11_MASK  0x000000ff
#define AXP_REG_12_MASK  0xffffffff

/*
 * The following structure is used to maintain information about the status of
 * a single controller containing up to two drives..
 */
#define AXP_IDE_BUF_SIZE 65536  /* 64K words = 128K = 256 sectors @ 512 bytes */
#define AXP_IDE_BUSMASTER_SIZE 16
#define AXP_IDE_DRIVE_COUNT 2
typedef struct
{
    pthread_t controllerThread;
    void *thread_args;
    pthread_cond_t controllerCond;
    pthread_mutex_t controllerMutex;
    AXP_IDE_Drive drive[AXP_IDE_DRIVE_COUNT];
    u8 busmaster[AXP_IDE_BUSMASTER_SIZE];
    u16 data[AXP_IDE_BUF_SIZE];
    u16 dataPtr;
    u16 dataSize;
    bool disableIrq;
    bool reset;
    bool resetInProg;
    bool selected;
    bool interruptPend;
    u8 dmaMode;
    u8 bmStatus;
    u8 driveCount;       /* Number of configured drives on this controller */
} AXP_IDE_Controller;

/*
 * ATA Registers
 */
#define ATA_REG_DATA            0
#define ATA_REG_ERROR           1
#define ATA_REG_FEATURES        1
#define ATA_REG_SECTOR_COUNT    2
#define ATA_REG_SECTOR_NUMBER   3
#define ATA_REG_CYLINDER_LOW    4
#define ATA_REG_CYLINDER_HIGH   5
#define ATA_REG_DRIVE_SELECT    6
#define ATA_REG_COMMAND         7
#define ATA_REG_STATUS          7

/*
 * Memory region IDs
 */
#define AXP_PRI_COMMAND         1
#define AXP_PRI_CONTROL         2
#define AXP_SEC_COMMAND         3
#define AXP_SEC_CONTROL         4
#define AXP_PRI_BUSMASTER       5
#define AXP_SEC_BUSMASTER       6

/*
 * Bar IDs
 */
#define AXP_BAR_PRI_COMMAND     0
#define AXP_BAR_PRI_CONTROL     1
#define AXP_BAR_SEC_COMMAND     2
#define AXP_BAR_SEC_CONTROL     3
#define AXP_BAR_BUSMASTER       4

#define AXP_IDE_CONTROLLER_COUNT 2
typedef struct
{
    AXP_PCI_ConfigData currentConfig;
    AXP_IDE_Controller controller[AXP_IDE_CONTROLLER_COUNT];
    u32 devId;
    u8 controllerCount;    /* Number of controllers configured */
} AXP_IDE;

/*
 * The following definitions are used to make the code a bit more readable and
 * are loosely taken from the es40 code.
 */
#define AXP_SELECTED(ide, ctrl)                 (ide).controller[ctrl].selected
#define AXP_CONTROLLER(ide, ctrl)               (ide).controller[ctrl]
#define AXP_SELECTED_DISK(ide, ctrl)            AXP_CONTROLLER(ide, ctrl).drive[AXP_SELECTED(ide, ctrl)]
#define AXP_DISK(ide, ctrl, disk)               AXP_CONTROLLER(ide, ctrl).drive[disk]
#define AXP_BITSET(val, bits)                   (((val) & (bits)) == (bits))
#define AXP_CONTROLLER_EXISTS(ide, ctrl)        (ctrl) < (ide).controllerCount
#define AXP_DISK_EXISTS(ide, ctrl, disk)        ((ctrl) < (ide).controllerCount) && \
                                                 ((disk) < AXP_CONTROLLER(ide, ctrl).driveCount)
#define AXP_SELECTED_EXISTS(ide, ctrl)          ((ctrl) < (ide).controllerCount) && \
                                                 ((AXP_SELECTED(ide, ctrl) ? 1 : 0) < \
                                                  AXP_CONTROLLER(ide, ctrl).driveCount)

#endif /* _AXP_IDE_H_ */
