/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This source file contains the code to emulate IDE devices for the Digital
 *  Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 26-Nov-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include "p-chip/devices/Ali_M1543C/IDE/AXP_IDE.h"

static const AXP_PCI_ConfigData configBase =
{
    .type0.common.vendorID = AXP_PCI_ALADDIN_VENDOR_ID,     /* Index 01h-00h, RO */
    .type0.common.deviceID = AXP_PCI_M5229_DEVICE_ID,       /* Index 03h-02h, RO */
    .type0.common.command.command = 0,                      /* Index 05h-04h, RW */
    .type0.common.status.res_0 = 0x0,                       /* Index 07h-06h, RW */
    .type0.common.status.interruptStatus = 0x0,
    .type0.common.status.capabilitiesList = 0x0,
    .type0.common.status.sixtySixMhz = 0x0,
    .type0.common.status.res_1 = 0x0,
    .type0.common.status.fastB2BEna = 0x1,
    .type0.common.status.masterDataParityErr = 0x0,
    .type0.common.status.DEVSELTiming = AXP_PCI_STATUS_DEVSEL_MED,
    .type0.common.status.signalTargetAbort = 0x0,
    .type0.common.status.rcvdTargetAbort = 0x0,
    .type0.common.status.rcvdMasterAbort = 0x0,
    .type0.common.status.signalledSystemErr = 0x0,
    .type0.common.status.detectParityErr = 0x0,
    .type0.common.revisionID = AXP_PCI_M5229_REV_ID,        /* Index 08h, RO */
    /* Index 0bh-09h, RO */
    .type0.common.programmingInterface = AXP_PCI_01_IDE_MASTER |
                                         AXP_PCI_01_IDE_SEC_PROG |
                                         AXP_PCI_01_IDE_PRI_PROG,
    .type0.common.subclass = AXP_PCI_01_SUB_IDE,
    .type0.common.class = AXP_PCI_CLASS_STORAGE,
    .type0.common.cacheLineSize = 0x00,                     /* Index 0dh-0ch, RO */
    .type0.common.latencyTimer = 0x00,
    .type0.common.headerType.headerType = 0x00,             /* Index 0eh, RO */
    .type0.common.bist.BIST = 0x00,                         /* Index 2bh-0fh, RO */
    .type0.bar[0].BAR = 0x000001f1,
    .type0.bar[1].BAR = 0x000003f5,
    .type0.bar[2].BAR = 0x00000171,
    .type0.bar[3].BAR = 0x00000375,
    .type0.bar[4].BAR = 0x0000f001,
    .type0.bar[5].BAR = 0,
    .type0.cardbusCISPtr = 0,
    .type0.subsystemVendorID = 0x0000,                      /* Index 2dh-2ch, RW */
    .type0.subsystemID = 0x0000,                            /* Index 2fh-2eh, RW */
    .type0.ROMBaseAddr = 0,                                 /* Index 3fh-30h, RO */
    .type0.capabilitiesPtr = 0x00,
    .type0.res_0[0] = 0x00,
    .type0.res_0[1] = 0x00,
    .type0.res_0[2] = 0x00,
    .type0.res_1 = 0,
    .type0.interruptLine = 0x00,
    .type0.interruptPIN = 0x01,
    .type0.minGrant = 0x02,
    .type0.maxLatency = 0x04,
    .configLong[16] = 0,                                    /* Index 43h-40h */
    .configLong[17] = 0,                                    /* Index 47h-44h */
    .configLong[18] = 0,                                    /* Index 4bh-48h */
    .configByte[76] = 0,                                    /* Index 4ch */
    /* Configuration registers */
    .configByte[77] = 0,                                    /* Index 4dh, RW */
    .configByte[78] = 0,                                    /* Index 4eh, RO */
    .configByte[79] = 0,                                    /* Index 4fh, RW */
    .configByte[80] = 0,                                    /* Index 50h, RW */
    /* Reset and Testing register */
    .configByte[81] = 0,                                    /* Index 51h, RW */
    /* CFG_USE_CMDT and Flexible Channel Setting */
    .configByte[82] = 0,                                    /* Index 52h, RW */
    .configByte[83] = 0,                                    /* Index 53h, RW */
    /* FIFO threshold of primary channel drive 0 and drive 1 */
    .configByte[84] = 0x55,                                 /* Index 54h, RW */
    /* FIFO threshold of secondary channel drive 0 and drive 1 */
    .configByte[85] = 0x55,                                 /* Index 55h, RW */
    /* Ultra DMA /33 setting for Primary drive 0 and drive 1 */
    .configByte[86] = 0,                                    /* Index 56h, RW */
    /* Ultra DMA /33 setting for Secondary drive 0 and drive 1 */
    .configByte[87] = 0,                                    /* Index 57h, RW */
    /* Primary channel address setup timing register */
    .configByte[88] = 0,                                    /* Index 58h, RW */
    /* Primary channel command block timing register */
    .configByte[89] = 0,                                    /* Index 59h, RW */
    /* Primary channel Drive 0 data read/write timing register */
    .configByte[90] = 0,                                    /* Index 5ah, RW */
    /* Primary channel Drive 1 data read/write timing register */
    .configByte[81] = 0,                                    /* Index 5bh, RW */
    /* Secondary channel address setup timing register */
    .configByte[82] = 0,                                    /* Index 5ch, RW */
    /* Secondary channel command block timing register */
    .configByte[83] = 0,                                    /* Index 5dh, RW */
    /* Secondary channel Drive 0 data read/write timing register */
    .configByte[94] = 0,                                    /* Index 5eh, RW */
    /* Secondary channel Drive 1 data read/write timing register */
    .configByte[95] = 0,                                    /* Index 5fh, RW */
    /* Master byte counter for each PRD table entry */
    .configWord[48] = 0,                                    /* Index 60-61h, RO */
    /* Latency timer of PCI interface */
    .configByte[98] = 0,                                    /* Index 62h, RO */
    /* Latency timer expire indicator */
    .configByte[99] = 0,                                    /* Index 63h, RO */
    /* Byte counter for counting in ATA state machine */
    .configWord[50] = 0,                                    /* Index 64-65h, RO */
    /* Sector count counter for counting in ATA state machine */
    .configByte[102] = 0,                                   /* Index 66h, RO */
    /* Block size counter for counting in ATA state machine */
    .configByte[103] = 0,                                   /* Index 67h, RO */
    /* Block size register of device 0 on primary channel */
    .configByte[104] = 0,                                   /* Index 68h, RO */
    /* Block size register of device 1 on primary channel */
    .configByte[105] = 0,                                   /* Index 69h, RO */
    /* Block size register of device 0 on secondary channel */
    .configByte[106] = 0,                                   /* Index 6ah, RO */
    /* Block size register of device 1 on secondary channel */
    .configByte[107] = 0,                                   /* Index 6bh, RO */
    /* Primary channel selector count register */
    .configByte[108] = 0,                                   /* Index 6ch, RO */
    /* Secondary channel selector count register */
    .configByte[109] = 0,                                   /* Index 6dh, RO */
    /* Primary channel command register */
    .configByte[110] = 0,                                   /* Index 6eh, RO */
    /* Secondary channel command register */
    .configByte[111] = 0,                                   /* Index 6fh, RO */
    /* Primary channel byte count high-low register */
    .configWord[56] = 0,                                    /* Index 71-70h, RO */
    /* Secondary channel byte count high-low register */
    .configWord[57] = 0,                                    /* Index 73-72h, RO */
    .configByte[116] = 0,                                   /* Index 74h, RO */
    .configByte[117] = 0,                                   /* Index 75h, RO */
    .configByte[118] = 0,                                   /* Index 76h, RO */
    .configByte[119] = 0,                                   /* Index 77h, RO */
    /* IDE clock's frequency default = 33 */
    .configByte[120] = 0x21,                                /* Index 78h, RW */
    .configByte[121] = 0,                                   /* Index 79h, RO */
    .configByte[122] = 0,                                   /* Index 7ah, RO */
    .configByte[123] = 0,                                   /* Index 7bh, RO */
    .configByte[124] = 0,                                   /* Index 7ch, RW */
    .configByte[125] = 0,                                   /* Index 7dh, RO */
    .configByte[126] = 0,                                   /* Index 7eh, RO */
    .configByte[127] = 0,                                   /* Index 7fh, RO */
    .configLong[32] = 0,                                    /* Index 83-80h */
    .configLong[33] = 0,                                    /* Index 87h-84h */
    .configLong[34] = 0,                                    /* Index 8bh-88h */
    .configLong[35] = 0,                                    /* Index 8fh-8ch */
    .configLong[36] = 0,                                    /* Index 93h-90h */
    .configLong[37] = 0,                                    /* Index 97h-94h */
    .configLong[38] = 0,                                    /* Index 9bh-98h */
    .configLong[39] = 0,                                    /* Index 9fh-9ch */
    .configLong[40] = 0,                                    /* Index a3h-a0h */
    .configLong[41] = 0,                                    /* Index a7h-a4h */
    .configLong[42] = 0,                                    /* Index abh-a8h */
    .configLong[43] = 0,                                    /* Index afh-ach */
    .configLong[44] = 0,                                    /* Index b3h-b0h */
    .configLong[45] = 0,                                    /* Index b7h-b4h */
    .configLong[46] = 0,                                    /* Index bbh-b8h */
    .configLong[47] = 0,                                    /* Index bfh-bch */
    .configLong[48] = 0,                                    /* Index 63h-c0h */
    .configLong[49] = 0,                                    /* Index 67h-c4h */
    .configLong[50] = 0,                                    /* Index cbh-c8h */
    .configLong[51] = 0,                                    /* Index cfh-cch */
    .configLong[52] = 0,                                    /* Index d3h-d0h */
    .configLong[53] = 0,                                    /* Index d7h-d4h */
    .configLong[54] = 0,                                    /* Index dbh-d8h */
    .configLong[55] = 0,                                    /* Index dfh-dch */
    .configLong[56] = 0,                                    /* Index e3h-e0h */
    .configLong[57] = 0,                                    /* Index e7h-e4h */
    .configLong[58] = 0,                                    /* Index ebh-e8h */
    .configLong[59] = 0,                                    /* Index efh-ech */
    .configLong[60] = 0,                                    /* Index f3h-f0h */
    .configLong[61] = 0,                                    /* Index f7h-f4h */
    .configLong[62] = 0,                                    /* Index fbh-f8h */
    .configLong[63] = 0                                     /* Index ffh-fch */
};
static const AXP_PCI_ConfigData configMask =
{
    .configLong[0] = 0 ,            // CFID: vendor + device
    .configLong[1] = 0x00000105,    // CFCS: command + status
    .configLong[2] = 0x00000000,    // CFRV: class + revision
    .configLong[3] = 0x0000ffff,    // CFLT: latency timer + cache line size
    .configLong[4] = 0xfffffff8,    // BAR0
    .configLong[5] = 0xfffffffc,    // BAR1: CBMA
    .configLong[6] = 0xfffffff8,    // BAR2:
    .configLong[7] = 0xfffffffc,    // BAR3:
    .configLong[8] = 0xfffffff0,    // BAR4:
    .configLong[9] = 0x00000000,    // BAR5:
    .configLong[10] = 0x00000000,    // CCIC: CardBus
    .configLong[11] = 0x00000000,    // CSID: subsystem + vendor
    .configLong[12] = 0x00000000,    // BAR6: expansion rom base
    .configLong[13] = 0x00000000,    // CCAP: capabilities pointer
    .configLong[14] = 0x00000000,
    .configLong[15] = 0x000000ff,    // CFIT: interrupt configuration
    .configLong[16] = 0,
    .configLong[17] = 0,
    .configLong[18] = 0,
    .configLong[19] = 0,
    .configLong[20] = 0,
    .configLong[21] = 0,
    .configLong[22] = 0,
    .configLong[23] = 0,
    .configLong[24] = 0,
    .configLong[25] = 0,
    .configLong[26] = 0,
    .configLong[27] = 0,
    .configLong[28] = 0,
    .configLong[29] = 0,
    .configLong[30] = 0,
    .configLong[31] = 0,
    .configLong[32] = 0,
    .configLong[33] = 0,
    .configLong[34] = 0,
    .configLong[35] = 0,
    .configLong[36] = 0,
    .configLong[37] = 0,
    .configLong[38] = 0,
    .configLong[39] = 0,
    .configLong[40] = 0,
    .configLong[41] = 0,
    .configLong[42] = 0,
    .configLong[43] = 0,
    .configLong[44] = 0,
    .configLong[45] = 0,
    .configLong[46] = 0,
    .configLong[47] = 0,
    .configLong[48] = 0,
    .configLong[49] = 0,
    .configLong[50] = 0,
    .configLong[51] = 0,
    .configLong[52] = 0,
    .configLong[53] = 0,
    .configLong[54] = 0,
    .configLong[55] = 0,
    .configLong[56] = 0,
    .configLong[57] = 0,
    .configLong[58] = 0,
    .configLong[59] = 0,
    .configLong[60] = 0,
    .configLong[61] = 0,
    .configLong[62] = 0,
    .configLong[63] = 0
};

static AXP_IDE deviceBlk;
static bool initialized = false;

#if 0
static const char *register_names[] =
{
    "DATA",
    "ERROR/FEATURE",
    "SECTOR_COUNT/PKT REASON",
    "SECTOR_NO",
    "CYL_HI/PKT BYTE HIGH",
    "CYL_LOW/PKT BYTE LOW",
    "DRIVE",
    "STATUS/COMMAND"
};
#endif

typedef union
{
    struct
    {
        u8 version_patch;
        u8 version_minor;
        u8 version_major;
        u8 res;
    };
    u32 version;
} _AXP_PCI_API_Supported_Version;

static _AXP_PCI_API_Supported_Version supported_version =
{
    AXP_PCI_VER_PAT,
    AXP_PCI_VER_MIN,
    AXP_PCI_VER_MAJ,
    0
};

static u32 ConfigRead(u16 func, u32 address, u32 dsize);
static void ConfigWrite(u16 func, u32 address, u32 dsize, u32 data);
static u32 MemoryRead(u16 func, u32 address, u32 dsize);
static void MemoryWrite(u16 func, u32 address, u32 dsize, u32 data);
static u32 BarRead(u16 func, u32 address, u32 dsize);
static void BarWrite(u16 func, u32 address, u32 dsize, u32 data);
static void Reset(void);
static u32 CommandRead(u16 func, u32 address, u32 dsize);
static void CommandWrite(u16 func, u32 address, u32 dsize, u32 data);
static u32 ControlRead(u16 func, u32 address);
static void ControlWrite(u16 func, u32 address, u32 dsize);
static u32 BusmasterRead(u16 func, u32 address, u32 dsize);
static void BusmasterWrite(u16 func, u32 address, u32 dsize, u32 data);
static void CommandAborted(u32 func, u32 data);
static void SetSignature(u32 func, u8 device);
static void ExecuteDiskCommand(u32 func);

static void *AXP_IDE_Main();

/*
 * TODO: We need to be able to create a controller and all its disk drives in a
 * single call.  Therefore, we need the following information:
 *
 *      1) controller index
 *      2) disk count
 *      3) information for each disk
 *
 * This will require a change to the configuration file XML format.
 */
int initialize_device(AXP_PCIAPI_Device_CB *pci_device_info)
{
    int retVal = EXIT_SUCCESS;
    _AXP_PCI_API_Supported_Version suppliedVersion;
    AXP_VHD_STORAGE_TYPE storageType;
    char *diskFilename[AXP_IDE_DRIVE_COUNT];
    char *diskName[AXP_IDE_DRIVE_COUNT];
    u64 diskSize[AXP_IDE_DRIVE_COUNT];
    int how[AXP_IDE_DRIVE_COUNT]; /* TODO:  = R_OK | W_OK; */
    u32 vhd_retVal = AXP_VHD_SUCCESS;
    int controllerIndex;
    int diskCount;
    int ii;
    int pthreadStatus;

    if (initialized == false)
    {
        deviceBlk.controllerCount = 0;
        initialized = true;
    }

    /*
     * Make sure that this is a version we can support.  Support is backward
     * compatible, so a caller that supports a newer version, also supports the
     * older one.  Conversely, a caller that supports an older version, cannot
     * support a newer version.
     */
    suppliedVersion.res = 0;
    suppliedVersion.version_major = pci_device_info->version_major;
    suppliedVersion.version_minor = pci_device_info->version_minor;
    suppliedVersion.version_patch = pci_device_info->version_patch;
    if (supported_version.version <= suppliedVersion.version)
    {
        /* Dump the arguments supplied on the call */
        printf("AXP_1543C PCI Device Initialization called:\n");
        how[0] = R_OK | W_OK; /* TODO: Fix */
        while (pci_device_info->args[ii].type != Empty)
        {
            switch (pci_device_info->args[ii].type)
            {
                case FileName:
                    printf("    Filename: %s\n",
                           pci_device_info->args[ii].name);
                    diskFilename = pci_device_info->args[ii].name; /* TODO: Fix */
                    break;

                case Type:
                    printf("    Type: %u\n",
                           pci_device_info->args[ii].devType);
                    break;

                case ReadOnly:
                    printf("    ReadOnly: %s\n",
                           pci_device_info->args[ii].readOnly ? "Yes" : "No");
                    how = R_OK;
                    break;

                /*
                 * Need:
                 *  Create Version
                 *  Size
                 *  Device ID? (STORAGE_TYPE_DEV_VHDX)
                 *  Vendor ID
                 *  Create Flag?
                 *  StorageType?
                 */

                default:
                    printf("    Unknown Argument: %d\n",
                           pci_device_info->args[ii].type);
                    break;
            }
            ii++;
        }

        /*
         * TODO: Do we want to supply the controller index or just assign the next one?
         */
        if ((controllerIndex == deviceBlk.controllerCount) &&
            (controllerIndex < AXP_IDE_CONTROLLER_COUNT) &&
            ((diskCount >= 0) && (diskCount <= AXP_IDE_DRIVE_COUNT)))
        {
            deviceBlk.controllerCount++;

            AXP_CONTROLLER(deviceBlk, controllerIndex).driveCount = diskCount;
            for (ii = 0; ((ii < diskCount) && (vhd_retVal == AXP_VHD_SUCCESS)); ii++)
            {

                /*
                 * This is the best place to initialize create/open the virtual disk.
                 *
                 * TODO: Do we want to supply the disk id or just assign the next one?
                 * TODO: Fix (multiple disks)
                 */
                if (access(diskFilename[ii], how[ii]) == 0)
                {
                    AXP_VHD_OPEN_PARAM open_param;

                    vhd_retVal = AXP_VHD_Open(&storageType,
                                              diskFilename, 0, 0,
                                              &open_param,
                                              &AXP_DISK(deviceBlk, controllerIndex, ii).diskHandle);

                    /*
                     * TODO: Check to see if the disk size has been increased and if so
                     *       call AXP_VHD_Expand to the new size of the disk,
                     */
                }
                else
                {
                    AXP_VHD_CREATE_PARAM create_param;

                    vhd_retVal = AXP_VHD_Create(&storageType, diskFilename,
                                                (how[0] == R_OK ? ACCESS_READ : ACCESS_ALL),
                                                NULL, CREATE_NONE, 0, &create_param, NULL,
                                                &AXP_DISK(deviceBlk, controllerIndex, ii).diskHandle);
                }
            }

            if (vhd_retVal != AXP_VHD_SUCCESS)
            {
                retVal = EXIT_FAILURE;
            }
            else
            {
                pthreadStatus = pthread_mutex_init(&AXP_CONTROLLER(deviceBlk, controllerIndex).controllerMutex, NULL);
                if (pthreadStatus == 0)
                {
                    pthreadStatus = pthread_cond_init(&AXP_CONTROLLER(deviceBlk, controllerIndex).controllerCond, NULL);
                }
                if (pthreadStatus == 0)
                {
                    pthreadStatus = pthread_create(
                                        &AXP_CONTROLLER(deviceBlk, controllerIndex).controllerThread,
                                        NULL,
                                        &AXP_IDE_Main,
                                        NULL /* TODO: Need an argument? */);
                }
                if (pthreadStatus == 0)
                {

                    /* Do a Reset on this device */
                    Reset();

                    /*
                     * Return the Base Address Register data (legacy, so not in config
                     * data).
                     */
                    for (ii = 0; ii < AXP_PCI_MAX_FUNCTIONS; ii++)
                    {
                        pci_device_info->pciBaseAddressRegisters[ii] =
                            deviceBlk.currentConfig.type0.bar[ii];
                    }
                    pci_device_info->pciExpansionROMBAR =
                        deviceBlk.currentConfig.type0.ROMBaseAddr;

                    /*
                     * Return the addresses of all the entrypoints.
                     */
                    pci_device_info->PCIConfigRead = ConfigRead;
                    pci_device_info->PCIConfigWrite = ConfigWrite;
                    pci_device_info->PCIMemRead = MemoryRead;
                    pci_device_info->PCIMemWrite = MemoryWrite;
                    pci_device_info->PCIBarRead = BarRead;
                    pci_device_info->PCIBarWrite = BarWrite;
                    pci_device_info->PCIReset = Reset;
                }
                else
                {
                    retVal = EXIT_FAILURE;
                }
            }
        }
        else
        {
            retVal = EXIT_FAILURE;
        }
    }
    else
    {
        retVal = EXIT_FAILURE;
    }

    /*
     * Return back to the caller.
     */
    return (retVal);
}

/****************************************************************************
 *                          PCI Interface functions                         *
 ****************************************************************************/

static u32 ConfigRead(u16 func, u32 index, u32 length)
{
    u32 retVal = 0;
    printf("M1543C ConfigRead(%u, %u, %u) called\n", func, index, length);

    switch(length)
    {
        case 1:
            retVal = deviceBlk.currentConfig.configByte[index];
            break;

        case 2:
            retVal = deviceBlk.currentConfig.configWord[index / 2];
            break;

        case 4:
            retVal = deviceBlk.currentConfig.configLong[index / 4];
            break;
    }
    printf("M1543C ConfigRead returning %0*x\n", length*2, retVal);
    return retVal;
}

static void ConfigWrite(u16 func, u32 index, u32 length, u32 data)
{
    printf("M1543C ConfigWrite(%u, %u, %u, %u) called\n", func, index, length, data);

    switch(length)
    {
        case 1:
            {
                u8 mask = configMask.configByte[index];
                u8 configData = data & mask;
                u8 configKeep =
                        deviceBlk.currentConfig.configByte[index] & ~mask;

                deviceBlk.currentConfig.configByte[index] =
                    configData | configMask;
            }
            break;

        case 2:
            {
                u16 mask = configMask.configWord[index / 2];
                u16 configData = data & mask;
                u16 configKeep =
                        deviceBlk.currentConfig.configWord[index / 2] & ~mask;

                deviceBlk.currentConfig.configWord[index / 2] =
                    configData | configMask;
            }
            break;

        case 4:
            {
                u32 mask = configMask.configLong[index / 4];
                u32 configData = data & mask;
                u32 configKeep =
                        deviceBlk.currentConfig.configLong[index / 4] & ~mask;

                deviceBlk.currentConfig.configLong[index / 4] =
                    configData | configMask;
            }
            break;
    }

    printf("M1543C ConfigWrite returning\n");
    return;
}

static u32 MemoryRead(u16 func, u32 address, u32 dsize)
{
    u32 retVal = 0;

    switch(func)
    {
        case AXP_PRI_COMMAND:
            retVal = CommandRead(0, address, dsize);
            break;

        case AXP_PRI_CONTROL:
            retVal = ControlRead(0, address);
            break;

        case AXP_SEC_COMMAND:
            retVal = CommandRead(1, address, dsize);
            break;

        case AXP_SEC_CONTROL:
            retVal = ControlRead(1, address);
            break;

        case AXP_PRI_BUSMASTER:
            retVal = BusmasterRead(0, address, dsize);
            break;

        case AXP_SEC_BUSMASTER:
            retVal = BusmasterRead(0, address, dsize);
            break;
    }

    /*
     * Return the results of this call back to the caller.
     */
    return retVal;
}
static void MemoryWrite(u16 func, u32 address, u32 dsize, u32 data)
{
    switch(func)
    {
        case AXP_PRI_COMMAND:
            CommandWrite(0, address, dsize, data);
            break;

        case AXP_PRI_CONTROL:
            ControlWrite(0, address, data);
            break;

        case AXP_SEC_COMMAND:
            CommandWrite(1, address, dsize, data);
            break;

        case AXP_SEC_CONTROL:
            ControlWrite(1, address, data);
            break;

        case AXP_PRI_BUSMASTER:
            BusmasterWrite(0, address, dsize, data);
            break;

        case AXP_SEC_BUSMASTER:
            BusmasterWrite(1, address, dsize, data);
            break;
    }

    /*
     * Return back to the caller.
     */
    return;
}

static u32 BarRead(u16 func, u32 address, u32 dsize)
{
    u32 retVal = 0;

    switch(func)
    {
        case AXP_BAR_PRI_COMMAND:
            retVal = CommandRead(0, address, dsize);
            break;

        case AXP_BAR_PRI_CONTROL:
            retVal = ControlRead(0, (address - 2));
            break;

        case AXP_BAR_SEC_COMMAND:
            retVal = CommandRead(1, address, dsize);
            break;

        case AXP_BAR_SEC_CONTROL:
            retVal = ControlRead(1, (address - 2));
            break;

        case AXP_BAR_BUSMASTER:
            if (address < 8)
            {
                retVal = BusmasterRead(0, address, dsize);
            }
            else
            {
                retVal = BusmasterRead(1, address - 8, dsize);
            }
            break;
    }

    /*
     * Return the results of this call back to the caller.
     */
    return retVal;
}
static void BarWrite(u16 func, u32 address, u32 dsize, u32 data)
{
    switch(func)
    {
        case AXP_BAR_PRI_COMMAND:
            CommandWrite(0, address, dsize, data);
            break;

        case AXP_BAR_PRI_CONTROL:
            ControlWrite(0, (address - 2), data);
            break;

        case AXP_BAR_SEC_COMMAND:
            CommandWrite(1, address, dsize, data);
            break;

        case AXP_BAR_SEC_CONTROL:
            ControlWrite(1, (address - 2), data);
            break;

        case AXP_BAR_BUSMASTER:
            if (address < 8)
            {
                BusmasterWrite(0, address, dsize, data);
            }
            else
            {
                BusmasterWrite(1, (address - 8), dsize, data);
            }
            break;
    }

    /*
     * Return back to the caller.
     */
    return;
}

static void Reset(void)
{
    printf("[IDE] Reset() called\n");

    memcpy(deviceBlk.currentConfig, configBase, sizeof(AXP_PCI_ConfigData));

    /*
     * Return back to the caller.
     */
    return;
}

/****************************************************************************
 *                          IDE Interface functions                         *
 ****************************************************************************/
static u32 CommandRead(u16 func, u32 address, u32 dsize)
{
    u32 retVal = 0;

    /*
     * If no controllers and no disks on the specific controller are
     * configured, then the data lines actually float to a high, all 1s state.
     */
    if ((func >= deviceBlk.controllerCount) ||
        (AXP_CONTROLLER(deviceBlk, func).driveCount == 0) ||
        (pthread_mutex_lock(&AXP_CONTROLLER(deviceBlk, func).controllerMutex) != 0))
    {
        retVal = 0xffffffff;
    }
    else
    {

        /*
         * Get the data from the requested register.
         */
        switch(address)
        {
            case ATA_REG_DATA:
                if (AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq == false)
                {
                    /* Trying to read data from a disk that is not ready */
                    break;
                }
                if (dsize == sizeof(u16))
                {
                    retVal = AXP_CONTROLLER(deviceBlk, func).data[AXP_CONTROLLER(deviceBlk, func).dataPtr];
                    AXP_CONTROLLER(deviceBlk, func).dataPtr++;
                }
                else    /* dsize == sizeof(u32) */
                {
                    retVal = * (u32 *) &AXP_CONTROLLER(deviceBlk, func).data[AXP_CONTROLLER(deviceBlk, func).dataPtr];
                    AXP_CONTROLLER(deviceBlk, func).dataPtr += 2;
                }
                if (AXP_CONTROLLER(deviceBlk, func).dataPtr >= AXP_CONTROLLER(deviceBlk, func).dataSize)
                {
                    AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                    if (AXP_SELECTED_DISK(deviceBlk, func).commandInProgress)
                    {
                        AXP_SELECTED_DISK(deviceBlk, func).status.busy = true;
                        AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = false;
                        AXP_SELECTED_DISK(deviceBlk, func).altStatus =
                            AXP_SELECTED_DISK(deviceBlk, func).status.status;
                        pthread_cond_signal(&AXP_CONTROLLER(deviceBlk, func).controllerMutex);
                    }
                }
                /* TODO: Should dataPtr be something larger than 16-bits? */
                break;

            case ATA_REG_ERROR:
                retVal = AXP_SELECTED_DISK(deviceBlk, func).error.error;
                break;

            case ATA_REG_SECTOR_COUNT:
                retVal = AXP_SELECTED_DISK(deviceBlk, func).sectorCount;
                break;

            case ATA_REG_SECTOR_NUMBER:
                retVal = AXP_SELECTED_DISK(deviceBlk, func).sectorNumber;
                break;

            case ATA_REG_CYLINDER_LOW:
                retVal = AXP_SELECTED_DISK(deviceBlk, func).lba.lbaLow;
                break;

            case ATA_REG_CYLINDER_HIGH:
                retVal = AXP_SELECTED_DISK(deviceBlk, func).lba.lbaHigh;
                break;

            case ATA_REG_DRIVE_SELECT:
                retVal = 0x80 | (AXP_SELECTED_DISK(deviceBlk, func).lbaMode ? 0x40 : 0x00) |
                    0x20 | (AXP_CONTROLLER(deviceBlk, func).selected ? 0x10 : 0x00) |
                    (AXP_SELECTED_DISK(deviceBlk, func).head & 0x0f);
                break;

            case ATA_REG_STATUS:
                retVal = AXP_SELECTED_DISK(deviceBlk, func).status.status;
                AXP_SELECTED_DISK(deviceBlk, func).indexMarkCount++;
                AXP_SELECTED_DISK(deviceBlk, func).status.indexMark = false;
                if (AXP_SELECTED_DISK(deviceBlk, func).indexMarkCount >= 10)
                {
                    AXP_SELECTED_DISK(deviceBlk, func).indexMarkCount = 0;
                    AXP_SELECTED_DISK(deviceBlk, func).status.indexMark = true;
                }
                /* TODO: clear the interrupt */
                break;
        }
        pthread_mutex_unlock(&AXP_CONTROLLER(deviceBlk, func).controllerMutex);
    }

    /*
     * Return the results of this call back to the caller.
     */
    return retVal;
}
static void CommandWrite(u16 func, u32 address, u32 dsize, u32 data)
{
    /* TODO: Need to handle when we fail to lock the mutex */

    /*
     * If no controllers and no disks on the specific controller are
     * configured, then the data lines actually float to a high, all 1s state.
     */
    if ((func < deviceBlk.controllerCount) &&
        (AXP_CONTROLLER(deviceBlk, func).driveCount != 0) &&
        (pthread_mutex_lock(&AXP_CONTROLLER(deviceBlk, func).controllerMutex) == 0))
    {

        /*
         * Get the data from the requested register.
         */
        switch(address)
        {
            case ATA_REG_DATA:
                if (AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq)
                {
                    if (dsize == sizeof(u16))
                    {
                        AXP_CONTROLLER(deviceBlk, func).data[AXP_CONTROLLER(deviceBlk, func).dataPtr] =
                            *(u16 *) &data;
                        AXP_CONTROLLER(deviceBlk, func).dataPtr++;
                    }
                    else    /* dsize == sizeof(u32) */
                    {
                        * (u32 *) &AXP_CONTROLLER(deviceBlk, func).data[AXP_CONTROLLER(deviceBlk, func).dataPtr] =
                            data;
                        AXP_CONTROLLER(deviceBlk, func).dataPtr += 2;
                    }
                    if (AXP_CONTROLLER(deviceBlk, func).dataPtr >= AXP_CONTROLLER(deviceBlk, func).dataSize)
                    {
                        AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                        AXP_SELECTED_DISK(deviceBlk, func).status.busy = true;
                        AXP_SELECTED_DISK(deviceBlk, func).altStatus =
                            AXP_SELECTED_DISK(deviceBlk, func).status.status;
                        pthread_cond_signal(&AXP_CONTROLLER(deviceBlk, func).controllerMutex);
                    }
                    /* TODO: Should dataPtr be something larger than 16-bits? */
                }
                break;

            case ATA_REG_FEATURES:
                AXP_SELECTED_DISK(deviceBlk, func).features = *(u8 *) &data;
                break;

            case ATA_REG_SECTOR_COUNT:
                AXP_SELECTED_DISK(deviceBlk, func).sectorCount = *(u8 *) &data;
                break;

            case ATA_REG_SECTOR_NUMBER:
                AXP_SELECTED_DISK(deviceBlk, func).sectorNumber = *(u8 *) &data;
                break;

            case ATA_REG_CYLINDER_LOW:
                AXP_SELECTED_DISK(deviceBlk, func).lba.lbaLow = *(u8 *) &data;
                break;

            case ATA_REG_CYLINDER_HIGH:
                AXP_SELECTED_DISK(deviceBlk, func).lba.lbaHigh = *(u8 *) &data;
                break;

            case ATA_REG_DRIVE_SELECT:
                if (AXP_CONTROLLER(deviceBlk, func).selected != AXP_DRIVER_SELECTED(data))
                {
                    AXP_CONTROLLER(deviceBlk, func).selected = AXP_DRIVER_SELECTED(data);
                    AXP_SELECTED_DISK(deviceBlk, func).head = data & 0x0f;
                    AXP_SELECTED_DISK(deviceBlk, func).lbaMode = AXP_DRIVER_LBAMODE(data);
                }
                break;

            case ATA_REG_COMMAND:
                /* TODO: Clear interrupt */
                data = (data & 0xf0) == 0x10 ? 0x10 : data;
                AXP_SELECTED_DISK(deviceBlk, func).commandInProgress = false;
                AXP_SELECTED_DISK(deviceBlk, func).commandCycle = 0;
                AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                AXP_SELECTED_DISK(deviceBlk, func).altStatus = AXP_SELECTED_DISK(deviceBlk, func).status.status;
                AXP_CONTROLLER(deviceBlk, func).dataPtr = 0;
                if (data != 0)
                {
                    AXP_SELECTED_DISK(deviceBlk, func).status.busy = true;
                    AXP_SELECTED_DISK(deviceBlk, func).altStatus = AXP_SELECTED_DISK(deviceBlk, func).status.status;
                    AXP_SELECTED_DISK(deviceBlk, func).commandInProgress = true;
                    AXP_SELECTED_DISK(deviceBlk, func).packetPhase = packetNoneDp0;
                    pthread_cond_signal(&AXP_CONTROLLER(deviceBlk, func).controllerMutex);
                }
                else
                {
                    if (AXP_CONTROLLER(deviceBlk, func).selected)
                    {
                        CommandAborted(func, data);
                    }
                }
                break;
        }
        pthread_mutex_unlock(&AXP_CONTROLLER(deviceBlk, func).controllerMutex);
    }

    /*
     * Return back to the caller.
     */
    return;
}

static u32 ControlRead(u16 func, u32 address)
{
    u32 retVal = 0;

    /* TODO: Need to determine if this is what should be here */
    switch(address)
    {
        case 0:
            retVal = AXP_SELECTED_DISK(deviceBlk, func).altStatus;
            break;

        case 1:
            retVal = AXP_CONTROLLER(deviceBlk, func).selected ? 0x01 : 0x02 |
                     AXP_SELECTED_DISK(deviceBlk, func).head << 2;
            retVal = ~retVal & 0x0ff;
            break;
    }

    /*
     * Return the results of this call back to the caller.
     */
    return retVal;
}

static void ControlWrite(u16 func, u32 address, u32 data)
{
    int ii;
    bool previousReset;

    /* TODO: Need to determine if this is what should be here */
    switch(address)
    {
        case 0:
            previousReset = AXP_CONTROLLER(deviceBlk, func).reset;
            AXP_CONTROLLER(deviceBlk, func).reset = AXP_BITSET(data, 0x40);
            AXP_CONTROLLER(deviceBlk, func).disableIrq = AXP_BITSET(data, 0x20);
            if (!previousReset && AXP_CONTROLLER(deviceBlk, func).reset)
            {
                for (ii = 0; ii < AXP_CONTROLLER(deviceBlk, func).driveCount; ii++)
                {
                    AXP_DISK(deviceBlk, func, ii).status.busy = false;
                    AXP_DISK(deviceBlk, func, ii).status.deviceReady = false;
                    AXP_DISK(deviceBlk, func, ii).status.seekComplete = true;
                    AXP_DISK(deviceBlk, func, ii).status.dataXferRq = false;
                    AXP_DISK(deviceBlk, func, ii).status.error = false;
                    AXP_DISK(deviceBlk, func, ii).command = 0;
                    AXP_DISK(deviceBlk, func, ii).commandInProgress = false;
                }
                AXP_CONTROLLER(deviceBlk, func).resetInProg = true;
                AXP_CONTROLLER(deviceBlk, func).disableIrq = false;
            }
            if (previousReset && !AXP_CONTROLLER(deviceBlk, func).reset)
            {
                for (ii = 0; ii < AXP_CONTROLLER(deviceBlk, func).driveCount; ii++)
                {
                    AXP_DISK(deviceBlk, func, ii).status.busy = false;
                    AXP_DISK(deviceBlk, func, ii).status.deviceReady = false;
                    SetSignature(func, ii);
                }
                AXP_CONTROLLER(deviceBlk, func).resetInProg = false;
            }
            break;

        case 1:     // floppy?
            break;
    }

    /*
     * Return back to the caller.
     */
    return;
}

static u32 BusmasterRead(u16 func, u32 address, u32 dsize)
{
    u32 retVal = 0;

    switch(address)
    {
        case AXP_BMD_COMMAND_OFFSET_PRI:
            retVal = *(u32 *) &AXP_CONTROLLER(deviceBlk, func).busmaster[address] &
                    AXP_REG_00_MASK;
            break;

        case AXP_DEV_SPEC_1_OFFSET_PRI:
            retVal = *(u32 *) &AXP_CONTROLLER(deviceBlk, func).busmaster[address] &
                    AXP_REG_01_MASK;
            break;

        case AXP_BMD_IDE_STATUS_OFFSET_PRI:
            retVal = *(u32 *) &AXP_CONTROLLER(deviceBlk, func).busmaster[address] &
                    AXP_REG_02_MASK;
            break;

        case AXP_DEV_SPEC_2_OFFSET_PRI:
            retVal = *(u32 *) &AXP_CONTROLLER(deviceBlk, func).busmaster[address] &
                    AXP_REG_03_MASK;
            break;

        case AXP_DESC_TABLE_PTR_OFFSET_PRI:
            retVal = *(u32 *) &AXP_CONTROLLER(deviceBlk, func).busmaster[address] &
                    AXP_REG_04_MASK;
            break;

        case AXP_BMD_COMMAND_OFFSET_SEC:
            retVal = *(u32 *) &AXP_CONTROLLER(deviceBlk, func).busmaster[address] &
                    AXP_REG_00_MASK;
            break;

        case AXP_DEV_SPEC_1_OFFSET_SEC:
            retVal = *(u32 *) &AXP_CONTROLLER(deviceBlk, func).busmaster[address] &
                    AXP_REG_01_MASK;
            break;

        case AXP_BMD_IDE_STATUS_OFFSET_SET:
            retVal = *(u32 *) &AXP_CONTROLLER(deviceBlk, func).busmaster[address] &
                    AXP_REG_02_MASK;
            break;

        case AXP_DEV_SPEC_2_OFFSET_SEC:
            retVal = *(u32 *) &AXP_CONTROLLER(deviceBlk, func).busmaster[address] &
                    AXP_REG_03_MASK;
            break;

        case AXP_DESC_TABLE_PTR_OFFSET_SEC:
            retVal = *(u32 *) &AXP_CONTROLLER(deviceBlk, func).busmaster[address] &
                    AXP_REG_04_MASK;
            break;
    }

    /*
     * Reset the bits based off of the number of bytes being returned.
     */
    switch(dsize)
    {
        case 1:     /* 1 byte */
            retVal &= 0x000000ff;
            break;

        case 2:     /* 1 word (2 bytes) */
            retVal &= 0x0000ffff;
            break;

        case 3:     /* 3 bytes */
            retVal &= 0x00ffffff;
            break;

        case 4:     /* 1 longword (4 bytes) */
            /* nothing to do */
            break;

    }

    /*
     * Return the results of this call back to the caller.
     */
    return retVal;
}

/*
 * BusmasterWrite
 *  This function is called to write to the various busmaster registers.  There
 *  are three architecturally defined registers and two device specific
 *  registers.  There are two sets of these registers, one for the Primary
 *  Busmaster and one for the Secondary.  The first four of these registers are
 *  just one byte.  The last one is a single four byte (32-bit) register.  This
 *  function allows for the possibility of writing multiple registers in a
 *  single operation.  So, if this function is called with an address of 0 and
 *  a dsize of 4, then the first 4 registers will be written at once, including
 *  the device specific ones.
 *
 * Input Parameters:
 *  func:
 *      A value indicating the controller being addressed.
 *  address:
 *      A value indicating the offset into the register array of the register(s)
 *      to be written
 *  dsize:
 *      A value indicating the number of bytes to be written.  This can be one
 *      of 1, 2, 3, or 4.
 *  data:
 *      An up to 32-bit value to be written to the addressed busmaster register.
 *
 * Output Parameters:
 *  None
 *
 * Return Values:
 *  None
 */
static void BusmasterWrite(u16 func, u32 address, u32 dsize, u32 data)
{
    u32 data32;
    u32 mask = 0x00000000;
    u32 *regPtr = &AXP_CONTROLLER(deviceBlk, func).busmaster[address];

    switch(dsize)
    {
        case 1:
            if ((address != AXP_DESC_TABLE_PTR_OFFSET_PRI) &&
                (address != AXP_DESC_TABLE_PTR_OFFSET_SEC))
            {
                mask = 0x000000ff;
            }
            break;

        case 2:
            if ((address != AXP_DEV_SPEC_2_OFFSET_PRI) &&
                (address != AXP_DEV_SPEC_2_OFFSET_SEC) &&
                (address != AXP_DESC_TABLE_PTR_OFFSET_PRI) &&
                (address != AXP_DESC_TABLE_PTR_OFFSET_SEC))
            {
                mask = 0x0000ffff;
            }
            break;

        case 3:
            if ((address == AXP_BMD_COMMAND_OFFSET_PRI) ||
                (address == AXP_BMD_COMMAND_OFFSET_SEC) ||
                (address == AXP_DEV_SPEC_1_OFFSET_PRI) ||
                (address == AXP_DEV_SPEC_1_OFFSET_SEC))
            {
                mask = 0x00ffffff;
            }
            break;

        case 4:
            if ((address == AXP_BMD_COMMAND_OFFSET_PRI) ||
                (address == AXP_BMD_COMMAND_OFFSET_SEC) ||
                (address == AXP_DESC_TABLE_PTR_OFFSET_PRI) ||
                (address == AXP_DESC_TABLE_PTR_OFFSET_SEC))
            {
                mask = 0xffffffff;
            }
            break;
    }
    data32 = data & mask;
    *regPtr = (*regPtr & ~mask) | data32;

    /*
     * Return back to the caller.
     */
    return;
}

static void CommandAborted(u32 func, u32 data)
{
    AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
    AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = false;
    AXP_SELECTED_DISK(deviceBlk, func).status.error = false;
    AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
    AXP_SELECTED_DISK(deviceBlk, func).error.commandAborted = true;
    AXP_CONTROLLER(deviceBlk, func).dataPtr = 0;
    AXP_SELECTED_DISK(deviceBlk, func).commandInProgress = false;
    AXP_SELECTED_DISK(deviceBlk, func).altStatus = AXP_SELECTED_DISK(deviceBlk, func).status.status;
    /* TODO: Raise an interrupt */

    /*
     * Return back to the caller.
     */
    return;
}

static void SetSignature(u32 func, u8 device)
{
    AXP_DISK(deviceBlk, func, device).sectorCount = 1;
    AXP_DISK(deviceBlk, func, device).sectorNumber = 1;
    if (AXP_DISK(deviceBlk, func, device).readOnly)
    {
        AXP_DISK(deviceBlk, func, device).lba.cylinderNo = 0;
        AXP_DISK(deviceBlk, func, device).head = 0;
    }
    else
    {
        AXP_DISK(deviceBlk, func, device).lba.lbaLow = 0x14;
        AXP_DISK(deviceBlk, func, device).lba.lbaHigh = 0xeb;
        AXP_DISK(deviceBlk, func, device).head &= 0x10;
    }

    /*
     * Return back to the caller.
     */
    return;
}

static void ExecuteDiskCommand(u32 func)
{
    if (AXP_SELECTED_EXISTS(deviceBlk, func))
    {
        AXP_SELECTED_DISK(deviceBlk, func).commandInProgress = false;
    }
    else
    {
        switch((AXP_ATA_Commands) AXP_SELECTED_DISK(deviceBlk, func).command)
        {
            case ATA_Nop:                   /* 0x00 */
                AXP_SELECTED_DISK(deviceBlk, func).error.error = false;
                AXP_SELECTED_DISK(deviceBlk, func).error.commandAborted = true;
                AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
                AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = true;
                AXP_SELECTED_DISK(deviceBlk, func).status.deviceFault = true;
                AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                AXP_SELECTED_DISK(deviceBlk, func).status.error = true;
                AXP_SELECTED_DISK(deviceBlk, func).commandInProgress = false;
                /* TODO: Raise an interrupt */
                break;

            /* case ATA_CFAReqExtendErr:       0x03 */

            case ATA_DeviceReset:           /* 0x08 */

                /*
                 * The ATAPI Specification says that non-packet devices must not respond to device
                 * reset.  However, by allowing it, Tru64 recognizes the device properly.
                 */
                AXP_SELECTED_DISK(deviceBlk, func).commandInProgress = false;
                if (AXP_SELECTED(deviceBlk, func) == false)
                {
                    AXP_DISK(deviceBlk, func, 0).error.badBlock = false;
                    AXP_DISK(deviceBlk, func, 0).status.deviceReady = false;
                    AXP_DISK(deviceBlk, func, 0).status.deviceFault = false;
                    AXP_DISK(deviceBlk, func, 0).status.seekComplete = false;
                    AXP_DISK(deviceBlk, func, 0).status.dataXferRq = false;
                    AXP_DISK(deviceBlk, func, 0).status.dataCorrected = false;
                    AXP_DISK(deviceBlk, func, 0).status.error= false;
                    if (AXP_CONTROLLER(deviceBlk, func).driveCount == 2)
                    {
                        AXP_DISK(deviceBlk, func, 1).error.badBlock = false;
                        AXP_DISK(deviceBlk, func, 1).status.deviceReady = false;
                        AXP_DISK(deviceBlk, func, 1).status.deviceFault = false;
                        AXP_DISK(deviceBlk, func, 1).status.seekComplete = false;
                        AXP_DISK(deviceBlk, func, 1).status.dataXferRq = false;
                        AXP_DISK(deviceBlk, func, 1).status.dataCorrected = false;
                        AXP_DISK(deviceBlk, func, 1).status.error= false;
                    }
                }
                else
                {
                    AXP_SELECTED_DISK(deviceBlk, func).error.badBlock = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.deviceFault = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.seekComplete = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.dataCorrected = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.error= false;
                }
                SetSignature(func, AXP_SELECTED(deviceBlk, func));

                /* TODO: Is this enough to identify a cdrom */
                if (AXP_SELECTED_DISK(deviceBlk, func).readOnly == false)
                {
                    AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = true;
                }
                AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
                break;

            case ATA_CalibrateDrive:        /* 0x10     not in ATA Extensions Documentation */
                AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
                AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = true;
                AXP_SELECTED_DISK(deviceBlk, func).status.deviceFault = false;
                AXP_SELECTED_DISK(deviceBlk, func).status.seekComplete = true;
                AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                AXP_SELECTED_DISK(deviceBlk, func).head = 0;
                AXP_SELECTED_DISK(deviceBlk, func).commandInProgress = false;
                /* TODO raise interrupt */
                break;

            case ATA_ReadRetry:             /* 0x20 */
            case ATA_ReadNoRetry:           /* 0x21     not in ATA Extensions Documentation */
                if (AXP_SELECTED_DISK(deviceBlk, func).commandCycle == 0)
                {
                    /* Fix the 0 == 256 case. */
                    if (AXP_SELECTED_DISK(deviceBlk, func).sectorCount == 0)
                    {
                        AXP_SELECTED_DISK(deviceBlk, func).sectorCount = 256;
                    }
                }
                if (AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq)
                {
                    if (AXP_SELECTED_DISK(deviceBlk, func).lbaMode)
                    {
                        u32 lba;

                        lba = (AXP_SELECTED_DISK(deviceBlk, func).head << 24) |
                              (AXP_SELECTED_DISK(deviceBlk, func).lba.cylinderNo << 8) |
                              (AXP_SELECTED_DISK(deviceBlk, func).sectorNumber);
                        /* TODO: Seek to block */
                        /* TODO: Read blocks */
                        AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
                        AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = true;
                        AXP_SELECTED_DISK(deviceBlk, func).status.deviceFault = false;
                        AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = true;
                        AXP_SELECTED_DISK(deviceBlk, func).status.error = false;
                        AXP_CONTROLLER(deviceBlk, func).dataPtr = 0;
                        AXP_CONTROLLER(deviceBlk, func).dataSize = 256;

                        /* Prepare for next sector */
                        AXP_SELECTED_DISK(deviceBlk, func).sectorCount--;
                        if (AXP_SELECTED_DISK(deviceBlk, func).sectorCount == 0)
                        {
                            AXP_SELECTED_DISK(deviceBlk, func).commandInProgress = false;
                            if (AXP_SELECTED_DISK(deviceBlk, func).readOnly)
                            {
                                SetSignature(func, AXP_SELECTED(deviceBlk, func));  /* Per 9.1 */
                            }
                        }
                        else
                        {

                            /*
                             * Set the next block to be read and increment the lba.
                             */
                            AXP_SELECTED_DISK(deviceBlk, func).sectorNumber++;
                            if (AXP_SELECTED_DISK(deviceBlk, func).sectorNumber > 255)
                            {
                                AXP_SELECTED_DISK(deviceBlk, func).sectorNumber = 0;
                                AXP_SELECTED_DISK(deviceBlk, func).lba.cylinderNo++;
                                if (AXP_SELECTED_DISK(deviceBlk, func).lba.cylinderNo > 65535)
                                {
                                    AXP_SELECTED_DISK(deviceBlk, func).lba.cylinderNo = 0;
                                    AXP_SELECTED_DISK(deviceBlk, func).head++;
                                }
                            }
                        }
                    }
                    /* TODO: raise interrupt */
                }
                break;

    /*
     * TODO: See what code is replicated or nearly replicated and make it one or more functions.
     */
            case ATA_WriteRetry:            /* 0x30 */
            case ATA_WriteNoRetry:          /* 0x31     not in ATA Extensions Documentation */

                /*
                 * First time in here for this command, so make some initial checks and
                 * get things started.
                 */
                if (AXP_SELECTED_DISK(deviceBlk, func).commandCycle == 0)
                {
                    if (AXP_SELECTED_DISK(deviceBlk, func).readOnly)
                    {
                        CommandAborted(func, AXP_SELECTED_DISK(deviceBlk, func).command);
                    }
                    else
                    {
                        AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = true;
                        AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
                        AXP_CONTROLLER(deviceBlk, func).dataSize = 256;
                        if (AXP_SELECTED_DISK(deviceBlk, func).sectorCount == 0)
                        {
                            AXP_SELECTED_DISK(deviceBlk, func).sectorCount = 256;
                        }
                    }
                }

                /*
                 * At this point, we should be getting some data with which to play.
                 */
                else if ((AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq == false) &&
                         (AXP_SELECTED_DISK(deviceBlk, func).lbaMode))
                {
                    u32 lba;

                    lba = (AXP_SELECTED_DISK(deviceBlk, func).head << 24) |
                          (AXP_SELECTED_DISK(deviceBlk, func).lba.cylinderNo << 8) |
                          (AXP_SELECTED_DISK(deviceBlk, func).sectorNumber);
                    /* TODO: Seek to block */
                    /* TODO: Write blocks */
                    AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = true;
                    AXP_SELECTED_DISK(deviceBlk, func).status.deviceFault = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = true;
                    AXP_SELECTED_DISK(deviceBlk, func).status.error = false;
                    AXP_CONTROLLER(deviceBlk, func).dataPtr = 0;

                    /* Get ready for the next sector */
                    AXP_SELECTED_DISK(deviceBlk, func).sectorCount--;
                    if (AXP_SELECTED_DISK(deviceBlk, func).sectorCount == 0)
                    {

                        /* Nothing more to do */
                        AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                        AXP_SELECTED_DISK(deviceBlk, func).commandInProgress = false;
                    }
                    else
                    {

                        /*
                         * Set the next block to be write and increment the lba.
                         */
                        AXP_SELECTED_DISK(deviceBlk, func).sectorNumber++;
                        if (AXP_SELECTED_DISK(deviceBlk, func).sectorNumber > 255)
                        {
                            AXP_SELECTED_DISK(deviceBlk, func).sectorNumber = 0;
                            AXP_SELECTED_DISK(deviceBlk, func).lba.cylinderNo++;
                            if (AXP_SELECTED_DISK(deviceBlk, func).lba.cylinderNo > 65535)
                            {
                                AXP_SELECTED_DISK(deviceBlk, func).lba.cylinderNo = 0;
                                AXP_SELECTED_DISK(deviceBlk, func).head++;
                            }
                        }
                    }
                    /* TODO: raise interrupt */
                }
                break;

            /* case ATA_CFAWriteWOErase:       0x38 */
            /* case ATA_ReadVerifyRetry:       0x40 */
            /* case ATA_ReadVerifyNoRetry:     0x41     not in ATA Extensions Documentation */

            case ATA_Seek:                  /* 0x70 */
                if (AXP_SELECTED_DISK(deviceBlk, func).readOnly)
                {
                    CommandAborted(func, AXP_SELECTED_DISK(deviceBlk, func).command);
                }
                else
                {
                    AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = true;
                    AXP_SELECTED_DISK(deviceBlk, func).status.deviceFault = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.seekComplete = true;
                    AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.error = false;
                    AXP_SELECTED_DISK(deviceBlk, func).commandInProgress = false;
                    /* TODO: raise interrupt */
                }
                break;

            /* case ATA_CFAXlateSector:        0x87 */

            case ATA_ExecuteDevDiags:       /* 0x90 */
                /* TODO: need to figure this one out */
                break;

            case ATA_InitDevParams:         /* 0x91 */
                AXP_SELECTED_DISK(deviceBlk, func).commandInProgress = false;
                if (AXP_SELECTED_DISK(deviceBlk, func).readOnly)
                {
                    CommandAborted(func, AXP_SELECTED_DISK(deviceBlk, func).command);
                }
                else
                {
                    u8 heads, sectors = 0;

                    /*
                     * TODO: We need the actual number of device heads and sectors.
                     */
                    if ((heads == AXP_SELECTED_DISK(deviceBlk, func).head + 1) &&
                        (sectors == AXP_SELECTED_DISK(deviceBlk, func).sectorCount))
                    {
                        AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
                        AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = true;
                        AXP_SELECTED_DISK(deviceBlk, func).status.deviceFault = false;
                        AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                        AXP_SELECTED_DISK(deviceBlk, func).status.error = false;
                    }
                    else
                    {
                        AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
                        AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = true;
                        AXP_SELECTED_DISK(deviceBlk, func).status.deviceFault = false;
                        AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                        AXP_SELECTED_DISK(deviceBlk, func).status.error = true;
                        AXP_SELECTED_DISK(deviceBlk, func).error.commandAborted = true;
                    }
                    /* TODO: raise interrupt */
                }
                break;

            /* case ATA_DownloadMicrocode:     0x92 */

            case ATA_Packet:                /* 0xa0 */
                break;

            case ATA_IdentifyPacketDev:     /* 0xa1 */
                break;

            /* case ATA_Service:               0xa2 */
            /* case ATA_Smart:                 0xb0 */
            /* case ATA_CFAErase:              0xc0 */

            case ATA_ReadMultiple:          /* 0xc4 */
                break;

            case ATA_WriteMultiple:         /* 0xc5 */
                break;

            case ATA_SetMultipleMode:       /* 0xc6 */
                if (AXP_SELECTED_DISK(deviceBlk, func).readOnly)
                {
                    CommandAborted(func, AXP_SELECTED_DISK(deviceBlk, func).command);
                }
                else
                {
                    AXP_SELECTED_DISK(deviceBlk, func).multipleSize =
                        AXP_SELECTED_DISK(deviceBlk, func).sectorCount;
                    AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = true;
                    AXP_SELECTED_DISK(deviceBlk, func).status.deviceFault = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                    AXP_SELECTED_DISK(deviceBlk, func).status.error = false;
                    AXP_SELECTED_DISK(deviceBlk, func).error.commandAborted = true;
                    /* TODO: raise interrupt */
                }
                break;

            case ATA_ReadDMAQ:              /* 0xc7 */
            case ATA_ReadDMA:               /* 0xc8 */
            case ATA_ReadDMAOrig:           /* 0xc9 */
                break;

            case ATA_WriteDMA:              /* 0xca */
            case ATA_WriteDMAOrig:          /* 0xcb */
            case ATA_WriteDMAQ:             /* 0xcc */
                break;

            /* case ATA_CFAWriteMultWOErase:   0xcd */
            /* case ATA_GetMediaStatus:        0xda */
            /* case ATA_MediaLock:             0xde */
            /* case ATA_MediaUnlock:           0xdf */
            /* case ATA_ReadBuffer:            0xe4 */

            case ATA_CheckPowerMode:        /* 0xe5 */
                break;

            case ATA_StandbyImmediate:      /* 0xe0 */
            case ATA_IdleImmediate:         /* 0xe1 */
            case ATA_Standby:               /* 0xe2 */
            case ATA_Idle:                  /* 0xe3 */
            case ATA_Sleep:                 /* 0xe6 */
            case ATA_FlushCache:            /* 0xe7 */
            case ATA_FlushCacheExt:         /* 0xea */
                AXP_SELECTED_DISK(deviceBlk, func).status.busy = false;
                AXP_SELECTED_DISK(deviceBlk, func).status.deviceReady = true;
                AXP_SELECTED_DISK(deviceBlk, func).status.dataXferRq = false;
                AXP_SELECTED_DISK(deviceBlk, func).status.error = false;
                AXP_SELECTED_DISK(deviceBlk, func).error.commandAborted = false;
                /* TODO: raise interrupt */
                break;

            /* case ATA_WriteBuffer:           0xe8 */

            case ATA_IdentifyDevice:        /* 0xec */
                break;

            /* case ATA_MediaEject:            0xed */

            case ATA_SetFeatures:           /* 0xef */
                break;

            /* case ATA_SecuritySetPwd:        0xf1 */
            /* case ATA_SecurityUnlock:        0xf2 */
            /* case ATA_SecurityErasePrepare:  0xf3 */
            /* case ATA_SecurityEraseUnit:     0xf4 */
            /* case ATA_SecurityFreezeLock:    0xf5 */
            /* case ATA_SecurityDisablePwd:    0xf6 */
            /* case ATA_ReadNativeMaxAddr:     0xf8 */
            /* case ATA_SetMaxAddr:            0xf9 */

            default:
                break;
        }
    }

    /*
     * Return back to the caller.
     */
    return;
}

static void *AXP_IDE_Main()
{
    u32 func;   /* TODO: Get this from the call arguments */
    int pthreadStatus = 0;

    while (pthreadStatus == 0)
    {
        pthreadStatus = pthread_mutex_lock(&AXP_CONTROLLER(deviceBlk, func).controllerMutex);
        if (pthreadStatus == 0)
        {
            pthreadStatus = pthread_cond_wait(&AXP_CONTROLLER(deviceBlk, func).controllerCond,
                                              &AXP_CONTROLLER(deviceBlk, func).controllerMutex);
        }
        if (pthreadStatus == 0)
        {
            if (AXP_SELECTED_DISK(deviceBlk, func).commandInProgress)
            {
                ExecuteDiskCommand(func);
            }
            AXP_SELECTED_DISK(deviceBlk, func).altStatus =
                AXP_SELECTED_DISK(deviceBlk, func).status.status;
        }
    }

    /*
     * Return back to the caller.
     */
    return NULL;
}
