/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This source file contains the code to implement the Aladdin M1543C South
 *  Bridge PCI-ISA Bridge device for the Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 27-Sep-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include "clocks/AXP_DS12887A_TOYClockDefs.h"
#include "clocks/AXP_8254_PITDefs.h"
#include "clocks/AXP_8259A_PICDefs.h"
#include "com-util/AXP_PCIAPI.h"
#include "p-chip/devices/Ali_M1543C/AXP_M1543C.h"
#include "p-chip/devices/Ali_M1543C/LPT/AXP_LPTDefs.h"


static const AXP_PCI_ConfigData configBase =
{
    .type0.common.vendorID = AXP_PCI_ALADDIN_VENDOR_ID, /* Index 01h-00h, RO */
    .type0.common.deviceID = AXP_PCI_M1543C_DEVICE_ID,  /* Index 03h-02h, RO */
    .type0.common.command.ioSpace = 0x1,                /* Index 05h-04h, RO */
    .type0.common.command.memorySpace = 0x1,
    .type0.common.command.busMaster = 0x1,
    .type0.common.command.specialCycles = 0x1,
    .type0.common.command.memWriteInvalidEna = 0x0,
    .type0.common.command.vgaPaletteSnoop = 0x0,
    .type0.common.command.res_0 = 0x0,
    .type0.common.command.serrEna = 0x0,
    .type0.common.command.fastB2BEna = 0x0,
    .type0.common.command.iterruptDisable = 0x0,
    .type0.common.command.res_1 = 0x0,
    .type0.common.status.res_0 = 0x0,                   /* Index 07h-06h, RW */
    .type0.common.status.interruptStatus = 0x0,
    .type0.common.status.capabilitiesList = 0x0,
    .type0.common.status.sixtySixMhz = 0x0,
    .type0.common.status.res_1 = 0x0,
    .type0.common.status.fastB2BEna = 0x0,
    .type0.common.status.masterDataParityErr = 0x0,
    .type0.common.status.DEVSELTiming = AXP_PCI_STATUS_DEVSEL_SLOW,
    .type0.common.status.signalTargetAbort = 0x0,
    .type0.common.status.rcvdTargetAbort = 0x0,
    .type0.common.status.rcvdMasterAbort = 0x0,
    .type0.common.status.signalledSystemErr = 0x0,
    .type0.common.status.detectParityErr = 0x0,
    .type0.common.revisionID = AXP_PCI_M1543C_REV_ID,   /* Index 08h, RO */
    .type0.common.programmingInterface = 0x00,          /* Index 0bh-09h, RO */
    .type0.common.subclass = AXP_PCI_06_SUB_ISA,
    .type0.common.class = AXP_PCI_CLASS_BRIDGE,
    .type0.common.cacheLineSize = 0x00,                 /* Index 0dh-0ch, RO */
    .type0.common.latencyTimer = 0x00,
    .type0.common.headerType.headerType = 0x00,         /* Index 0eh, RO */
    .type0.common.bist.BIST = 0x00,                     /* Index 2bh-0fh, RO */
    .type0.bar[0].BAR = 0,
    .type0.bar[1].BAR = 0,
    .type0.bar[2].BAR = 0,
    .type0.bar[3].BAR = 0,
    .type0.bar[4].BAR = 0,
    .type0.bar[5].BAR = 0,
    .type0.cardbusCISPtr = 0,
    .type0.subsystemVendorID = 0x0000,                  /* Index 2dh-2ch, RW */
    .type0.subsystemID = 0x0000,                        /* Index 2fh-2eh, RW */
    .type0.ROMBaseAddr = 0,                             /* Index 3fh-30h, RO */
    .type0.capabilitiesPtr = 0x00,
    .type0.res_0[0] = 0x00,
    .type0.res_0[1] = 0x00,
    .type0.res_0[2] = 0x00,
    .type0.res_1 = 0,
    .type0.interruptLine = 0x00,
    .type0.interruptPIN = 0x00,
    .type0.minGrant = 0x00,
    .type0.maxLatency = 0x00,
    .configLong[16] = 0,                                    /* Index 43h-40h */
    .configLong[17] = 0,                                    /* Index 47h-44h */
    .configLong[18] = 0,                                    /* Index 4bh-48h */
    .configLong[19] = 0,                                    /* Index 4fh-4ch */
    .configLong[20] = 0,                                    /* Index 53h-50h */

    /* Programmable chip select (pin PCSJ) address define. */
    .configLong[21] = 0x00000200,                           /* Index 57h-54h, RO */
    .configLong[22] = 0,                                    /* Index 5bh-58h */
    .configLong[23] = 0,                                    /* Index 5fh-5ch */
    .configLong[24] = 0,                                    /* Index 63h-60h */
    .configLong[25] = 0,                                    /* Index 67h-64h */
    .configLong[26] = 0,                                    /* Index 6bh-68h */
    .configLong[27] = 0,                                    /* Index 6fh-6ch */
    .configLong[28] = 0,                                    /* Index 73h-70h */
    .configLong[29] = 0,                                    /* Index 77h-74h */
    .configLong[30] = 0,                                    /* Index 7bh-78h */
    .configLong[31] = 0,                                    /* Index 7fh-7ch */
    .configLong[32] = 0,                                    /* Index 83h-80h */
    .configLong[33] = 0,                                    /* Index 87h-84h */
    .configLong[34] = 0,                                    /* Index 8bh-88h */
    .configLong[35] = 0,                                    /* Index 8fh-8ch */
    .configLong[36] = 0,                                    /* Index 93h-90h */
    .configLong[37] = 0,                                    /* Index 97h-94h */
    .configLong[38] = 0,                                    /* Index 9bh-98h */
    .configLong[39] = 0,                                    /* Index 9fh-9ch */
    .configLong[40] = 0,                                    /* Index a3h-a0h */
    .configLong[41] = 0,                                    /* Index a7h-a4h */
    .configLong[42] = 0,                                    /* Index abh-a8h */
    .configLong[43] = 0,                                    /* Index afh-ach */
    .configLong[44] = 0,                                    /* Index b3h-b0h */
    .configLong[45] = 0,                                    /* Index b7h-b4h */
    .configLong[46] = 0,                                    /* Index bbh-b8h */
    .configLong[47] = 0,                                    /* Index bfh-bch */
    .configLong[48] = 0,                                    /* Index 63h-c0h */
    .configLong[49] = 0,                                    /* Index 67h-c4h */
    .configLong[50] = 0,                                    /* Index cbh-c8h */
    .configLong[51] = 0,                                    /* Index cfh-cch */
    .configLong[52] = 0,                                    /* Index d3h-d0h */
    .configLong[53] = 0,                                    /* Index d7h-d4h */
    .configLong[54] = 0,                                    /* Index dbh-d8h */
    .configLong[55] = 0,                                    /* Index dfh-dch */
    .configLong[56] = 0,                                    /* Index e3h-e0h */
    .configLong[57] = 0,                                    /* Index e7h-e4h */
    .configLong[58] = 0,                                    /* Index ebh-e8h */
    .configLong[59] = 0,                                    /* Index efh-ech */
    .configLong[60] = 0,                                    /* Index f3h-f0h */
    .configLong[61] = 0,                                    /* Index f7h-f4h */
    .configLong[62] = 0,                                    /* Index fbh-f8h */
    .configLong[63] = 0                                     /* Index ffh-fch */
};

/*
 * The following is used to mask out read-only bits to be used when writing to
 * a register.
 */
static const AXP_PCI_ConfigData configMask =
{
    .configLong[0] = 0,
    .configLong[1] = 0, /* bits 13,12 of high word cleared by writing 1 */
    .configLong[2] = 0,
    .configLong[3] = 0,
    .configLong[4] = 0,
    .configLong[5] = 0,
    .configLong[6] = 0,
    .configLong[7] = 0,
    .configLong[8] = 0,
    .configLong[9] = 0,
    .configLong[10] = 0,
    .configLong[11] = 0,
    .configLong[12] = 0,
    .configLong[13] = 0,
    .configLong[14] = 0,
    .configLong[15] = 0,
    .configLong[16] = 0xffcfff7f,
    .configLong[17] = 0xff00cbdf,
    .configLong[18] = 0xffffffff,
    .configLong[19] = 0x000000ff,
    .configLong[20] = 0xcfff8fff,
    .configLong[21] = 0xe0ffff00,
    .configLong[22] = 0x020f0d7f,
    .configLong[23] = 0xffe0027f,
    .configLong[24] = 0,
    .configLong[25] = 0,
    .configLong[26] = 0,
    .configLong[27] = 0x00ffbf00,
    .configLong[28] = 0xffefefff,
    .configLong[29] = 0x1fffffdf,
    .configLong[30] = 0,
    .configLong[31] = 0,
    .configLong[32] = 0,
    .configLong[33] = 0,
    .configLong[34] = 0,
    .configLong[35] = 0,
    .configLong[36] = 0,
    .configLong[37] = 0,
    .configLong[38] = 0,
    .configLong[39] = 0,
    .configLong[40] = 0,
    .configLong[41] = 0,
    .configLong[42] = 0,
    .configLong[43] = 0,
    .configLong[44] = 0,
    .configLong[45] = 0,
    .configLong[46] = 0,
    .configLong[47] = 0,
    .configLong[48] = 0,
    .configLong[49] = 0,
    .configLong[50] = 0,
    .configLong[51] = 0,
    .configLong[52] = 0,
    .configLong[53] = 0,
    .configLong[54] = 0,
    .configLong[55] = 0,
    .configLong[56] = 0,
    .configLong[57] = 0,
    .configLong[58] = 0,
    .configLong[59] = 0,
    .configLong[60] = 0,
    .configLong[61] = 0,
    .configLong[62] = 0,
    .configLong[63] = 0
};

static AXP_M1543C deviceBlk;

typedef union
{
    struct
    {
        u8 version_patch;
        u8 version_minor;
        u8 version_major;
        u8 res;
    };
    u32 version;
} _AXP_PCI_API_Supported_Version;

static _AXP_PCI_API_Supported_Version supported_version =
{
    AXP_PCI_VER_PAT,
    AXP_PCI_VER_MIN,
    AXP_PCI_VER_MAJ,
    0
};

static u32 ConfigRead(u16 func, u32 address, u32 dsize);
static void ConfigWrite(u16 func, u32 address, u32 dsize, u32 data);
static u32 MemoryRead(u16 func, u32 address, u32 dsize);
static void MemoryWrite(u16 func, u32 address, u32 dsize, u32 data);
static void Reset(void);

int initialize_device(AXP_PCIAPI_Device_CB *pci_device_info)
{
    int retVal = EXIT_SUCCESS;
    _AXP_PCI_API_Supported_Version supplied_version;
    char *lpt_filename = NULL;

    /*
     * Make sure that this is a version we can support.  Support is backward
     * compatible, so a caller that supports a newer version, also supports the
     * older one.  Conversely, a caller that supports an older version, cannot
     * support a newer version.
     */
    supplied_version.res = 0;
    supplied_version.version_major = pci_device_info->version_major;
    supplied_version.version_minor = pci_device_info->version_minor;
    supplied_version.version_patch = pci_device_info->version_patch;
    if (supported_version <= supplied_version)
    {
        int ii = 0;

        /* Dump the arguments supplied on the call */
        printf("AXP_1543C PCI Device Initialization called:\n");
        while (pci_device_info->args[ii].type != Empty)
        {
            switch (pci_device_info->args[ii].type)
            {
                case FileName:
                    printf("    Filename: %s\n",
                           pci_device_info->args[ii].name);
                    lpt_filename = pci_device_info->args[ii].name;
                    break;

                case Type:
                    printf("    Type: %u\n",
                           pci_device_info->args[ii].devType);
                    break;

                case ReadOnly:
                    printf("    ReadOnly: %s\n",
                           pci_device_info->args[ii].readOnly ? "Yes" : "No");
                    break;

                default:
                    printf("    Unknown Argument: %d\n",
                           pci_device_info->args[ii].type);
                    break;
            }
            ii++;
        }

        /*
         * This is the best place to initialize the Intel 8254 Chip and Parallel Port.
         */
        AXP_8254_Initialize();
        AXP_LPT_Initialize(lpt_filename);

        /* Do a Reset on this device */
        Reset();

        /*
         * Return the Base Address Register data (legacy, so not in config
         * data).
         */
        for (ii = 0; ii < AXP_PCI_MAX_FUNCTIONS; ii++)
        {
            pci_device_info->pciBaseAddressRegisters[ii] =
                deviceBlk.currentConfig.type0.bar[ii];
        }
        pci_device_info->pciExpansionROMBAR =
            deviceBlk.currentConfig.type0.ROMBaseAddr;

        /*
         * Return the addresses of all the entrypoints.
         */
        pci_device_info->PCIConfigRead = ConfigRead;
        pci_device_info->PCIConfigWrite = ConfigWrite;
        pci_device_info->PCIMemRead = MemoryRead;
        pci_device_info->PCIMemWrite = MemoryWrite;
        pci_device_info->PCIBarRead = NULL;
        pci_device_info->PCIBarWrite = NULL;
        pci_device_info->PCIReset = Reset;
    }
    else
    {
        retVal = EXIT_FAILURE;
    }

    /*
     * Return back to the caller.
     */
    return (retVal);
}

static u32 ConfigRead(u16 func, u32 index, u32 length)
{
    u32 retVal = 0;
    printf("M1543C ConfigRead(%u, %u, %u) called\n", func, index, length);

    switch(length)
    {
        case 1:
            retVal = deviceBlk.currentConfig.configByte[index];
            break;

        case 2:
            retVal = deviceBlk.currentConfig.configWord[index / 2];
            break;

        case 4:
            retVal = deviceBlk.currentConfig.configLong[index / 4];
            break;
    }
    printf("M1543C ConfigRead returning %0*x\n", length*2, retVal);
    return retVal;
}

static void ConfigWrite(u16 func, u32 index, u32 length, u32 data)
{
    printf("M1543C ConfigWrite(%u, %u, %u, %u) called\n", func, index, length, data);

    switch(length)
    {
        case 1:
            {
                u8 mask = configMask.configByte[index];
                u8 configData = data & mask;
                u8 configKeep =
                        deviceBlk.currentConfig.configByte[index] & ~mask;

                deviceBlk.currentConfig.configByte[index] =
                    configData | configMask;
            }
            break;

        case 2:
            {
                u16 mask = configMask.configWord[index / 2];
                u16 configData = data & mask;
                u16 configKeep =
                        deviceBlk.currentConfig.configWord[index / 2] & ~mask;

                deviceBlk.currentConfig.configWord[index / 2] =
                    configData | configMask;
            }
            break;

        case 4:
            {
                u32 mask = configMask.configLong[index / 4];
                u32 configData = data & mask;
                u32 configKeep =
                        deviceBlk.currentConfig.configLong[index / 4] & ~mask;

                deviceBlk.currentConfig.configLong[index / 4] =
                    configData | configMask;
            }
            break;
    }

    printf("M1543C ConfigWrite returning\n");
    return;
}

/*
 * MemoryRead
 *  This function is called to perform a memory read for various functions.  For
 *  this emulation, only byte-access is supported, with the exception of reading
 *  from the IACK address. where the length does not matter.
 *
 * Input Parameters:
 *  func:
 *      An unsigned word containing the range or ports (functions) to to be
 *      read from.  This parameter has the following values:
 *
 *          AXP_M1543C_PORT_61:     I/O port 0x61
 *          AXP_M1543C_TOY:         I/O ports 0x70-0x73 (Time-Of-Year Clock)
 *          AXP_M1543C_PIT:         I/O ports 0x40-0x43 (Programmable Interrupt Timer)
 *          AXP_M1543C_PRI_PIC      I/O ports 0x20-0x21 (Primary Programmable Interrupt Controller)
 *          AXP_M1543C_SEC_PIC      I/O ports 0xa0-0xa1 (Secondary (cascaded) Programmable Interrupt Controller)
 *          AXP_M1543C_IACK_ADDR    PCI IACK address (interrupt vector)
 *          AXP_M1543C_LPT          I/O ports 0x3bc-0x3bf (Line Printer (Parallel) Port)
 *          AXP_M1543C_EDGE_PIC     I/O ports 0x4d0-0x4d1 (Edge/Level Register of Programmable Interrupt Controller)
 *
 *  index:
 *      An unsigned quadword containing the index into the register to be read.
 *  length:
 *      An unsigned longword containing the number of bytes to be read (must be
 *      1, but is ignored for AXP_ALI_IACK_ADDR).
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  An unsigned longword containing the value read from the indicated register.
 */
static u32 MemoryRead(u16 func, u64 index, u32 length)
{
    u32 retVal = 0;
    u8 toggle;

    printf("MemoryRead(%u, %u, %u) called\n", func, index, length);

    if ((length != 1) && (func != AXP_M1543C_IACK_ADDR))
    {
        printf("InvalidArgument: %s: Length %d reading from memory range # %d "
                "at address %02x\n", "devid_string", length, func, index);
    }

    switch(func)
    {
        case AXP_M1543C_PORT_61:

            /*
             * The 5th bit toggles following every refresh cycle.
             * The 6th bit follows the Timer Counter 2 OUT status.
             * All the other bits are left alone.
             */
            toggle = deviceBlk.nmi_status_control_reg & 0x10;
            toggle ^= 0x10;
            deviceBlk.nmi_status_control_reg &= ~0x20;
            deviceBlk.nmi_status_control_reg |= toggle;
            if (AXP_8254_GetStatusOut(2))
            {
                deviceBlk.nmi_status_control_reg |= 0x20;
            }
            retVal = deviceBlk.nmi_status_control_reg;
            break;

        case AXP_M1543C_TOY:
            AXP_DS1288A_Read((u8) (index & 0x7f), (u8 *) &retVal);
            break;

        case AXP_M1543C_PIT:
            retVal = AXP_8254_Read(index);
            break;

        case AXP_M1543C_PRI_PIC:
            retVal = AXP_8259A_Read(true, index);
            break;

        case AXP_M1543C_SEC_PIC:
            retVal = AXP_8259A_Read(false, index);
            break;

        case AXP_M1543C_IACK_ADDR:
            retVal = AXP_8259A_ReadIRQ(true);
            break;

        case AXP_M1543C_LPT:
            retVal = AXP_LPT_Read(index);
            break;

        case AXP_M1543C_EDGE_PIC:
            retVal = AXP_8259A_ReadEdge(true, index);
            break;
    }

    /*
     * Return the results of this call back to the caller.
     */
    return retVal;
}

/*
 * MemoryWrite
 *  This function is called to perform a memory write for various functions.
 *  For this emulation, only byte-access is supported.
 *
 * Input Parameters:
 *  func:
 *      An unsigned word containing the range or ports (functions) to to be
 *      read from.  This parameter has the following values:
 *
 *          AXP_M1543C_PORT_61:    I/O port 0x61
 *          AXP_M1543C_TOY:        I/O ports 0x70-0x73 (Time-Of-Year Clock)
 *          AXP_M1543C_PIT:        I/O ports 0x40-0x43 (Programmable Interrupt Timer)
 *          AXP_M1543C_PRI_PIC     I/O ports 0x20-0x21 (Primary Programmable Interrupt Controller)
 *          AXP_M1543C_SEC_PIC     I/O ports 0xa0-0xa1 (Secondary (cascaded) Programmable Interrupt Controller)
 *          AXP_M1543C_LPT         I/O ports 0x3bc-0x3bf (Line Printer (Parallel) Port)
 *          AXP_M1543C_EDGE_PIC    I/O ports 0x4d0-0x4d1 (Edge/Level Register of Programmable Interrupt Controller)
 *
 *  index:
 *      An unsigned quadword containing the index into the register to be read.
 *  length:
 *      An unsigned longword containing the number of bytes to be read (must be
 *      1, but is ignored for AXP_ALI_IACK_ADDR).
 *  data:
 *      An unsigned longword containing the data to be written to the register.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
static void MemoryWrite(u16 func, u64 index, u32 length, u64 data)
{
    u8 nmi_save;

    printf("MemoryWrite(%u, %u, %u, %u) called\n", func, index, length, data);

    if (length != 1)
    {
        printf("InvalidArgument: %s: Length %d reading from memory range # %d "
                "at address %02x\n", "devid_string", length, func, index);
    }

    switch(func)
    {
        case AXP_M1543C_PORT_61:

            /*
             * The first nibble is R/W and the second is R only.  Make sure to preserve the high nibble.
             */
            nmi_save = deviceBlk.nmi_status_control_reg & 0xf0;
            deviceBlk.nmi_status_control_reg = nmi_save | ((u8) data & 0x0f);
            break;

        case AXP_M1543C_TOY:
            AXP_DS1288A_Write((u8) (index & 0x7f), (u8) (data & 0x7f));
            break;

        case AXP_M1543C_PIT:
            AXP_8254_Write(index, data);
            break;

        case AXP_M1543C_PRI_PIC:
            AXP_8529A_Write(true, (index == 1), data);
            break;

        case AXP_M1543C_SEC_PIC:
            AXP_8529A_Write(false, (index == 1), data);
            break;

        case AXP_M1543C_LPT:
            AXP_LPT_Write(index, data);
            break;

        case AXP_M1543C_EDGE_PIC:
            AXP_8529A_WriteEdge(true, (index == 1), data);
            break;
    }
    return;
}

static void Reset(void)
{
    printf("[M1543C] Reset() called\n");

    memcpy(deviceBlk.currentConfig, configBase, sizeof(AXP_PCI_ConfigData));

    /* Reset the DS12887A chip */
    AXP_DS12887A_Reset();

    /* Reset the 8259A chip */
    AXP_8529A_Reset();

    /* Reset Parallel Port */
    AXP_LPT_Reset();

    /* Return back to the caller */
    return;
}
