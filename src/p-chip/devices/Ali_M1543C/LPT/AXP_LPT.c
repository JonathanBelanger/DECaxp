/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 08-Nov-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include "p-chip/devices/Ali_M1543C/LPT/AXP_LPT.h"
#include "clocks/AXP_8259A_PICDefs.h"

static AXP_LPT_DataPort data_port;
static AXP_LPT_StatusPort status_port;
static AXP_LPT_ControlPort control_port;
static char *lpt_fn = NULL;
static FILE *lpt_fp = NULL;

/*
 * AXP_LPT_Initialize
 *  This function ...
 */
bool
AXP_LPT_Initialize(char *lpt_filename)
{
    bool retVal = true;

    if (lpt_filename != NULL)
    {
        lpt_fn = AXP_Allocate_Block(-(strlen(lpt_filename) + 1), NULL);
        strcpy(lpt_fn, lpt_filename);
        lpt_fp = fopen(lpt_fn, "a");
        if (lpt_fp == NULL)
        {
            retVal = false;
        }
    }

    /*
     * Return the results back to the caller.
     */
    return(retVal);
}

/*
 * AXP_LPT_Reset
 *  This function ...
 */
void
AXP_LPT_Reset(void)
{
    data_port = 0xff;
    status_port.reg = 0xd8;     /* not busy, not acked, selected, not_error */
    control_port.reg = 0xcc;    /* mbo, selected, initiate_output  */

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_LPT_Write
 *  This function ...
 */
void
AXP_LPT_Write(u8 index, u8 data)
{
    AXP_LPT_ControlPort ctrl_val;

    switch(index)
    {
        case AXP_LPT_DATA_PORT:
            data_port = data;
            break;

        case AXP_LPT_CONTROL_PORT:
            ctrl_val.reg = data | 0xc0;
            if (ctrl_val.initiate_output == 0)
            {
                status_port.not_busy = 1;
                status_port.not_acked = 1;
                status_port.selected = 1;
                status_port.not_error = 1;
            }
            else if (ctrl_val.selected == 1)
            {
                if (ctrl_val.strobe == 1)
                {
                    status_port.not_busy = 0;
                    if ((control_port.strobe == 0) && (lpt_fp != NULL))
                    {
                        fputc(data_port, lpt_fp);
                    }
                }
                else
                {
                    if (control_port.irq_enable == 1)
                    {
                        AXP_8259A_WriteIRQ(true, 7);
                    }
                }
            }
            control_port.reg = ctrl_val.reg;
            break;
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_LPT_Read
 *  This function ...
 */
u8
AXP_LPT_Read(u8 index)
{
    u8 retVal = 0xff;

    switch(index)
    {
        case AXP_LPT_DATA_PORT:
            retVal = data_port;
            break;

        case AXP_LPT_STATUS_PORT:
            retVal = status_port.reg;
            if ((status_port.not_busy == 0) && (control_port.strobe == 0))
            {
                if (status_port.not_acked)
                {
                    status_port.not_acked = 0;
                }
                else
                {
                    status_port.not_acked = 1;
                    status_port.not_busy = 1;
                }
            }
            break;

        case AXP_LPT_CONTROL_PORT:
            retVal = control_port.reg;
            break;
    }

    /*
     * Return the results back to the caller.
     */
    return(retVal);
}
