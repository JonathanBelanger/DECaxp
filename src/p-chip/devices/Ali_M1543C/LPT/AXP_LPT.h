/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 08-Nov-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_LPT_H_
#define _AXP_LPT_H_

#include "com-util/AXP_Common_Include.h"

typedef u8 AXP_LPT_DataPort;
typedef union
{
    u8 reg;
    struct
    {
        u8 mbz : 3;
        u8 not_error : 1;
        u8 selected : 1;
        u8 paper_end : 1;
        u8 not_acked : 1;
        u8 not_busy : 1;
    };
} AXP_LPT_StatusPort;
typedef union
{
    u8 reg;
    struct
    {
        u8 strobe : 1;
        u8 auto_feed : 1;
        u8 initiate_output : 1;
        u8 selected : 1;
        u8 irq_enable : 1;
        u8 input_mode : 1;
        u8 mbo : 2;
    };
} AXP_LPT_ControlPort;

#define AXP_LPT_DATA_PORT 0
#define AXP_LPT_STATUS_PORT 1
#define AXP_LPT_CONTROL_PORT 2

/* NOTE: The Extended Capabilities Ports are not implemented */

#endif /* AXP_LPT_H_ */
