/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions to emulate the Ali M1542C chipset
 *  used in the Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 26-Apr-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_M1543C_DEFS_
#define _AXP_M1543C_DEFS_

#include "com-util/AXP_Common_Include.h"
#include "com-util/AXP_PCI.h"

#define AXP_M1543C_DEVICE_ID    0x1533
#define AXP_M1543C_REV_ID       0xc3

typedef union
{
    u8 ctrl;
    struct
    {
        u8 strobe : 1;
        u8 autofeed : 1;
        u8 initiate : 1;
        u8 select : 1;
        u8 interrupt_req : 1;
        u8 direction : 1;
        u8 res_1 : 2;
    };
} AXP_M1543C_Device_Control_Register;
typedef struct
{
    u8 status;
    struct
    {
        u8 res_1 : 3;
        u8 fault : 1;
        u8 select : 1;
        u8 error : 1;
        u8 ack : 1;
        u8 not_busy : 1;
    };
} AXP_M1543C_Device_Status_Register;

/*
 * Serial Port Registers
 *
 * Register Address:    0h
 * Access (AEN=0):      DLAB 0
 * Abbreviation:        THR
 * Register Name:       Transmit Holding Register
 * Access:              Write
 */
typedef u8 AXP_M1543C_THR;

/*
 * Register Address:    0h
 * Access (AEN=0):      DLAB 0
 * Abbreviation:        RBR
 * Register Name:       Receive Buffer Register
 * Access:              Read
 */
typedef u8 AXP_M1543C_RBR;

/*
 * Register Address:    0h
 * Access (AEN=0):      DLAB 1
 * Abbreviation:        DLL
 * Register Name:       Divisor Latch LSB
 * Access:              Read/Write
 */
typedef u8 AXP_M1543C_DLL;

/*
 * Register Address:    1h
 * Access (AEN=0):      DLAB 1
 * Abbreviation:        DLM
 * Register Name:       Divisor Latch MSB
 * Access:              Read/Write
 */
typedef u8 AXP_M1543C_DLM;

/*
 * Register Address:    1h
 * Access (AEN=0):      DLAB 0
 * Abbreviation:        IER
 * Register Name:       Interrupt Enable Register
 * Access:              Read/Write
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 rcv_data_avail_inter : 1;
        u8 THRE_inter : 1;
        u8 rcvr_line_status_inter : 1;
        u8 modem_status_inter : 1;
        u8 res_1 : 4;
    };
} AXP_M1543C_IER;

/*
 * Register Address:    2h
 * Access (AEN=0):      -
 * Abbreviation:        IIR
 * Register Name:       Interrupt Identification Register
 * Access:              Read
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 no_inter_pending : 1;
        u8 high_inter_pending : 2;
        u8 timeout_pending : 1;
        u8 res_1 : 2;
        u8 FCR_bit_0_set_1 : 1;
        u8 FCR_bit_0_set_2 : 1;
    };
} AXP_M1543C_IIR;

/*
 * Register Address:    2h
 * Access (AEN=0):      -
 * Abbreviation:        FCR
 * Register Name:       FIFO Control Register
 * Access:              Write
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 enable_fifo : 1;
        u8 clear_rcvr_fifo : 1;
        u8 clear_xmit_fifo : 1;
        u8 res_1 : 3;
        u8 rcvr_inter_trigger_lvl : 2;
    };
} AXP_M1543C_FCR;
#define AXP_M1543C_RCVR_FIFO_LVL_01    0
#define AXP_M1543C_RCVR_FIFO_LVL_04    1
#define AXP_M1543C_RCVR_FIFO_LVL_08    2
#define AXP_M1543C_RCVR_FIFO_LVL_14    3

/*
 * Register Address:    3h
 * Access (AEN=0):      -
 * Abbreviation:        LCR
 * Register Name:       Line Control Register
 * Access:              Read/Write
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 data_bits : 2;
        u8 stop_bits : 1;
        u8 parity_enable : 1;
        u8 parity_select :1;
        u8 parity_sticky : 1;
        u8 break_ctrl : 1;
        u8 DLAB : 1;
    };
} AXP_M1543C_LCR;
#define AXP_M1543C_5_BITS  0
#define AXP_M1543C_6_BITS  1
#define AXP_M1543C_7_BITS  2
#define AXP_M1543C_8_BITS  3

/*
 * Register Address:    4h
 * Access (AEN=0):      -
 * Abbreviation:        MCR
 * Register Name:       Modem Control Register
 * Access:              Read/Write
 */
typedef union
{
    u8 reg;
    struct
    {
            u8 DTRJ_force_0 : 1;
            u8 RTSJ_force_0 : 1;
            u8 OUT1 : 1;
            u8 interrupt_enable : 1;
            u8 loopback : 1;
            u8 res_1 : 3;
    };
} AXP_M1543C_MCR;

/*
 * Register Address:    5h
 * Access (AEN=0):      -
 * Abbreviation:        LSR
 * Register Name:       Line Status Register
 * Access:              Read
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 rcv_data_ready : 1;
        u8 overrun_error : 1;
        u8 parity_error : 1;
        u8 framing_error : 1;
        u8 break_interrupt : 1;
        u8 THR_empty : 1;
        u8 TEMT_empty : 1;
        u8 error_present : 1;
    };
} AXP_M1543C_LSR;

/*
 * Register Address:    6h
 * Access (AEN=0):      -
 * Abbreviation:        MSR
 * Register Name:       Modem Status Register
 * Access:              Read
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 DCTS : 1;
        u8 DDSR : 1;
        u8 TERI : 1;
        u8 DDCD : 1;
        u8 not_CTSJ : 1;
        u8 not_DSRJ : 1;
        u8 not_RIJ : 1;
        u8 not_DCDJ : 1;
    };
} AXP_M1543C_MSR;

/*
 * Register Address:    7h
 * Access (AEN=0):      -
 * Abbreviation:        SCR
 * Register Name:       Scratch PadRegister
 * Access:              Read/Write
 */
typedef u8 AXP_M1543C_SCR;
typedef struct
{
    AXP_M1543C_THR thr;
    AXP_M1543C_RBR rbr;
    AXP_M1543C_DLL dll;
    AXP_M1543C_DLM dlm;
    AXP_M1543C_IER ier;
    AXP_M1543C_IIR iir;
    AXP_M1543C_FCR fcr;
    AXP_M1543C_LCR lcr;
    AXP_M1543C_MCR mcr;
    AXP_M1543C_LSR lsr;
    AXP_M1543C_MSR msr;
    AXP_M1543C_SCR scr;
    bool present;
} AXP_M1543C_Serial_Port;

#define AXP_M1543C_RTC_PORTS 4
#define AXP_M1543C_SERIAL_PORTS 4
#define AXP_M1543C_TIMER_COUNTERS 3
typedef struct
{
    u8 timer_channel_counter[AXP_M1543C_TIMER_COUNTERS];/* Ports 40h - 42h */
    u8 timer_command_mode;                              /* Port 43h */
    u8 nmi_status_control_reg;                          /* Port 61h */
    u8 rtc_ports[AXP_M1543C_RTC_PORTS];                 /* Ports 70h - 73h */
    u8 printer_data;
    AXP_M1543C_Device_Control_Register printer_ctrl;
    AXP_M1543C_Device_Status_Register printer_status;
    AXP_PCI_ConfigData currentConfig;
    AXP_M1543C_Serial_Port serial_port[AXP_M1543C_SERIAL_PORTS];
    u32 devId;
} AXP_M1543C;

#define AXP_M1543C_PORT_61     1   /* I/O port 0x61 */
#define AXP_M1543C_TOY         2   /* I/O port 0x70-0x73 (Time Of Year Clock) */
#define AXP_M1543C_PIT         6   /* I/O port 0x40-0x43 (Programmable Interrupt Timer) */
#define AXP_M1543C_PRI_PIC     7   /* I/O port 0x20-0x21 (Primary Programmable Interrupt Controller) */
#define AXP_M1543C_SEC_PIC     8   /* I/O port 0xa0-0xa1 (Secondary (cascaded) Programmable Interrupt Controller) */
#define AXP_M1543C_PRI_DMA     12  /* I/O port 0x00-0x0f (Primary DMA Controller) */
#define AXP_M1543C_SEC_DMA     13  /* I/O port 0xc0-0xdf (Secondary DMA Controller) */
#define AXP_M1543C_IACK_ADDR   20  /* PCI IACK Address (Interrupt Vector) */
#define AXP_M1543C_LPT         27  /* I/O port 0x3bc-0x3bf (Parallel Port) */
#define AXP_M1543C_EDGE_PIC    30  /* I/O port 0x4d0-0x4d1 (Edge/Level Register of Programmable Interrupt Controller) */
#define AXP_M1543C_DMA_LOW     33  /* I/O port 0x80-0x8f (DMA Controller Memory Base Low Page Register) */
#define AXP_M1543C_DMA_HI      34  /* I/O port 0x480-0x48f (DMA Controller Memory Base High Page Register) */

#endif  /* _AXP_M1543C_DEFS_ */
