/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 14-Nov-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include "p-chip/devices/PC16550D/AXP_PC16550D.h"

static AXP_PC16550D consoles[AXP_PC16550D_MAX_CONSOLES];
static u8 console_init_mask = 0;
static bool rcv_thread_started = false;
static pthread_mutex_t console_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t console_receive_thread;

/*
 * Forward prototypes
 */
static void
AXP_PC16550D_Update_IRQ(AXP_PC16550D *console);
static void
AXP_PC16550D_Update_FCR(AXP_PC16550D *console, AXP_PC16550D_FCR *fcr);
static void *
AXP_PC16550D_Main(void *args);

/*
 * AXP_PC16550D_Update_IRQ
 *  This function is called to review the settings of various registers in the
 *  PC16550D UART and determining if an interrupt is pending and raising or
 *  lowering the irq for this device.
 *
 * Input Parameters:
 *  console:
 *      This is a pointer to the structure containing the information required
 *      to maintain the emulation for a PC16550D UART chip.
 *
 * Output Parameters:
 *  console:
 *      This is a pointer to the structure containing the information to be set
 *      based on the setting of various registers.
 *
 * Return Values:
 *  None.
 */
static void
AXP_PC16550D_Update_IRQ(AXP_PC16550D *console)
{
    AXP_PC16550D_IIR temp_iir = {.reg = 0};

    temp_iir.intr_pend = 1;

    /*
     * Determine the setting of the IIR register.
     */
    if (console->ier.elsi &&
        (console->lsr.oe || console->lsr.pe || console->lsr.fe || console->lsr.bi))
    {
        temp_iir.intr_id = AXP_PC16550D_IIR_RLSI;
    }
    else if (console->ier.erbfi)    /* TODO: timeout ipending */
    {
        temp_iir.intr_id = AXP_PC16550D_IIR_CTI;
    }
    else if (console->ier.erbfi && console->lsr.dr &&
             ((console->fcr.enable == 0) ||
              (AXP_PC16550D_CNT(console->rcv) >= console->rcv_intr_lvl)))
    {
        temp_iir.intr_id = AXP_PC16550D_IIR_RDI;
    }
    else if (console->ier.etbei)    /* TODO: thr pending? */
    {
        temp_iir.intr_id = AXP_PC16550D_IIR_THRI;
    }
    else if (console->ier.edssi &&
             (console->msr.cts || console->msr.dsr || console->msr.ri || console->msr.dcd))
    {
        temp_iir.reg = 0;
    }

    /*
     * Set the IIR register based on other settings and make sure that the
     * high nibble set bits are maintained.
     */
    console->iir.reg = temp_iir.reg | (console->iir.reg & 0xf0);

    /*
     * Raise or lower the IRQ.
     */
    if (console->iir.intr_pend == 1)
    {
        /* Raise IRQ */
    }
    else
    {
        /* Lower IRQ */
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_PC16550D_Update_FCR
 *  This function is called to update the FIFO Control Register (FCR), as well
 *  as other registers accordingly.
 *
 * Input Parameters:
 *  fcr:
 *      A pointer to a representation of what the FCR should be set.
 *
 * Output Parameters:
 *  console:
 *      This is a pointer to the structure containing the information to be set
 *      based on the setting of various registers.
 *
 * Return Values:
 *  None.
 */
static void
AXP_PC16550D_Update_FCR(AXP_PC16550D *console, AXP_PC16550D_FCR *fcr)
{
    console->fcr.reg = fcr->reg;
    if (fcr->enable)
    {
        console->iir.fifo_enable = 3;
        switch (fcr->rcv_trigger)
        {
            case AXP_PC16550D_RCV_01:
                console->rcv_intr_lvl = 1;
                break;

            case AXP_PC16550D_RCV_04:
                console->rcv_intr_lvl = 4;
                break;

            case AXP_PC16550D_RCV_08:
                console->rcv_intr_lvl = 8;
                break;

            case AXP_PC16550D_RCV_14:
                console->rcv_intr_lvl = 14;
                break;
        }
    }
    else
    {
        console->iir.fifo_enable = 0;
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_PC16550D_Initialize
 *  This function is called to initialize one of the two possible consoles.
 *
 * Input Parameters:
 *  index:
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  true:   Successfully initialized.
 *  false:  Failed to initialize (already initialized).
 */
bool
AXP_PC16550D_Initialize(int index)
{
    bool retVal = true;

    if ((index > 0) && (index < AXP_PC16550D_MAX_CONSOLES))
    {
        pthread_mutex_lock(&console_mutex);
        if (console_init_mask != 0)
        {
            retVal = AXP_Telnet_Initialize(0);  /* TODO: port */
        }
        if (console_init_mask & (1 << index))
        {
            retVal = false;
            printf("Console %d already initialize!!!");
        }
        else if (retVal == true)
        {
            AXP_PC16550D *console = &consoles[index];

            memset(console, 0, sizeof(AXP_PC16550D));
            console->iir.intr_pend = 1;
            console->mcr.out = AXP_PC16550D_OUT2;
            console->lsr.temt = 1;
            console->lsr.thre = 1;
            console->msr.dcd = 1;
            console->msr.dsr = 1;
            console->msr.cts = 1;
            console->dll = AXP_PC16550D_9600BAUD;
            console->telnet_session = AXP_Telnet_Accept();
            if (console->telnet_session != NULL)
            {
                retVal = AXP_Telnet_Negotiate(console->telnet_session);
                if (retVal != true)
                {
                    AXP_Telnet_Close(&console->telnet_session);
                }
            }
            else
            {
                retVal = false;
            }
        }
        if (rcv_thread_started == false)
        {
            if (pthread_create(&console_receive_thread,
                               NULL,
                               &AXP_PC16550D_Main,
                               NULL) == 0)
            {
                rcv_thread_started = true;
            }
            else
            {
                AXP_Telnet_Close(consoles[index].telnet_session);
                retVal = false;
            }
        }
        if (retVal == true)
        {
            console_init_mask = 1 << index;
        }
        pthread_mutex_unlock(&console_mutex);
    }

    /*
     * Return back to the caller.
     */
    return(retVal);
}

/*
 * AXP_PC16550D_Read
 *  This function is called ...
 */
u8
AXP_PC16550D_Read(int index, u8 address, int len)
{
    AXP_PC16550D *console = &consoles[index];
    u8 retVal = 0;

    switch(address)
    {
        case AXP_PC16550D_RBR_READ:
        case AXP_PC16550D_DLL_REG:
            if (console->lcr.dlab)
            {
                retVal = console->dll.reg;
            }
            else
            {
                if (console->fcr.enable)
                {
                    if (!AXP_PC16550D_EMPTY(console->rcv))
                    {
                        AXP_PC16550D_POP(console->rcv, retVal);
                    }
                    if (AXP_PC16550D_EMPTY(console->rcv))
                    {
                        console->lsr.dr = 0;
                        console->lsr.bi = 0;
                    }
                    else
                    {
                        /* TODO: Timeout Timer ? */
                    }
                    AXP_PC16550D_Update_IRQ(console);
                    if (console->mcr.loop == 0)
                    {
                        /* TODO: Don't receive any data in loopback mode */
                    }
                }
                else
                {
                    retVal = console->rbr.reg;
                    console->lsr.dr = 0;
                    console->lsr.bi = 0;
                }
            }
            break;

        case AXP_PC16550D_IER_REG:
        case AXP_PC16550D_DLM_REG:
            if (console->lcr.dlab)
            {
                retVal = console->dlm.reg;
            }
            else
            {
                retVal = console->ier.reg;
            }
            break;

        case AXP_PC16550D_IIR_READ:
            retVal = console->iir;
            if (console->iir.intr_id == 2)   /* TODO: Not sure if this is correct */
            {
                AXP_PC16550D_Update_IRQ(console);
            }
            break;

        case AXP_PC16550D_LCR_REG:
            retVal = console->lcr.reg;
            break;

        case AXP_PC16550D_MCR_REG:
            retVal = console->mcr.reg;
            break;

        case AXP_PC16550D_LSR_REG:
            retVal = console->lsr.reg;
            if (console->lsr.bi || console->lsr.oe)
            {
                console->lsr.bi = 0;
                console->lsr.oe = 0;
                AXP_PC16550D_Update_IRQ(console);
            }
            break;

        case AXP_PC16550D_MSR_REG:
            if (console->mcr.loop)
            {

                /*
                 * In loopback mode, the model output pins are connected to the input pins.
                 */
                retVal = console->mcr.out << 6 | console->mcr.dtr << 5 | console->mcr.rts << 4;
            }
            else
            {
                retVal = console->msr.reg;

                /*
                 * Clear the delta bits and msr interrupt bits after read, but only if they
                 * are set.
                 */
                if (console->msr.dcts || console->msr.ddsr || console->msr.teri || console->msr.ddcd)
                {
                    console->msr.cts = 0;
                    console->msr.dsr = 0;
                    console->msr.ri = 0;
                    console->msr.dcd = 0;
                    AXP_PC16550D_Update_IRQ(console);
                }
            }
            break;

        case AXP_PC16550D_SCR_REG:
            retVal = console->scr.reg;
            break;
    }

    /*
     * Return the results back to the caller.
     */
    return(retVal);
}

/*
 * AXP_PC16550D_Write
 *  This function is called ...
 */
void
AXP_PC16550D_Write(int index, u8 address, u8 data)
{
    AXP_PC16550D *console = &consoles[index];

    switch(address)
    {
        case AXP_PC16550D_THR_WRITE:
        case AXP_PC16550D_DLL_REG:
            if (console->lcr.dlab)
            {
                console->dll.reg = data;
            }
            else
            {
                console->thr.reg = data;
                if (console->fcr.enable)
                {
                    if (AXP_PC16550D_FULL(console->xmit))
                    {
                        u8 dummy;

                        AXP_PC16550D_POP(console->xmit, dummy);
                    }
                    AXP_PC16550D_PUSH(console->xmit, data);
                }
                console->lsr.thre = 0;
                console->lsr.temt = 0;
                AXP_PC16550D_Update_IRQ(console);
                /* TODO: Need to send data from xmit queue */
                if (AXP_Telnet_Send(console->telnet_session, data, 1) == false)
                {
                    /* TODO: Not sure what to do with an error at this point */
                }
            }
            break;

        case AXP_PC16550D_IER_REG:
        case AXP_PC16550D_DLM_REG:
            if (console->lcr.dlab)
            {
                console->dlm.reg = data;
            }
            else
            {
                AXP_PC16550D_IER new_ier = {.reg = data & 0x0f};

                if (console->ier.reg != new_ier)
                {
                    console->ier.reg = data;
                    console->ier.mbz = 0;
                    AXP_PC16550D_Update_IRQ(console);
                }
            }
            break;

        case AXP_PC16550D_FCR_WRITE:
            {
                AXP_PC16550D_FCR fcr = {.reg = data};

                fcr.res = 0;
                if (fcr.enable != console->fcr.enable)
                {
                    fcr.rcv_reset = fcr.xmit_reset = 1;
                }
                if (fcr.rcv_reset)
                {
                    console->lsr.dr = console->lsr.bi = 0;
                    AXP_PC16550D_RESET(console->rcv);
                }
                if (fcr.xmit_reset)
                {
                    console->lsr.thre = 1;
                    AXP_PC16550D_RESET(console->xmit);
                }
                fcr.rcv_reset = fcr.xmit_reset = 0;
                AXP_PC16550D_Update_FCR(console, &fcr);
                AXP_PC16550D_Update_IRQ(console);
            }
            break;

        case AXP_PC16550D_LCR_REG:
            console->lcr.reg = data;
            /* TODO: Deal with break enable changing */
            break;

        case AXP_PC16550D_MCR_REG:
            console->mcr.reg = data;
            console->mcr.mbz = 0;
            break;

        case AXP_PC16550D_LSR_REG:
        case AXP_PC16550D_MSR_REG:
            /* These registers are read-only */
            break;

        case AXP_PC16550D_SCR_REG:
            console->scr.reg = data;
            break;
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_PC16550D_Receive_cb
 *  This function is called...
 */
void
AXP_PC16550D_Receive_cb(int index)
{
    AXP_PC16550D *console = &consoles[index];
    u8 data;

    if (AXP_Telnet_Receive(console->telnet_session, &data, 1))
    {
        if (console->fcr.enable)
        {
            if (AXP_PC16550D_FULL(console->rcv))
            {
                console->lsr.oe = 1;
            }
            else
            {
                AXP_PC16550D_PUSH(console->rcv, data);
            }
            console->lsr.dr = 1;
        }
        else
        {
            if (console->lsr.dr)
            {
                console->lsr.oe = 1;
            }
            console->rbr.reg = data;
            console->lsr.dr = 1;
        }
        AXP_PC16550D_Update_IRQ(console);
    }
    else
    {
        /* TODO: Not sure what to do with an error at this point */
    }

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_PC16550D_Main
 *  This function is called as a thread used to note when data has been received
 *  over any of the TELNET sessions currently active.  This function will set the
 *  timeout value to 1 millisecond (1,000,000 namo seconds).
 *
 * Input Parameters:
 *  args:
 *      A pointer to void containing arguments supplied when creating this thread.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  NULL
 */
static void *
AXP_PC16550D_Main(void *args)
{
    AXP_PC16550D *select_telnet[AXP_PC16550D_MAX_CONSOLES];
    int index[AXP_PC16550D_MAX_CONSOLES];
    struct timespec timeout;
    u64 mask;
    int count;
    int ii;
    bool result = true;

    timeout.tv_sec = 0;
    timeout.tv_nsec = AXP_PC16550D_MILLISECOND;
    while(result)
    {
        count = 0;
        pthread_mutex_lock(&console_mutex);
        for(ii = 0; ii < AXP_PC16550D_MAX_CONSOLES; ii++)
        {
            index[ii] = 0;
            if (console_init_mask & (1 << ii))
            {
                select_telnet[count] = &consoles[ii].telnet_session;
                index[count] = ii;
                count++;
            }
        }
        pthread_mutex_unlock(&console_mutex);
        result = AXP_Telnet_Select(count, select_telnet, &mask, &timeout);
        if (result == true)
        {
            for (ii = 0; ii < AXP_PC16550D_MAX_CONSOLES; ii++)
            {
                if (mask & (1 << ii))
                {
                    AXP_PC16550D_Receive_cb(index[ii]);
                }
            }
        }
        else
        {
            /* TODO: Not sure what to do about an error return */
        }
    }
    return(NULL);
}
