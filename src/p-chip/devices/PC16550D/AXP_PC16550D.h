/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 14-Nov-2020 Jonathan D. Belanger
 *  Initially written.
 */

#ifndef _PC16550D_AXP_PC16550D_H_
#define _PC16550D_AXP_PC16550D_H_
#include "com-util/AXP_Common_Include.h"
#include "p-chip/devices/PC16550D/AXP_TelnetDefs.h"

typedef struct
{
    u8 reg;
} AXP_PC16550D_RBR;

typedef struct
{
    u8 reg;
} AXP_PC16550D_THR;

typedef union
{
    u8 reg;
    struct
    {
        u8 erbfi : 1;
        u8 etbei : 1;
        u8 elsi : 1;
        u8 edssi : 1;
        u8 mbz : 4;
    };
} AXP_PC16550D_IER;

typedef union
{
    u8 reg;
    struct
    {
        u8 intr_pend : 1;
        u8 intr_id : 3;
        u8 mbz : 2;
        u8 fifo_enable : 2;
    };
} AXP_PC16550D_IIR;

#define AXP_PC16550D_IIR_THRI   1   /* xmit hold register empty */
#define AXP_PC16550D_IIR_RDI    2   /* rcv data interrupt */
#define AXP_PC16550D_IIR_RLSI   3   /* rcv line status interrupt */
#define AXP_PC16550D_IIR_CTI    6   /* character timeout indication */

typedef union
{
    u8 reg;
    struct
    {
        u8 enable : 1;
        u8 rcv_reset : 1;
        u8 xmit_reset : 1;
        u8 dma_mode : 1;
        u8 res : 2;
        u8 rcv_trigger : 2;
    };
} AXP_PC16550D_FCR;

#define AXP_PC16550D_RCV_01 0
#define AXP_PC16550D_RCV_04 1
#define AXP_PC16550D_RCV_08 2
#define AXP_PC16550D_RCV_14 3

typedef union
{
    u8 reg;
    struct
    {
        u8 wls0 : 2;
        u8 stb : 1;
        u8 pen : 1;
        u8 eps : 1;
        u8 stick_par : 1;
        u8 set_brk : 1;
        u8 dlab : 1;
    };
} AXP_PC16550D_LCR;

#define AXP_PC16550D_LCR_5BITS  0
#define AXP_PC16550D_LCR_6BITS  1
#define AXP_PC16550D_LCR_7BITS  2
#define AXP_PC16550D_LCR_8BITS  3

typedef union
{
    u8 reg;
    struct
    {
        u8 dtr : 1;
        u8 rts : 1;
        u8 out : 2;
        u8 loop : 1;
        u8 mbz : 3;
    };
} AXP_PC16550D_MCR;

#define AXP_PC16550D_OUT2 2

typedef union
{
    u8 reg;
    struct
    {
        u8 dr : 1;
        u8 oe : 1;
        u8 pe : 1;
        u8 fe : 1;
        u8 bi : 1;
        u8 thre : 1;
        u8 temt : 1;
        u8 rcv_fifo_err : 1;
    };
} AXP_PC16550D_LSR;

typedef union
{
    u8 reg;
    struct
    {
        u8 dcts : 1;
        u8 ddsr : 1;
        u8 teri : 1;
        u8 ddcd : 1;
        u8 cts : 1;
        u8 dsr : 1;
        u8 ri : 1;
        u8 dcd : 1;
    };
} AXP_PC16550D_MSR;

typedef struct
{
    u8 reg;
} AXP_PC16550D_SCR;

typedef struct
{
    u8 reg;
} AXP_PC16550D_DLL;

#define AXP_PC16550D_9600BAUD   12  /* 0xc is the divisor for 1.8432 MHz clock */

typedef struct
{
    u8 reg;
} AXP_PC16550D_DLM;

/*
 * The following are the register addresses.
 */
#define AXP_PC16550D_RBR_READ 0     /* LCR.DLAB = 0 */
#define AXP_PC16550D_THR_WRITE 0    /* LCR.DLAB = 0 */
#define AXP_PC16550D_IER_REG 1      /* LCR.DLAB = 0 */
#define AXP_PC16550D_IIR_READ 2     /* LCR.DLAB = x */
#define AXP_PC16550D_FCR_WRITE 2    /* LCR.DLAB = x */
#define AXP_PC16550D_LCR_REG 3      /* LCR.DLAB = x */
#define AXP_PC16550D_MCR_REG 4      /* LCR.DLAB = x */
#define AXP_PC16550D_LSR_REG 5      /* LCR.DLAB = x */
#define AXP_PC16550D_MSR_REG 6      /* LCR.DLAB = x */
#define AXP_PC16550D_SCR_REG 7      /* LCR.DLAB = x */
#define AXP_PC16550D_DLL_REG 0      /* LCR.DLAB = 1 */
#define AXP_PC16550D_DLM_REG 1      /* LCR.DLAB = 1 */

#define AXP_PC16550D_FIFO_LEN 16    /* This number must be a power of 2 */
typedef struct
{
    u8 in;
    u8 out;
    u8 count;
    u8 fifo[AXP_PC16550D_FIFO_LEN];
} AXP_PC16550D_FIFO;
#define AXP_PC16550D_RESET(q)       q.in = q.out = q.count = 0;
#define AXP_PC16550D_FULL(q)        (q.count == AXP_PC16550D_FIFO_LEN)
#define AXP_PC16550D_EMPTY(q)       (q.count == 0)
#define AXP_PC16550D_CNT(q)         q.count
#define AXP_PC16550D_PUSH(q, d)                         \
    q.fifo[q.in] = d;                                   \
    q.in = (q.in + 1) & (AXP_PC16550D_FIFO_LEN - 1);    \
    q.count++
#define AXP_PC16550D_POP(q, d)                          \
    d = q.fifo[q.out];                                  \
    q.out = (q.out + 1) & (AXP_PC16550D_FIFO_LEN - 1);  \
    q.count--

typedef struct
{
    AXP_PC16550D_RBR rbr;
    AXP_PC16550D_THR thr;
    AXP_PC16550D_IER ier;
    AXP_PC16550D_IIR iir;
    AXP_PC16550D_FCR fcr;
    AXP_PC16550D_LCR lcr;
    AXP_PC16550D_MCR mcr;
    AXP_PC16550D_LSR lsr;
    AXP_PC16550D_MSR msr;
    AXP_PC16550D_SCR scr;
    union
    {
        u16 divisor;
        struct
        {
            AXP_PC16550D_DLL dll;
            AXP_PC16550D_DLM dlm;
        };
    };
    AXP_PC16550D_FIFO rcv;
    AXP_PC16550D_FIFO xmit;
    u8 rcv_intr_lvl;
    AXP_TELNET_SESSION *telnet_session;
} AXP_PC16550D;

#define AXP_PC16550D_MAX_CONSOLES 2
#define AXP_PC16550D_MILLISECOND 1000000


#endif /* SRC_P_CHIP_DEVICES_PC16550D_AXP_PC16550D_H_ */
