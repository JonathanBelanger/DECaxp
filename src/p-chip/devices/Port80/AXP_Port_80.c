/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This source file contains the code to emulate Port 80 in the Digital Alpha
 *  AXP emulation software.  Port 80 is a port without any real functionality.
 *  It is used to slow things down.  Since an emulator, and this one is no
 *  different, is slow enough, this port is not really going to do anything,
 *  but it needs to be configured in order to avoid error messages about
 *  non-existing hardware.
 *
 * Revision History:
 *
 *  V01.000 10-May-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include <p-chip/devices/Port80/AXP_Port_80.h>

/*
 * Port 80 value.
 */
static u32 port80;

/*
 * AXP_Port80_init
 *  This function is called by the Pchip when the Port 80 legacy device is
 *  configured to the emulated system.  This function initialize the current
 *  configuration from the base and calls back into the Pchip to register
 *  itself.
 *
 * Input Parameters:
 *  p:
 *      A pointer to the Pchip structure.
 *  dev_id:
 *      A value indicating the device ID to be assigned to this device.  If it
 *      is AXP_PCI_MAX_DEVICES, then the next available device location will be
 *      selected.
 *
 * Output Parameters:
 *  None,
 *
 * Return Values:
 *  None
 */
void
AXP_Port80_Init(AXP_21274_PCHIP *p, u32 dev_id)
{
    port80 = 0;
    /* TODO: Register ourselves. */

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_Port80_Read
 *  This function is called to read from port 80.  It returns the last value
 *  written to port 80.
 *
 * Input Parameters:
 *  offset:
 *      A value indicating the offset into the register to be read.  This
 *      parameter is ignored.
 *  size:
 *      A value indicating the number of bytes to be returned.
 *
 * Output Parameters:
 *  None.
 *
 * Return Value:
 *  The last value written to port 80.
 */
u64
AXP_Port80_Read(u32 offset, int size)
{
    switch(size)
    {
        case 1:
            return (u64) (*(u8 *) &port80);
        case 2:
            return (u64) (*(u16 *) &port80);
        default:
            return (u64) port80;
    }
}

/*
 * AXP_Port80_Write
 *  This function is called to write a value into port 80.
 *
 * Input Parameters:
 *  size:
 *      The number of bytes associated with the supplied data parameter.
 *  data:
 *      The value to be written to port 80.  It'll be truncated, depending upon
 *      the size parameter.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void
AXP_Port80_Write(int size, u64 data)
{

    /*
     * Set the port 80 value with the data supplied.  The length of the data is
     * not relevant at this point.
     */
    port80 = (u32) data;

    /*
     * Return back to the caller.
     */
    return;
}
