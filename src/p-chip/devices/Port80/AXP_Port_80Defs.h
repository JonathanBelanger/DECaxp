/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 10-May-2020 Jonathan D. Belanger
 *  Initially written.
 */

#ifndef _AXP_PORT_80DEFS_H_
#define _AXP_PORT_80DEFS_H_

#include <p-chip/devices/port80/AXP_Port_80.h>

/*
 * Function Prototypes
 */
void
AXP_Port80_Init(AXP_21274_PCHIP *p, u32 dev_id);

u64
AXP_Port80_Read(u32 offset, int size);

void
AXP_Port80_Write(int size, u64 data);

#endif /* _AXP_PORT_80DEFS_H_ */
