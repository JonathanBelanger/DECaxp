/*
 * Copyright (C) Jonathan D. Belanger 2018-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions required for the Tsunami/Typhoon
 *  Pchip emulation.
 *
 * Revision History:
 *
 *  V01.000 22-Mar-2018 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.001 12-May-2018 Jonathan D. Belanger
 *  Moved the Pchip CSRs and queues over here to be similar to the real thing.
 *
 *  V01.002 22-Dec-2019 Jonathan D. Belanger
 *  Reorganizing the code so that header files and source files are in the same
 *  directory.
 */
#ifndef _AXP_21274_PCHIP_H_
#define _AXP_21274_PCHIP_H_

#include "com-util/AXP_Utility.h"
#include "com-util/AXP_CAPbus.h"
#include "com-util/AXP_PCI.h"
#include "system/AXP_21274_Registers.h"
#include "c-chip/AXP_21274_Cchip.h"

/*
 * PCI Linear Memory Space Translation
 *
 * HRM Figure 10-1: Linear Memory Address Translation
 *
 *   4   3 3
 *   3   2 1       3
 *  +-----+---------+   +------+
 *  | 800 |         |   | Mask |
 *  +-----+---------+   +------+
 *             |            |
 *             |            V
 *             |      +------------+
 *             |      | Table 10-2 |
 *             |      +------------+
 *             |         |      |
 *             V      +--+      |
 *          3         V         |
 *          1     3 2  0        V
 *         +-------+----+   +--------+
 *         |       |    |   | PCI BE |
 *         +-------+----+   +--------+
 *
 * HRM Table 10-2 addr<2:0> and PCI BE conversion for Linear Addresses
 *  ------------------------------------------------------------
 *                                      PCI BE<3:0> PCI BE <7:0>
 *  Type        Mask        addr<2:0>   (32-bit)    (64-bit)
 *  ------      ---------   ---------   ----------- ------------
 *  Byte        0000.0001   000         1110        1111.1110
 *  Byte        0000.0010   001         1101        1111.1101
 *  Byte        0000.0100   010         1011        1111.1011
 *  Byte        0000.1000   011         0111        1111.0111
 *  Byte        0001.0000   100         1110        1110.1111
 *  Byte        0010.0000   101         1101        1101.1111
 *  Byte        0100.0000   110         1011        1011.1111
 *  Byte        1000.0000   111         0111        0111.1111
 *  Word        0000.0011   000         1100        1111.1100
 *  Word        0000.1100   010         0011        1111.0011
 *  Word        0011.0000   100         1100        1100.1111
 *  Word        1100.0000   110         0011        0011.1111
 *  Long        xxxx.xxx1   000         0000        xxxx.0000
 *  Long        xxxx.xx10   100         0000        0000.1111
 *  Long        xxxx.x100   000         0000        xxxx.0000
 *  Long        xxxx.1000   100         0000        0000.1111
 *  Long        xxx1.0000   000         0000        xxxx.0000
 *  Long        xx10.0000   100         0000        0000.1111
 *  Long        x100.0000   000         0000        xxxx.0000
 *  Long        1000.0000   100         0000        0000.1111
 *  Quad        xxxx.xxxx   000         0000        0000.0000
 *  ------------------------------------------------------------
 *
 * PCI Linear I/O Address Translation
 *
 * HRM Figure 10-2: Linear I/O Address Translation
 *
 *   4        2 2
 *   3        5 4     3
 *  +----------+-------+    +------+
 *  | 801f 110 |       |    | Mask |
 *  +----------+-------+    +------+
 *                 |            |
 *                 |            V
 *                 |     +------------+
 *                 |     | Table 10-3 |
 *                 |     +------------+
 *                 |        |       |
 *                 +-+     ++       |
 *                   |     |        |
 *                   V     V        |
 *     3       2 2                  |
 *     1       5 4     3 2  0       V
 *    +---------+-------+----+  +--------+
 *    | 0000000 |       |    |  | PCI BE |
 *    +---------+-------+----+  +--------+
 *
 * PCI Configuration Space Translation
 *
 * HRM Figure 10-3: Physical to Type 0 Configuration Address Conversion
 *
 *   4     2 2     1 1        1 1
 *   3     4 3     6 5        1 0        8 7          3 2   0    7    0
 *  +-------+-------+----------+----------+------------+-----+  +------+
 *  | 801fe | Bus 0 | Device # | Function | Register # | N/A |  | Mask |
 *  +-------+-------+----------+----------+------------+-----+  +------+
 *                        |          |           |                  |
 *                        V          |           |                  V
 *                 +------------+    |           |            +------------+
 *                 | Table 10-3 |    |           |            | Table 10-4 |
 *                 +------------+    |           |            +------------+
 *                        |          |           |              |       |
 *                        V          V           V      +-------+       |
 *           3                1 1                       V            +--+
 *           1                1 0        8 7          3 2 10         V
 *          +------------------+----------+------------+-+--+   +--------+
 *          | IDSEL from Table | Function | Register # |x|00|   | PCI BE |
 *          +------------------+----------+------------+-+--+   +--------+
 *
 * HRM Table 10-3: Device # Decode Table
 *  -------------------------------------------------------------
 *  Device #        IDSEL (Binary)              (Hex)   Decimal
 *  --------        --------------------------  ------- ---------
 *  0.0000  (0)     0.0000.0000.0000.0000.0001  00.0001       (1)
 *  0.0001  (1)     0.0000.0000.0000.0000.0010  00.0002       (2)
 *  0.0010  (2)     0.0000.0000.0000.0000.0100  00.0004       (4)
 *  0.0011  (3)     0.0000.0000.0000.0000.1000  00.0008       (8)
 *  0.0100  (4)     0.0000.0000.0000.0001.0000  00.0010      (16)
 *  0.0101  (5)     0.0000.0000.0000.0010.0000  00.0020      (32)
 *  0.0110  (6)     0.0000.0000.0000.0100.0000  00.0040      (64)
 *  0.0111  (7)     0.0000.0000.0000.1000.0000  00.0080     (128)
 *  0.1000  (8)     0.0000.0000.0001.0000.0000  00.0100     (256)
 *  0.1001  (9)     0.0000.0000.0010.0000.0000  00.0200     (512)
 *  0.1010 (10)     0.0000.0000.0100.0000.0000  00.0400    (1024)
 *  0.1011 (11)     0.0000.0000.1000.0000.0000  00.0800    (2048)
 *  0.1100 (12)     0.0000.0001.0000.0000.0000  00.1000    (4096)
 *  0.1101 (13)     0.0000.0010.0000.0000.0000  00.2000    (8192)
 *  0.1110 (14)     0.0000.0100.0000.0000.0000  00.4000   (16384)
 *  0.1111 (15)     0.0000.1000.0000.0000.0000  00.8000   (32768)
 *  1.0000 (16)     0.0001.0000.0000.0000.0000  01.0000   (65536)
 *  1,0001 (17)     0.0010.0000.0000.0000.0000  02.0000  (131072)
 *  1.0010 (18)     0.0100.0000.0000.0000.0000  04.0000  (262144)
 *  1.0011 (19)     0.1000.0000.0000.0000.0000  08.0000  (524288)
 *  1.0100 (20)     1.0000.0000.0000.0000.0000  10.0000 (1048576)
 *  1.0101-1.1111   0.0000.0000.0000.0000.0000  00.0000       (0)
 *  -------------------------------------------------------------
 *
 * HRM Figure 10-3: Physical to Type 1 Configuration Address Conversion
 *
 *   4     2 2     1 1        1 1
 *   3     4 3     6 5        1 0        8 7          3 2   0    7    0
 *  +-------+-------+----------+----------+------------+-----+  +------+
 *  | 801fe | Bus # | Device # | Function | Register # | N/A |  | Mask |
 *  +-------+-------+----------+----------+------------+-----+  +------+
 *              |         |          |           |                  |
 *              |         |          |           |                  V
 *              |         |          |           |            +------------+
 *              +---+     +---+      +---+       +---+        | Table 10-4 |
 *                  |         |          |           |        +------------+
 *                  |         |          |           |           |       |
 *                  V         V          V           V      +----+       |
 *   3         2 2     1 1        1 1                       V            |
 *   1         4 3     6 5        1 0        8 7          3 2 10         V
 *  +-----------+-------+----------+----------+------------+-+--+   +--------+
 *  | 0000 0000 | Bus # | Device # | Function | Register # |x|01|   | PCI BE |
 *  +-----------+-------+----------+----- ----+------------+-+--+   +--------+
 *
 * Table 1-4: Mask conversion
 *  --------------------------------------------
 *  Access Type     Mask        addr<2> PCI BE
 *  -----------     ---------   ------- --------
 *  Byte            0000.0001         0 1110 (e)
 *  Byte            0000.0010         0 1101 (d)
 *  Byte            0000.0100         0 1011 (b)
 *  Byte            0000.1000         0 0111 (7)
 *  Byte            0001.0000         1 1110 (e)
 *  Byte            0010.0000         1 1101 (d)
 *  Byte            0100.0000         1 1011 (b)
 *  Byte            1000.0000         1 0111 (7)
 *  Word            0000.0011         0 1100 (c)
 *  Word            0000.1100         0 0011 (3)
 *  Word            0011.0000         1 1100 (c)
 *  Word            1100.0000         1 0011 (3)
 *  Long            xxxx.xxx1         0 0000 (0)
 *  Long            xxxx.xx10         1 0000 (0)
 *  Long            xxxx.x100         0 0000 (0)
 *  Long            xxxx.1000         1 0000 (0)
 *  Long            xxx1.0000         0 0000 (0)
 *  Long            xx10.0000         1 0000 (0)
 *  Long            x100.0000         0 0000 (0)
 *  Long            1000.0000         1 0000 (0)
 *  Quad            xxxx.xxxx         0 0000 (0)
 *  --------------------------------------------
 *
 * Configuration Space Access Methods:
 *
 *  There are 2 ways to access PCI Configuration Space.  The first mechanism is
 *  to write the configuration address into offset 0x0cf8 and access offset
 *  0x0cfc to read from/write to the device information specified at offset
 *  0x0cf8.  The second mechanism has been deprecated since PCI Version 2.0,
 *  and will not be implemented, since we are implementing to PCI Version 2.1.
 *
 * Configuration Space Access Mechanism #1:
 *
 *  There are 2 32-bit I/O locations used in this mechanism.  The first
 *  location is at offset 0x0cf8 and is named CONFIG_ADDRESS.  The second
 *  location is at offset 0x0cfc and is named CONFIG_DATA.  CONFIG_ADDRESS
 *  specifies the configuration address that is required to be accessed, while
 *  accesses, read/write, to CONFIG_DATA will generate the configuration access
 *  and will transfer the data to/from the CONFIG_DATA register.
 *
 *  The CONFIG_ADDRESS has the following format:
 *
 *          3 3        2 2     1 1        1 1
 *          1 0        4 3     6 5        1 0          8 7            0
 *  +--------+----------+-------+----------+------------+--------------+
 *  | Enable | Reserved | Bus # | Device # | Function # | Reg. Offset* |
 *  +--------+----------+-------+----------+------------+--------------+
 *  * Reg. Offset has to point to consecutive DWORDS, therefore bits 1:0 must
 *    always equal 0.
 */

/*
 * Translation Lookaside Buffer
 */
typedef struct
{
        bool dacCycle;
        u32 pciAddr;
} AXP_21274_TAG;
typedef struct
{
    bool valid;
    u64 cpuPageAddr;
} AXP_21274_DATA;
typedef struct
{
    AXP_21274_TAG tag;
    AXP_21274_DATA data[4];
} AXP_21274_TLB;

/*
 * Control commands for the Pchip come from the Cchip, but data is transferred
 * between the Pchip and Dchip.  This data to the Dchip is on its way to one of
 * a CPU, memory, or the other Pchip.
 *
 * An Alpha AXP system can have 1 or 2 Pchips.  The Pchips communicate with
 * the Cchip and Dchip.  A high-level diagram would look like the following
 * (from a Pchip perspective):
 *
 *         CAP Cmd     +---------+
 *       +------------>+ Pchip 0 +<------------+
 *       |             +----+----+             | Data
 *       |                  ^                  |
 *       |                  | Cmd/Data         v            +------+
 *       v                  v             +----+----+   +-->+ CPUs |
 *  +----+----+      +------+------+      |         +<--+   +------+
 *  |  Cchip  |      | PCI Devices |      |  Dchip  |
 *  +----+----+      +------+------+      +         +<--+   +--------+
 *       ^                  ^             +----+----+   +-->+ Memory |
 *       |                  | Cmd/Data         ^            +--------+
 *       |                  v                  |
 *       |             +----+----+             | Data
 *       +------------>+ Pchip 1 +<------------+
 *         CAP Cmd     +---------+
 *
 * As you can see the data from the PCI Devices go through the Pchip on its way
 * to some place beyond the Dchip.  Data and commands to and from the Pchip are
 * the responsibility of the Pchip, as well.
 *
 * For data coming into the Pchip, through the Dchip, the data is retrieved
 * from the Dchip when the Cchip instructs the Pchip to do so, and the Pchip
 * will process it or send it to the PCI devices.
 *
 * For data going out to the System, also through the Dchip, the Cchip needs to
 * know when the data get is ready for the Cchip to be able to instruct the
 * Dchip to retrieve and process it.
 *
 * HRM: 8.1 Pchip Architecture
 *
 *  The Pchip is the interface between devices on the PCI bus and the rest of
 *  the system.  There can be one or two Pchips, and corresponding single or
 *  dual PCI buses, connected to the Cchip and Dchips.  The Pchip performs the
 *  following functions:
 *
 *      - Accepts requests from the Cchip by means of the CAPbus and enqueues
 *        them
 *      - Issues commands to the PCI bus based on these requests
 *      - Accepts requests from the PCI bus and enqueues them
 *      - Issues commands to the Cchip by means of the CAPbus based on these
 *        requests
 *      - Transfers data to and from the Dchips based on the above commands and
 *        requests
 *      - Buffers the data when necessary
 *      - Reports errors to the Cchip, after recoding the nature of the error
 */

/*
 * The following definition contains the fields and data structures required to
 * implement a single Pchip.  There is always at least one of these and as many
 * as two of them.
 */
#define AXP_21274_PCHIP_RQ_SIZE    4 /* up to 4 messages from Cchip */
typedef struct
{

    /*
     * There is one thread/mutex/condition variable per Pchip.
     */
    pthread_t threadID;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    bool running;

    /*
     * Pchip ID (either 0 or 1) and PCI bus
     */
    u32 pChipID;
    AXP_PCI_Bus bus;
    u32 devices;

    /*
     * Interface queues.
     */
    AXP_CAP_Msg prq[AXP_21274_PCHIP_RQ_SIZE];   /* CSC<PRQMAX> */
    u32 readPrq;
    u32 writePrq;
    AXP_QUEUE_HDR padBus;
//    AXP_21274_SYSTEM *cChip;

    /*
     * PCI Bus queues
     */
    AXP_QUEUE_HDR qddw; /* Downstream Write Data Queue - 16 QW */
    AXP_QUEUE_HDR qddr; /* Downstream Read Data Queue - 16 QW */
    AXP_QUEUE_HDR qdaw; /* Downstream Write Address Queue - 4 */
    AXP_QUEUE_HDR qdar; /* Downstream Read Address Queue - 4 */
    AXP_QUEUE_HDR qudw; /* Upstream Write Data Queue - 16 QW */
    AXP_QUEUE_HDR qudr; /* Upstream Read Data Queue - 16 QW */

    /*
     * PTP and DMA
     */
    AXP_QUEUE_HDR ptpRW;
    AXP_QUEUE_HDR dmaWrite[2];
    AXP_QUEUE_HDR dmaRead;

    /*
     * Scatter-Gather Associative TLB
     */
    AXP_21274_TLB tlb[168];

    /*
     * The following are the Pchip CSRs.  The addresses for these Pchip CSRs
     * have n=1 for p0 and n=3 for p1.
     */
    AXP_21274_WSBAn wsba0;          /* Address: 80n.8000.0000 */
    AXP_21274_WSBAn wsba1;          /* Address: 80n.8000.0040 */
    AXP_21274_WSBAn wsba2;          /* Address: 80n.8000.0080 */
    AXP_21274_WSBA3 wsba3;          /* Address: 80n.8000.00c0 */
    AXP_21274_WSMn wsm0;            /* Address: 80n.8000.0100 */
    AXP_21274_WSMn wsm1;            /* Address: 80n.8000.0140 */
    AXP_21274_WSMn wsm2;            /* Address: 80n.8000.0180 */
    AXP_21274_WSMn wsm3;            /* Address: 80n.8000.01c0 */
    AXP_21274_TBAn tba0;            /* Address: 80n.8000.0200 */
    AXP_21274_TBAn tba1;            /* Address: 80n.8000.0240 */
    AXP_21274_TBAn tba2;            /* Address: 80n.8000.0280 */
    AXP_21274_TBAn tba3;            /* Address: 80n.8000.02c0 */
    AXP_21274_PCTL pctl;            /* Address: 80n.8000.0300 */
    AXP_21274_PLAT plat;            /* Address: 80n.8000.0340 */
 /* AXP_21274_RES        res;        * Address: 80n.8000.0380 */
    AXP_21274_PERROR perror;        /* Address: 80n.8000.03c0 */
    AXP_21274_PERRMASK perrMask;    /* Address: 80n.8000.0400 */
    AXP_21274_PERRSET perrSet;      /* Address: 80n.8000.0440 */
    AXP_21274_TLBIV tlbiv;          /* Address: 80n.8000.0480 */
    AXP_21274_PMONCTL pMonCtl;      /* Address: 80n.8000.0500 */
    AXP_21274_PMONCNT pMonCnt;      /* Address: 80n.8000.0540 */
    AXP_21274_SPRST sprSt;          /* Address: 80n.8000.0800 */
} AXP_21274_PCHIP;

#define AXP_21274_WHICH_PCHIP(addr) (((addr) & 0x0000000200000000) >> 33)

#endif /* _AXP_21274_PCHIP_H_ */
