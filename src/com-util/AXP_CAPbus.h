/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 29-Feb-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_CAPBUS_H_
#define _AXP_CAPBUS_H_

#include "com-util/AXP_Common_Include.h"
#include "com-util/AXP_SysDc.h"

/*
 * HRM Table 6-7 Cchip-to-Pchip Commands
 * ----------------------------------------------------------------------------
 * Code Command                     Cycles      Valid Fields
 * ----------------------------------------------------------------------------
 * 0000 PCI IACK cycle                  2           T, Mask
 * 0001 PCI special cycle               2           T, Mask
 * 0010 PCI IO read                     2           T, Mask
 * 0011 PCI IO write                    2           T, Mask
 * 0100 Reserved                        -           -
 * 0101 PCI memory write, PTP           2           T, Mask
 * 0110 PCI memory read                 2           T, Mask
 * 0111 PCI memory write, from CPU      2           T, Mask
 * 1000 CSR read                        1 or 5(1)   C-bit, CSR#
 * 1001 CSR write                       1 (+1)(1)   C-bit, CSR#
 * 1010 PCI configuration read          2           T, Mask
 * 1011 PCI configuration write         2           T, Mask
 * 1100 Load PADbus data downstream     1           LDP
 * 1101 Reserved (Pchip upstream LoadP) -           -
 * 1110 Reserved                        -           -
 * 1111 No-op                           1           -
 * ----------------------------------------------------------------------------
 * (1) For details, refer to command descriptions.
 *
 * HRM Table 6-8 Pchip-to-Cchip and Pchip-to-Pchip Bypass Commands
 * ----------------------------------------------------------------------------
 * Code Command                             Cycles          Valid Fields
 * ----------------------------------------------------------------------------
 * 0000 DMA read N QW                           2           T=10, Mask
 * 0001 Scatter-gather table entry read N QW    2           T=10, Mask
 * 0010 Reserved                                -           -
 * 0011 Reserved                                -           -
 * 0100 Reserved                                -           -
 * 0101 Reserved                                -           -
 * 0110 PTP memory read                         2           T, Mask
 * 0111 PTP memory write                        2           T=10, Mask
 * 1000 DMA RMW QW                              2           T=10, only one
 *                                                          mask bit set
 * 1001 DMA write N QW                          2           T=10, Mask
 * 1010 Reserved                                -           -
 * 1011 Reserved                                -           -
 * 1100 Reserved (Cchip downstream LoadP)       -           -
 * 1101 Load PADbus data upstream               2 or 5(1)   LDP
 * 1110 PTP write byte-mask bypass              2           See text in
 *                                                          Section 6.2.3.2
 * 1111 No-op                                   1           -
 * ----------------------------------------------------------------------------
 * (1) For details, refer to command descriptions.
 *
 * The following enumeration defines all the CAPbus commands sent between the
 * Cchip and one or two Pchips.
 */
typedef enum
{
    /* Cchip-to-Pchip Commands */
    CAP_PCI_IACK_cycle,                 /* b'0000' */
    CAP_PCI_special_cycle,              /* b'0001' */
    CAP_PCI_IO_read,                    /* b'0010' */
    CAP_PCI_IO_write,                   /* b'0011' */
    CAP_PCI_memory_write_PTP,           /* b'0101' */
    CAP_PCI_memory_read,                /* b'0110' */
    CAP_PCI_memory_write_CPU,           /* b'0111' */
    CAP_CSR_read,                       /* b'1000' */
    CAP_CSR_write,                      /* b'1001' */
    CAP_PCI_configuration_read,         /* b'1010' */
    CAP_PCI_configuration_write,        /* b'1011' */
    CAP_Load_PADbus_data_downstream,    /* b'1100' */

    /* Pchip-to-Cchip Commands */
    CAP_DMA_read_N_QW,                  /* b'0000' */
    CAP_SG_table_entry_read_N_QW,       /* b'0001' */
    CAP_PTP_memory_read,                /* b'0110' */
    CAP_PTP_memory_write,               /* b'0111' */
    CAP_DMA_RMW_QW,                     /* b'1000' */
    CAP_DMA_write_N_QW,                 /* b'1001' */
    CAP_Load_PADbus_data_upstream,      /* b'1101' */
    CAP_PTP_write_byte_mask_bypass,     /* b'1110' */

    /* Common in both directions */
    CAP_No_op                           /* b'1111' */
} AXP_CAP_Command;

/*
 * Table 6-4 Encoding of T Field T Mask Type PADbus Transfer Characteristics
 * ----------------------------------------------------------------------------
 * T    Mask Type   PADbus Transfer Characteristics
 * ----------------------------------------------------------------------------
 * 00   Byte        One quadword transferred.
 * 01   Longword    Four quadwords are always transferred. The 8-bit mask
 *                  comprises four longword pairs. The first transferred
 *                  quadword is specified by addr<4:3> and corresponds to the
 *                  lowest-order nonzero pair of mask bits. If the number of
 *                  nonzero mask bit pairs is less than four, the trailing
 *                  quadwords on the PADbus are discarded.
 * 10   Quadword    The number of quadwords transferred is equal to the number
 *                  of asserted mask bits. The first transferred quadword is
 *                  specified by addr<5:3> and corresponds to the lowest-order
 *                  asserted mask bit.
 * 11   Illegal     Causes unspecified results.
 * ----------------------------------------------------------------------------
 *
 * The following enumeration is used to determine how to interpret the mask
 * information for PIO ReadBytes, ReadLWs, ReadQWs, WriteBytes, WriteLWs, and
 * WriteQWs.
 */
typedef enum
{
    CAP_Byte,
    CAP_Longword,
    CAP_Qualdword
} AXP_CAP_T;

/*
 * Table 6-5 C-Bit Encoding
 * ----------------------------
 * C-Bit    Meaning
 * ----------------------------
 * 0        Pchip CSR operation
 * 1        Cchip CSR operation
 * ----------------------------
 *
 * NOTE: In this implementation, Cchip CSR data flows directly to and from the
 *       Dchip though a non-architecturally accurate interface.  This data does
 *       not need to flow through the Pchips, at all.
 *
 * The following enumeration is used to specify which CSRs the CAPbus command
 * is referring.  In this emulation, where we have a data path between the
 * Cchip and Dchip, the CAP_Cchip_CSR_operation is not used.
 */
typedef enum
{
    CAP_Cbit_NoOp,                      /* NoOp */
    CAP_Pchip_CSR_operation,            /* b'0' */
    CAP_Cchip_CSR_operation             /* b'1' */
} AXP_CAP_C_Bit;

/*
 * Table 6-6 LDP Encoding
 * -----------------------------------------------------------------------
 * LDP      Meaning for Downstream LoadP    Meaning for Upstream LoadP
 * -----------------------------------------------------------------------
 * 00       LoadP DMA read                  LoadP PCI read
 * 01       LoadP DMA RMW (to Pchip)        LoadP DMA RMW (from Pchip)
 * 10       LoadP PTP                       LoadP CSR read
 * 11       LoadP SGTE read                 LoadP CSR write
 * -----------------------------------------------------------------------
 *
 * The following enumeration is used to specify the specific LoadP operation
 * being requested.
 */
typedef enum
{
    CAP_LoadP_NoOp,                     /* NoOp */

    /* Downstream LoadP */
    CAP_LoadP_DMA_read,                 /* b'00' */
    CAP_LoadP_PTP,                      /* b'10' */
    CAP_LoadP_SCTE_read,                /* b'11' */

    /* Upstream LoadP */
    CAP_LoadP_PCI_read,                 /* b'00' */
    CAP_LoadP_CSR_read,                 /* b'10' */
    CAP_LoadP_CSR_write,                /* b'11' */

    /* Common in both directions */
    CAP_LoadP_DMA_RMW                   /* b'01' */
} AXP_CAP_LDP;

/*
 * HRM 6.2.3 CAPbus Command Encodings
 *
 * Figure 6-3 shows the format of the 2-cycle CAPbus commands. Table 6-4 lists
 * the encoding of the T field, the number of quadwords for which the PADbus is
 * busy with the transfer, and the address of the first quadword transferred.
 * The mask field denotes which data is valid in the transfer, and is aligned
 * to eight times the data type. That is, for byte transfers, the eight bits
 * represent a total of one quadword. For longword transfers, the eight bits
 * represent four quadwords. For quadword transfers, the eight bits represent
 * up to a full cache block (eight quadwords).
 *
 *  Figure 6-3 Format of 2-Cycle Commands
 *
 *            2  2  2  2  1  1  1  1  1  1  1  1  1  1
 *            3  2  1  0  9  8  7  6  5  4  3  2  1  0  9  8  7  6  5  4  3  2  1  0
 *          +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  Phase 1 |  Command  |           Address<31:12>                                  |
 *          +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  Phase 2 |  T  |  X  |         Mask          |         Address<34:32,11:3>       |
 *          +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *
 *  Figure 6-4 Format of 2-Cycle Commands
 *
 *            2  2  2  2  1  1  1  1  1  1  1  1  1  1
 *            3  2  1  0  9  8  7  6  5  4  3  2  1  0  9  8  7  6  5  4  3  2  1  0
 *          +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *  Phase 1 |  Command  | LDP | C|  Reserved |              CSR #                   |
 *          +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *
 * The following structure is used in both the Pchip and Cchip.  Each contains
 * a list of messages that can be used by the other to request processing or
 * respond to a request over the CAPbus.
 */
typedef struct
{
    AXP_CAP_Command cmd;
    AXP_CAP_T t;
    AXP_CAP_C_Bit c;
    AXP_CAP_LDP ldp;
    AXP_SysDc_Mask mask;
    u64 address;
    u16 csr;
} AXP_CAP_Msg;

#endif /* _AXP_CAPBUS_H_ */
