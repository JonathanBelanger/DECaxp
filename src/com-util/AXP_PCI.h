/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 18-Apr-2020 Jonathan D. Belanger
 *  Initially written.
 */

#ifndef _AXP_PCI_H_
#define _AXP_PCI_H_

#include "com-util/AXP_Common_Include.h"

typedef union
{
    u8 headerType;
    struct
    {
        u8 type : 7;
        u8 multiFunction : 1;
    };
} AXP_PCI_HeaderType;

typedef union
{
    u8 BIST;
    struct
    {
        u8 completionCode : 4;
        u8 res : 2;
        u8 startBIST: 1;
        u8 BISTCabable : 1;
    };
} AXP_PCI_BIST;

typedef union
{
    u16 command;
    struct
    {
        u16 ioSpace : 1;
        u16 memorySpace : 1;
        u16 busMaster : 1;
        u16 specialCycles : 1;
        u16 memWriteInvalidEna : 1;
        u16 vgaPaletteSnoop : 1;
        u16 parityErrorRsp : 1;
        u16 res_0 : 1;
        u16 serrEna : 1;
        u16 fastB2BEna : 1;
        u16 interruptDisable : 1;
        u16 res_1 : 5;
    };
} AXP_PCI_Command;

#define AXP_PCI_STATUS_INDEX 0x06
#define AXP_PCI_STATUS_PERR_BIT 0x8000
#define AXP_PCI_STATUS_SERR_BIT 0x4000
#define AXP_PCI_STATUS_RMABT_BIT 0x2000
#define AXP_PCI_STATUS_RTABT_BIT 0x1000
#define AXP_PCI_STATUS_STABT_BIT 0x0800
#define AXP_PCI_STATUS_DEVSEL_FAST 0
#define AXP_PCI_STATUS_DEVSEL_MED 1
#define AXP_PCI_STATUS_DEVSEL_SLOW 2
typedef union
{
    u16 status;
    struct
    {
        u16 res_0 : 3;
        u16 interruptStatus : 1;
        u16 capabilitiesList : 1;
        u16 sixtySixMhz : 1;
        u16 res_1 : 1;
        u16 fastB2BEna : 1;
        u16 masterDataParityErr : 1;
        u16 DEVSELTiming : 2;
        u16 signalTargetAbort : 1;
        u16 rcvdTargetAbort : 1;
        u16 rcvdMasterAbort : 1;
        u16 signalledSystemErr : 1;
        u16 detectedParityErr : 1;
    };
} AXP_PCI_Status;

typedef struct
{
    u16 vendorID;
    u16 deviceID;
    AXP_PCI_Command command;
    AXP_PCI_Status status;
    u8 revisionID;
    u8 programmingInterface;
    u8 subclass;
    u8 class;
    u8 cacheLineSize;
    u8 latencyTimer;
    AXP_PCI_HeaderType headerType;
    AXP_PCI_BIST bist;
} AXP_PCI_Config_Common;

typedef union
{
    u32 BAR;
    struct
    {
        u32 ioSpace : 1;    /* 0 = Memory Space */
        u32 type : 2;
        u32 prefetchable : 1;
        u32 baseAddress : 28;
    } memorySpace;
    struct
    {
        u32 ioSpace : 1;    /* 1 = I/O Space */
        u32 res : 1;
        u32 baseAddress : 30;
    } ioSpace;
} AXP_PCI_BAR;
#define AXP_NUM_BARS    6

typedef struct
{
    AXP_PCI_Config_Common common;
    AXP_PCI_BAR bar[AXP_NUM_BARS];
    u32 cardbusCISPtr;
    u16 subsystemVendorID;
    u16 subsystemID;
    u32 ROMBaseAddr;
    u8 capabilitiesPtr;
    u8 res_0[3];
    u32 res_1;
    u8 interruptLine;
    u8 interruptPIN;
    u8 minGrant;
    u8 maxLatency;
} AXP_PCI_Config_Type_0;

typedef struct
{
    AXP_PCI_Config_Common common;
    AXP_PCI_BAR bar[2];
    u8 primaryBusNum;
    u8 secondaryBusNum;
    u8 subordinateBusNum;
    u8 secondaryLatencyTimer;
    u8 ioBase;
    u8 ioLimit;
    u16 secondaryStatus;
    u16 memoryBase;
    u16 memoryLimit;
    u16 prefetchableMemoryBase;
    u16 prefetchableMemoryLimit;
    u32 prefetchableMemoryBaseUpper;
    u32 prefetchableMemoryLimitUpper;
    u16 ioBaseUpper;
    u16 ioLimitUpper;
    u8 capabilitiesPtr;
    u8 res_0[3];
    u32 ROMBaseAddr;
    u8 interruptLine;
    u8 interruptPIN;
    u16 bridgeControl;
} AXP_PCI_Config_Type_1;

typedef struct
{
    u32 baseAddress;
    u32 limit;
} AXP_PCI_memoryIO;

typedef struct
{
    AXP_PCI_Config_Common common;
    u32 cardBusBaseAddress;
    u8 offsetCapabilities;
    u8 res_0;
    u16 secondaryStatus;
    u8 pciBusNumber;
    u8 cardbusBusNumber;
    u8 subordinateBusNumber;
    u8 cardbusLatencyTimer;
    AXP_PCI_memoryIO memory[2];
    AXP_PCI_memoryIO io[2];
    u8 interruptLine;
    u8 interruptPIN;
    u16 bridgeControl;
    u16 subsystemVendorID;
    u16 subsystemID;
    u32 PCCardLegacyModeBaseAddress;
} AXP_PCI_Config_Type_2;

#define AXP_PCI_CONFIG_BYTE 256
#define AXP_PCI_CONFIG_WORD (AXP_PCI_CONFIG_BYTE / sizeof(u16))
#define AXP_PCI_CONFIG_LONG (AXP_PCI_CONFIG_BYTE / sizeof(u32))
typedef union
{
    u8 configByte[AXP_PCI_CONFIG_BYTE];
    u8 configWord[AXP_PCI_CONFIG_WORD];
    u32 configLong[AXP_PCI_CONFIG_LONG];
    AXP_PCI_Config_Type_0 type0;    /* Standard Header */
    AXP_PCI_Config_Type_1 type1;    /* PCI-to-PCI Bridge Header */
    AXP_PCI_Config_Type_2 type2;    /* CardBus Bridge Header */
} AXP_PCI_ConfigData;

/*
 * The following typedef defines the format of the function each PCI device
 * must provide during register.
 */
typedef u32 (*AXP_PCI_ConfigRead)(u8 function, u8 reg, u8 offset, u8 size);
typedef void (*AXP_PCI_ConfigWrite)(u8 function, u8 reg, u8 offset, u32 data,
                                    u8 size);
typedef u64 (*AXP_PCI_MemIORead)(u32 pciAddr, u16 pciBe, u8 *data);
typedef void (*AXP_PCI_MemIOWrite)(u32 pciAddr, u16 pciBe, u8 *data);

typedef void (*AXP_PCI_Reset)(void);

typedef struct
{
    bool present;
    AXP_PCI_ConfigRead configRead;
    AXP_PCI_ConfigWrite configWrite;
    AXP_PCI_MemIORead ioRead;
    AXP_PCI_MemIOWrite ioWrite;
    AXP_PCI_Reset reset;
} AXP_PCI_Device;

/*
 * PCI Memory Address
 *
 *   3
 *   1     3 2  0
 *  +-------+----+   +--------+
 *  |       |    |   | PCI BE |
 *  +-------+----+   +--------+
 *
 * PCI I/O Address
 *
 *   3       2 2
 *   1       5 4     3 2  0
 *  +---------+-------+----+  +--------+
 *  | 0000000 |       |    |  | PCI BE |
 *  +---------+-------+----+  +--------+
 *
 * PCI Configuration Space
 *
 * Type 0 Configuration Address
 *
 *   3                1 1
 *   1                1 0        8 7          3 2 10
 *  +------------------+----------+------------+-+--+   +--------+
 *  | IDSEL from Table | Function | Register # |x|00|   | PCI BE |
 *  +------------------+----------+------------+-+--+   +--------+
 *
 * Type 1 Configuration Address
 *
 *   3         2 2     1 1        1 1
 *   1         4 3     6 5        1 0        8 7          3 2 10
 *  +-----------+-------+----------+----------+------------+-+--+   +--------+
 *  | 0000 0000 | Bus # | Device # | Function | Register # |x|01|   | PCI BE |
 *  +-----------+-------+----------+----- ----+------------+-+--+   +--------+
 */

/*
 *  The CONFIG_ADDRESS has the following format:
 *
 *          3 3        2 2     1 1        1 1
 *          1 0        4 3     6 5        1 0          8 7            0
 *  +--------+----------+-------+----------+------------+--------------+
 *  | Enable | Reserved | Bus # | Device # | Function # | Reg. Offset* |
 *  +--------+----------+-------+----------+------------+--------------+
 *  * Reg. Offset has to point to consecutive DWORDS, therefore bits 1:0 must
 *    always equal 0.
 */
typedef union
{
    u32 reg;
    struct
    {
        u8 offset;
        u8 function : 3;
        u8 device : 5;
        u8 bus;
        u8 res : 7;
        u8 enable : 1;
    };
} AXP_PCI_ConfigAddress;

#define AXP_PCI_MAX_BUSES   256
#define AXP_PCI_MAX_DEVICES 32
#define AXP_PCI_MAX_FUNCTIONS 8

/*
 * The following 2 addresses are used to pass configuration address information
 * for reads and writes.  First, a PCI address is written into the port
 * (register) at offset 0x0cf8.  A read or write for the port (register) at
 * offset 0x0cfc will use the value at 0x0cf8 to access the configuration
 * information referred to by this address.
 */
#define AXP_PCI_CONFIG_ADDRESS 0x0cf8
#define AXP_PCI_CONFIG_DATA 0x0cfc
typedef struct
{
    AXP_PCI_Device devs[AXP_PCI_MAX_DEVICES];
    AXP_PCI_ConfigAddress configPortAddress;    /* at offset 0x0cf8 */
} AXP_PCI_Bus;

/*
 * Vendor IDs
 */
#define AXP_PCI_ALADDIN_VENDOR_ID   0x10B9
#define AXP_PCI_BROADCOM_VENDOR_ID  0x1000
#define AXP_PCI_DEC_VENDOR_ID       0x1011
#define AXP_PCI_CIRRUS_VENDOR_ID    0x1013
#define AXP_PCI_S3_GRAPH_VENDOR_ID  0x5333

/*
 * Device IDs
 */
#define AXP_PCI_53C810_DEVICE_ID    0x0001
#define AXP_PCI_53C895_DEVICE_ID    0x000c
#define AXP_PCI_21142_43_DEVICE_ID  0x0019
#define AXP_PCI_ALPINE_DEVICE_ID    0x00a8
#define AXP_PCI_M1543C_DEVICE_ID    0x1533
#define AXP_PCI_M5229_DEVICE_ID     0x5229
#define AXP_PCI_USB_1_1_DEVICE_ID   0x5237
#define AXP_PCI_86C764_5_DEVICE_ID  0x8811

/*
 * Device Revisions
 */
#define AXP_PCI_M1543C_REV_ID       0xc3
#define AXP_PCI_M5229_REV_ID        0x20

/*
 * Class codes
 */
#define AXP_PCI_CLASS_NONE          0x00
#define AXP_PCI_CLASS_STORAGE       0x01
#define AXP_PCI_CLASS_NETWORK       0x02
#define AXP_PCI_CLASS_DISPLAY       0x03
#define AXP_PCI_CLASS_MULTIMEDIA    0x04
#define AXP_PCI_CLASS_MEMORY        0x05
#define AXP_PCI_CLASS_BRIDGE        0x06
#define AXP_PCI_CLASS_COMMS         0x07
#define AXP_PCI_CLASS_PERIPERAL     0x08
#define AXP_PCI_CLASS_INPUT         0x09
#define AXP_PCI_CLASS_DOCKING       0x0a
#define AXP_PCI_CLASS_PROCESSOR     0x0b
#define AXP_PCI_CLASS_SERIAL_BUS    0x0c
#define AXP_PCI_CLASS_WIRELESS      0x0d
#define AXP_PCI_CLASS_IO            0x0e
#define AXP_PCI_CLASS_SATELLITE     0x0f
#define AXP_PCI_CLASS_ENCRYPT       0x10
#define AXP_PCI_CLASS_DATA_SIGNAL   0x11

/*
 * Subclass and Interface codes
 */
/*
 * No Class
 */
#define AXP_PCI_00_SUB_NONE         0x00
#define AXP_PCI_00_SUB_VGA          0x01
#define AXP_PCI_00_INTER            0x00

/*
 * Storage
 */
#define AXP_PCI_01_SUB_SCSI         0x00
#define AXP_PCI_01_SUB_IDE          0x01
#define AXP_PCI_01_SUB_FLOPPY       0x02
#define AXP_PCI_01_SUB_IPI          0x03
#define AXP_PCI_01_SUB_RAID         0x04
#define AXP_PCI_01_SUB_OTHER        0x80
#define AXP_PCI_01_INTER            0x00
#define AXP_PCI_01_IDE_MASTER       0x80
#define AXP_PCI_01_IDE_SEC_PROG     0x08
#define AXP_PCI_01_IDE_SEC_MODE     0x04
#define AXP_PCI_01_IDE_PRI_PROG     0x02
#define AXP_PCI_01_IDE_PRI_MODE     0x01

/*
 * Network
 */
#define AXP_PCI_02_SUB_ETHER        0x00
#define AXP_PCI_02_SUB_TOKEN        0x01
#define AXP_PCI_02_SUB_FDDI         0x02
#define AXP_PCI_02_SUB_ATM          0x03
#define AXP_PCI_02_SUB_ISDN         0x04
#define AXP_PCI_02_SUB_OTHER        0x80
#define AXP_PCI_02_INTER            0x00

/*
 * Display
 */
#define AXP_PCI_03_SUB_VGA          0x00
#define AXP_PCI_03_SUB_XGA          0x01
#define AXP_PCI_03_SUB_3D           0x02
#define AXP_PCI_03_SUB_OTHER        0x80
#define AXP_PCI_03_INTER            0x00
#define AXP_PCI_03_VGA_VGA          0x00
#define AXP_PCI_03_VGA_8514         0x01

/*
 * Multimedia
 */
#define AXP_PCI_04_SUB_VIDEO        0x00
#define AXP_PCI_04_SUB_AUDIO        0x01
#define AXP_PCI_04_SUB_TELE         0x02
#define AXP_PCI_04_SUB_OTHER        0x80
#define AXP_PCI_04_INTER            0x00

/*
 * Memory
 */
#define AXP_PCI_05_SUB_RAM          0x00
#define AXP_PCI_05_SUB_FLASH        0x01
#define AXP_PCI_05_SUB_OTHER        0x80
#define AXP_PCI_05_INTER            0x00

/*
 * Bridge
 */
#define AXP_PCI_06_SUB_HOST         0x00
#define AXP_PCI_06_SUB_ISA          0x01
#define AXP_PCI_06_SUB_EISA         0x02
#define AXP_PCI_06_SUB_MCA          0x03
#define AXP_PCI_06_SUB_PCI          0x04
#define AXP_PCI_06_SUB_PCMCIA       0x05
#define AXP_PCI_06_SUB_NUBUS        0x06
#define AXP_PCI_06_SUB_CARDBUS      0x07
#define AXP_PCI_06_SUB_RACEWAY      0x08
#define AXP_PCI_06_SUB_OTHER        0x80
#define AXP_PCI_06_INTER            0x00
#define AXP_PCI_06_PCI_PCI          0x00
#define AXP_PCI_06_PCI_SUB          0x01
#define AXP_PCI_06_RACE_TRANSP      0x00
#define AXP_PCI_06_RACE_ENDP        0x01

/*
 * Communications
 */
#define AXP_PCI_07_SUB_GENERIC      0x00
#define AXP_PCI_07_SUB_PARALLEL     0x01
#define AXP_PCI_07_SUB_MULTIP       0x02
#define AXP_PCI_07_SUB_MODEM        0x03
#define AXP_PCI_07_SUB_OTHER        0x80
#define AXP_PCI_07_INTER            0x00
#define AXP_PCI_07_GEN_XT           0x00
#define AXP_PCI_07_GEN_16450        0x01
#define AXP_PCI_07_GEN_16550        0x02
#define AXP_PCI_07_GEN_16650        0x03
#define AXP_PCI_07_GEN_16750        0x04
#define AXP_PCI_07_GEN_16850        0x05
#define AXP_PCI_07_GEN_16950        0x06
#define AXP_PCI_07_PARAL_GEN        0x00
#define AXP_PCI_07_PARAL_BI         0x01
#define AXP_PCI_07_PARAL_ECP        0x02
#define AXP_PCI_07_PARAL_1284       0x03
#define AXP_PCI_07_PARAL_1285T      0xfe
#define AXP_PCI_07_MODEM_GEN        0x00
#define AXP_PCI_07_MODEM_16450      0x01
#define AXP_PCI_07_MODEM_16550      0x02
#define AXP_PCI_07_MODEM_16650      0x03
#define AXP_PCI_07_MODEM_16750      0x04

/*
 * Peripheral
 */
#define AXP_PCI_08_SUB_PIC          0x00
#define AXP_PCI_08_SUB_DMA          0x01
#define AXP_PCI_08_SUB_TIMER        0x02
#define AXP_PCI_08_SUB_RTC          0x03
#define AXP_PCI_08_SUB_PCI_HOT      0x04
#define AXP_PCI_08_SUB_OTHER        0x80
#define AXP_PCI_08_INTER            0x00
#define AXP_PCI_08_PIC_8259         0x00
#define AXP_PCI_08_PIC_ISA          0x01
#define AXP_PCI_08_PIC_EISA         0x02
#define AXP_PCI_08_PIC_APIC_1       0x10
#define AXP_PCI_08_PIC_APIC_2       0x20
#define AXP_PCI_08_DMA_8237         0x00
#define AXP_PCI_08_DMA_ISA          0x01
#define AXP_PCI_08_DMA_EISA         0x02
#define AXP_PCI_08_TIMER_8254       0x00
#define AXP_PCI_08_TIMER_ISA        0x01
#define AXP_PCI_08_TIMER_EISA       0x02
#define AXP_PCI_08_RTC_GENERIC      0x00
#define AXP_PCI_08_RTC_ISA          0x01

/*
 * Input
 */
#define AXP_PCI_09_SUB_KEYBOARD     0x00
#define AXP_PCI_09_SUB_PEN          0x01
#define AXP_PCI_09_SUB_MOUSE        0x02
#define AXP_PCI_09_SUB_SCANNER      0x03
#define AXP_PCI_09_SUB_GAMEPORT     0x04
#define AXP_PCI_09_SUB_OTHER        0x80
#define AXP_PCI_09_INTER            0x00
#define AXP_PCI_09_GAME_GENERIC     0x00
#define AXP_PCI_09_GAME_PROG        0x10

/*
 * Docking
 */
#define AXP_PCI_0A_SUB_GENERIC      0x00
#define AXP_PCI_0A_SUB_OTHER        0x80
#define AXP_PCI_0A_INTER            0x00

/*
 * Processor
 */
#define AXP_PCI_0B_SUB_386          0x00
#define AXP_PCI_0B_SUB_486          0x01
#define AXP_PCI_0B_SUB_PENTIUM      0x02
#define AXP_PCI_0B_SUB_ALPHA        0x10
#define AXP_PCI_0B_SUB_POWERPC      0x20
#define AXP_PCI_0B_SUB_MIPS         0x30
#define AXP_PCI_0B_SUB_COPROC       0x40
#define AXP_PCI_0B_INTER            0x00

/*
 * Serial Bus
 */
#define AXP_PCI_0C_SUB_FIREWIRE     0x00
#define AXP_PCI_0C_SUB_ACCESSBUS    0x01
#define AXP_PCI_0C_SUB_SSA          0x02
#define AXP_PCI_0C_SUB_USB          0x03
#define AXP_PCI_0C_SUB_FIBRECHAN    0x04
#define AXP_PCI_0C_SUB_SMBUS        0x05
#define AXP_PCI_0C_INTER            0x00
#define AXP_PCI_0C_FW_PRE_HCI       0x00
#define AXP_PCI_0C_FW_POST_HCI      0x10
#define AXP_PCI_0C_USB_USB          0x00
#define AXP_PCI_0C_USB_OPEN         0x10
#define AXP_PCI_0C_USB_NOSPEC       0x80
#define AXP_PCI_0C_USB_DEVICE       0xfe

/*
 * Wireless
 */
#define AXP_PCI_0D_SUB_IRDA         0x00
#define AXP_PCI_0D_SUB_IR           0x01
#define AXP_PCI_0D_SUB_RF           0x10
#define AXP_PCI_0D_SUB_OTHER        0x80
#define AXP_PCI_0D_INTER            0x00

/*
 * Intelligent I/O
 */
#define AXP_PCI_0E_SUB_IO           0x00
#define AXP_PCI_0E_INTER            0x00

/*
 * Satellite
 */
#define AXP_PCI_0F_SUB_TV           0x00
#define AXP_PCI_0F_SUB_AUDIO        0x01
#define AXP_PCI_0F_SUB_VOICE        0x02
#define AXP_PCI_0F_SUB_DATA         0x03
#define AXP_PCI_0F_INTER            0x00

/*
 * Encryption/Decryption
 */
#define AXP_PCI_10_SUB_NETWORK      0x00
#define AXP_PCI_10_SUB_ENTERTAIN    0x10
#define AXP_PCI_10_SUB_OTHER        0x80
#define AXP_PCI_10_INTER            0x00

/*
 * Data Acquisition and Signal Processing
 */
#define AXP_PCI_11_SUB_DPIO         0x00
#define AXP_PCI_11_SUB_OTHER        0x80
#define AXP_PCI_11_INTER            0x00

#endif /* _AXP_PCI_H_ */
