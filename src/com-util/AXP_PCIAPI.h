/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used to define the
 *  Application Programming Interface between the Digital Alpha AXP emulation
 *  software and any number of sharable objects containing code that implement
 *  a PCI device (or multiple devices).
 *
 * Revision History:
 *
 *  V01.000 13-Sep-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_PCIAPI_H_
#define _AXP_PCIAPI_H_

#include "com-util/AXP_Common_Include.h"
#include "com-util/AXP_PCI.h"

/*
 * The following enumeration and structure are used to provide device specific
 * information to the shareable object implementing the device.
 */
typedef enum
{
    Empty,
    FileName,
    Type,
    ReadOnly
} AXP_Arg_Type;
typedef struct
{
    AXP_Arg_Type type;
    union
    {
        char *name;
        bool readOnly;
        u32 devType;
    };
} AXP_PCI_Arg;

/*
 * PCI Device shareable object provides a single globally defined entry point,
 * initialize_device, which is called by the emulator to provide information
 * about where the emulator can call into the shared object for the PCI
 * specific function.  the emulator will initialize the the version information
 * to the highest supported version, the arguments to be supplied to the shared
 * object, and the PCI entry points to NULL.  The PCI shared object will set
 * the version information to the max it supports, but not greater than what is
 * supplied by the emulator, and the entry points it supports.  The emulator
 * will use this information when calling into the PCI device shared object.
 */
#define AXP_PCI_INITIALIZE_FUNC "initialize_device"
#define AXP_PCI_VER_TYPE 'V'
#define AXP_PCI_VER_MAJ 1
#define AXP_PCI_VER_MIN 0
#define AXP_PCI_VER_PAT 0
#define AXP_PCI_MAX_ARGS 16
typedef struct
{
    /*
     * Version information to indicate what version of the API is being used.
     */
    char version_type;
    u8 version_major;
    u8 version_minor;
    u8 version_patch;

    /*
     * Arguments to be provided to the initialize_device entry point in the
     * shared objects.
     */
    AXP_PCI_Arg args[AXP_PCI_MAX_ARGS];

    /*
     * PCI Functions supported by the device
     */
    u32 pciBaseAddressRegisters[AXP_NUM_BARS];
    u32 pciExpansionROMBAR;

    /*
     * PCI Entrypoints returned by the shared object.
     */
    u32 (*PCIConfigRead)(u16 func, u32 address, u32 dsize);
    void (*PCIConfigWrite)(u16 func, u32 address, u32 dsize, u32 data);
    void (*PCIReset)(void);
    u64 (*PCIMemRead)(u16 index, u64 address, u32 dsize);
    void (*PCIMemWrite)(u16 index, u64 address, u32 dsize, u64 data);
    u64 (*PCIBarRead)(u16 index, u64 address, u32 dsize);
    void (*PCIBarWrite)(u16 index, u64 address, u32 dsize, u64 data);
} AXP_PCIAPI_Device_CB;

#endif /* _AXP_PCIAPI_H_ */
