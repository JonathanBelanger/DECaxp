/*
 * Copyright (C) Jonathan D. Belanger 2017-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the function prototypes for the System interface.
 *
 * Revision History:
 *
 *  V01.000 31-Dec-2017 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.001 22-Dec-2019 Jonathan D. Belanger
 *  Reorganizing the code so that header files and source files are in the same
 *  directory.
 */
#ifndef _AXP_SYSTEM_DEFS_
#define _AXP_SYSTEM_DEFS_   1

#include "com-util/AXP_System_Common.h"
#include "com-util/AXP_SysData.h"
#include "sys-inter/AXP_21274_21264_Common.h"
#include "system/AXP_21274_Registers.h"
#include "d-chip/AXP_21274_Dchip.h"
#include "p-chip/AXP_21274_Pchip.h"
#include "memory/AXP_Memory.h"

/*
 * Unlike the other chips (Dchip and Pchip), the Cchip does not have a specific
 * structure defined for it.  The Cchip is pretty much the center of the
 * universe.  As such, it needs to have information about the entire system,
 * except devices hanging off the PCI buses.  When the system starts up, the
 * following structure is allocated and the Cchip started.  The Cchip is
 * responsible for collecting any initialization information that needs to be
 * provided to other parts of the system (Dchip, Pchip, and CPUs).  At reset,
 * the Cchip gets some required configuration information from the TIGbus, and
 * echos some of this information to the Dchip and Pchip.  This information is
 * as follows:
 *
 *  Table 12-1 Configuration Information
 *  ------------------------------------------------------------------------
 *  TIGbus Bit  Description
 *  ------------------------------------------------------------------------
 *  <7>         Module specific spare       (a fancy way to say unused)
 *  <6>         SysDc fill delay            (ignored in this implementation)
 *              ----------------
 *              Value   Cycles
 *              ----------------
 *              0       2 cycles
 *              1       3 cycles
 *              ----------------
 *  <5:4>       SysDc extract delay         (ignored in this implementation)
 *              ----------------
 *              Value   Cycles
 *              ----------------
 *              00      2 cycles
 *              01      3 cycles
 *              10      4 cycles
 *              11      5 cycles
 *              ----------------
 *  <3>         CPU 1 clock forward preset  (ignored in this implementation)
 *  <2>         CPU 0 clock forward preset  (ignored in this implementation)
 *  <1:0>       Base configuration
 *              --------------------------------
 *              Value   Meaning
 *              --------------------------------
 *              00      2 Dchips, 1 memory bus
 *              01      4 Dchips, 1 memory bus
 *              10      4 Dchips, 2 memory buses
 *              11      8 Dchips, 2 memory buses
 *  ------------------------------------------------------------------------
 *  NOTE: There is no mention, anywhere, of where the clock forward preset is
 *        to be specified for CPU 2 and CPU 3.  The best I can figure is that
 *        <2> is for CPU 0 and CPU 2, while <3> is for CPU 1 and CPU 3.
 *  NOTE: This implementation has only a single Dchip thread.  The CChip can
 *        supply an address to each of the Memory Arrays and request the Dchip
 *        to either supply or extract data from each array, such that there is
 *        only 1 outstanding address/data pair per memory array.  This will
 *        provide the look and feel of multiple Dchips and memory buses.
 */

/*
 * The following structure contains the information needed to be able to
 * communication with a single CPU.
 */
typedef struct
{
    pthread_mutex_t *mutex;
    pthread_cond_t *cond;
    AXP_21274_CBOX_PQ *pq;
    u8 *pqTop;
    u8 *pqBottom;
    u8 *irq_H;
} AXP_21274_CPU;

/*
 * The following are the states associated with the system.
 */
typedef enum
{
    SysPwrOn,
    SysReset,
    SysFault,
    SysNormal,
    SysSleep,
    SysPwrDown
} AXP_21274_States;

/*
 * HRM 2.1 System Building Block Variables
 *
 * The parameters that may be varied are as follows:
 *  - Number of CPUs (one or two) [sic] (there can be between one and four)
 *  - Number of memory data buses (one or two)
 *  - Number of Dchips (two, four, or eight)
 *  - Number of Pchips (one or two)
 *  - Number of main memory DRAM arrays (one, two, three, or four)
 *  - Width of the memory data buses (16 bytes or 32 bytes each)
 *  - Type of DRAM SIMMs (synchronous 16MB or 64MB, with various timing
 *    parameters)
 *
 * The combinations for possible system configurations are listed in Table 2?1.
 *
 *  Table 2-1 System Configurations
 *  --------------------------------------------------------------------------------------
 *                                      Pchip-to-
 *  Number of   Number of   Number of   Dchip Bus   Number  Number of       Memory Bus
 *  Cchips      Dchips      Pchips      Width       of CPUs Memory Buses    Width
 *  --------------------------------------------------------------------------------------
 *          1       2         1         4 bytes         1       1           16 bytes
 *          1       4       1 or 2      4 bytes     1 or 2    1(1)          32 bytes
 *          1       4       1 or 2      4 bytes     1 or 2    2(2)          16 bytes
 *          1       8       1 or 2      4 bytes     1 or 2  1 or 2(3)       32 bytes
 *          1       8       1 or 2      4 bytes        4    1 or 2(3)       32 bytes
 *  --------------------------------------------------------------------------------------
 *  (1) Preferable for uniprocessors.
 *  (2) Preferable for dual processors.
 *  (3) Two memory buses are recommended when using two or four CPUs.
 *
 * The following notes also apply to Table 2-1.
 *  - A 32-byte memory bus can be half-populated, in which case, it
 *    operates as a 16-byte memory bus. The difference is that the maximum
 *    number of arrays on the bus is still four.
 *  - Using SDRAMs and a system clock speed of 83 MHz, 16-byte memory buses
 *    each deliver 1.35-GB/s and 32-byte memory buses each deliver 2.7-GB/s
 *    effective bandwidth.
 *  - The data path from the CPU to the Dchip is always 8 bytes, and can
 *    run at 3ns using clock forwarding for an effective bandwidth of
 *    2.7-GB/s.
 *  - The PADbus (Pchip-to-Dchip) can run at 83 MHz for a raw bandwidth of
 *    400-MB/s, ignoring turnaround cycles.
 *  - In a system with eight Dchips, each Dchip transfers 1 check bit, but
 *    only ? byte per cycle. So the Pchip transfers 8 bytes with check bits
 *    every two cycles over a 40-wire interface.
 *  - In a system with eight Dchips, the Dchips support up to four CPUs,
 *    but the Cchip only supports one or two CPUs.
 *  - In a system with two memory buses, memory arrays 0 and 2 must be
 *    attached to bus 0, while memory arrays 1 and 3 must be attached to
 *    bus 1. The memory array number is determined by the set of DRAM
 *    control signals from the Cchip. The memory bus number is determined
 *    by the set of data signals on the Dchip slices (see Section 7.4).
 */

/*
 * System Data Structure.
 */
typedef struct
{

    /*************************************************************************
     * Cchip Data and Information                                            *
     *************************************************************************/
    pthread_t cChipThreadID;
    pthread_mutex_t cChipMutex;
    pthread_cond_t cChipCond;

    u32 cpuCount;
    AXP_21274_CPU cpu[AXP_COMMON_MAX_CPUS];
    AXP_21274_States state;

    /*
     * Input queues for messages arriving from the CPUs.  A CPU queues its
     * message (request or response) into the sysDc queue.  The Cchip removes
     * messages from these queues with the following alternation modes:
     *
     * Tsunami:
     *  CPU 0 alternate with CPU 1
     *
     * Typhoon:
     *  CPU 0 or CPU 1 alternate with CPU 2 or CPU 3
     *  CPU 0 or CPU 2 alternate with CPU 1 or CPU 3
     *  CPU 0 or CPU 3 alternate with CPU 1 or CPU 2
     *
     * What this means is that the Cchip will not always dequeue a request from
     * just a single or preferred set of CPUs, but will process requests from
     * each equitably.
     */
    AXP_QUEUE_HDR sysDc[AXP_COMMON_MAX_CPUS];
    AXP_COUNTED_QUEUE cpuSkidBuffer[AXP_COMMON_MAX_CPUS];   /* 4 entries/CPU */

    /*
     * Input queues for messages arriving from the Pchips.  A Pchip queues its
     * message (request or response) into the CAP queue.
     */
    AXP_QUEUE_HDR CAPq[AXP_COMMON_MAX_PCHIPS];
    AXP_COUNTED_QUEUE capbusSkidBuffers;                    /* 4 entries */

    AXP_COUNTED_QUEUE memoryArrayRq[4];                     /* 6 entries/queue */
    AXP_QUEUE_HDR waitQueue[AXP_COMMON_MAX_PCHIPS];

    /*
     * Cchip Registers
     */
    AXP_21274_CSC csc;              /* Address: 801.a000.0000 */
    AXP_21274_MTR mtr;              /* Address: 801.a000.0040 */
    AXP_21274_MISC misc;            /* Address: 801.a000.0080 */
    AXP_21274_MPD mpd;              /* Address: 801.a000.00c0 */
    AXP_21274_AARx aar0;            /* Address: 801.a000.0100 */
    AXP_21274_AARx aar1;            /* Address: 801.a000.0140 */
    AXP_21274_AARx aar2;            /* Address: 801.a000.0180 */
    AXP_21274_AARx aar3;            /* Address: 801.a000.01c0 */
    AXP_21274_DIMn dim0;            /* Address: 801.a000.0200 */
    AXP_21274_DIMn dim1;            /* Address: 801.a000.0240 */
    AXP_21274_DIRn dir0;            /* Address: 801.a000.0280 */
    AXP_21274_DIRn dir1;            /* Address: 801.a000.02c0 */
    AXP_21274_DRIR drir;            /* Address: 801.a000.0300 */
    AXP_21274_PRBEN prbEn;          /* Address: 801.a000.0340 */
    AXP_21274_IICn iic0;            /* Address: 801.a000.0380 */
    AXP_21274_IICn iic1;            /* Address: 801.a000.03c0 */
    AXP_21274_MPRn mpr0;            /* Address: 801.a000.0400 */
    AXP_21274_MPRn mpr1;            /* Address: 801.a000.0440 */
    AXP_21274_MPRn mpr2;            /* Address: 801.a000.0480 */
    AXP_21274_MPRn mpr3;            /* Address: 801.a000.04c0 */
    AXP_21274_TTR ttr;              /* Address: 801.a000.0580 */
    AXP_21274_TDR tdr;              /* Address: 801.a000.05c0 */
    AXP_21274_DIMn dim2;            /* Address: 801.a000.0600 */
    AXP_21274_DIMn dim3;            /* Address: 801.a000.0640 */
    AXP_21274_DIRn dir2;            /* Address: 801.a000.0680 */
    AXP_21274_DIRn dir3;            /* Address: 801.a000.06c0 */
    AXP_21274_IICn iic2;            /* Address: 801.a000.0700 */
    AXP_21274_IICn iic3;            /* Address: 801.a000.0740 */
    AXP_21274_PWR pwr;              /* Address: 801.a000.0780 */
    AXP_21274_CMONCTLA cmonctla;    /* Address" 801.a000.0c00 */
    AXP_21274_CMONCTLB cmonctlb;    /* Address" 801.a000.0c40 */
    AXP_21274_CMONCNT01 cmoncnt01;  /* Address" 801.a000.0c80 */
    AXP_21274_CMONCNT23 cmoncnt23;  /* Address" 801.a000.0cc0 */

    /*************************************************************************
     * Dchip Data and Information                                            *
     *************************************************************************/
    AXP_21274_DCHIP dChip;

    /*************************************************************************
     * Pchip Data and Information                                            *
     *************************************************************************/
    AXP_21274_PCHIP pChip[AXP_COMMON_MAX_PCHIPS];

    /*
     * Memory Arrays.
     */
    AXP_21274_Memory memory;
} AXP_21274_SYSTEM;

#endif  /* _AXP_SYSTEM_DEFS_ */
