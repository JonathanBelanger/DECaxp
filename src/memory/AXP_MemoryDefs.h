/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains definitions needed to implement the physical
 *  memory characteristic for an Alpha 21264 System using either the
 *  Tsunami (21272) or Typhoon (21274) Chip Set.
 *
 * Revision History:
 *
 *  V01.000 04-Jan-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _MEMORY_AXP_MEMORY_DEFS_H_
#define _MEMORY_AXP_MEMORY_DEFS_H_

#include "system/AXP_21274_System.h"

/*
 * Function prototypes
 */
int
AXP_21274_Addr2Array(AXP_21274_SYSTEM *sys, u64 address);

bool
AXP_21274_Memory_Init(AXP_21274_SYSTEM *sys);

void
AXP_21274_Memory_Rq(AXP_21274_SYSTEM *sys, AXP_21274_Mem_Op rq,
                    u64 address, u64*id);

bool
AXP_21274_Memory_Data_Ready(AXP_21274_SYSTEM *sys, u64 address);

void
AXP_21274_Memory_Buffer_Ready(AXP_21274_SYSTEM *sys, u64 address);

void
AXP_21274_Get_Memory_Size(AXP_21274_SYSTEM *sys, u64 *memSize, u32 *arrayCount);

void
AXP_21274_Get_Array_Info(AXP_21274_SYSTEM *sys, int arrIdx, u64 *arraySize,
                         u64 *baseAddress, u32 *rows, u32 *banks, u8 *split,
                         u8* twiceSplit);

#endif /* _MEMORY_AXP_MEMORY_DEFS_H_ */
