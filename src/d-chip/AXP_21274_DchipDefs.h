/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions required for the Tsunami/Typhoon
 *  Dchip emulation.
 *
 * Revision History:
 *
 *  V01.000 04-Jan-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_21274_DCHIP_DEFS_H_
#define _AXP_21274_DCHIP_DEFS_H_

#include "d-chip/AXP_21274_Dchip.h"
#include "system/AXP_21274_System.h"

/*
 * Interface prototypes
 */
#if 0
void
AXP_21274_Dchip_Init(AXP_21274_SYSTEM *sys);

void
AXP_21274_Dchip_CPM(AXP_21274_SYSTEM *sys, AXP_CPM_Command cmd, u8 cpu, u8 mem,
                    u8 csr, u64 *id);

void
AXP_21274_Dchip_PAD(AXP_21274_SYSTEM *sys, AXP_PAD_Command cmd, u8 pchip,
                    AXP_PAD_Shift shift, AXP_PAD_Length len, u64 id);
#endif
#endif /* _AXP_21274_DCHIP_DEFS_H_ */
