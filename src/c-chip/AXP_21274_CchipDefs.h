/*
 * Copyright (C) Jonathan D. Belanger 2018-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions required for the Tsunami/Typhoon
 *  Cchip emulation.
 *
 * Revision History:
 *
 *  V01.000     18-Mar-2018 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.002 22-Dec-2019 Jonathan D. Belanger
 *  Reorganizing the code so that header files and source files are in the same
 *  directory.
 */
#ifndef _AXP_21274_CCHIP_DEFS_H_
#define _AXP_21274_CCHIP_DEFS_H_

#include "c-chip/AXP_21274_Cchip.h"
#include "system/AXP_21274_System.h"

/*
 * Cchip Function Prototypes
 */
void
AXP_21274_Cchip_Init(AXP_21274_SYSTEM *sys);

void*
AXP_21274_CchipMain(void*);

#endif /* _AXP_21274_CCHIP_DEFS_H_ */
