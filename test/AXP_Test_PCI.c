/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This source file contains test code used to test the PCI interface of a
 *  loaded shareable object used in the Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 26-Sep-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include "com-util/AXP_PCIAPI.h"
#include <dlfcn.h>

/*
 * This is the main function called with any and all supplied arguments, which
 * are used to load and call a shareable object that contains the code to
 * implement one or more PCI devices.
 *
 * Input Parameters:
 *  argc:
 *      An integer containing the number of arguments specified in the argument
 *      vector.
 *  argv:
 *      An array containing pointers to the arguments specified on the command
 *      line, plus the name of the image executing.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  EXIT_SUCCESS:   If everything ran according to plan.
 *  EXIT_FAILURE:   If something failed along the way.
 */
int main(int argc, char *argv[])
{
    AXP_PCIAPI_Device_CB pci_entrypoints;
    void *so_handle;
    int (*pci_device_init)(AXP_PCIAPI_Device_CB *);
    int ii = 0;
    int retVal = EXIT_SUCCESS;

    /*
     * Load the shareable object containing the PCI device(s) implementation(s).
     */
    if (argc > 1)
    {
        so_handle = dlopen(argv[1], RTLD_LAZY);
    }
    else
    {
        so_handle = NULL;
    }
    if (so_handle != NULL)
    {

        /*
         * Get the initialization function address from the definitions within
         * the shareable object.  There should only be one and all other symbols
         * suppressed.
         */
        *(void **)(&pci_device_init) = dlsym(so_handle,
                                             AXP_PCI_INITIALIZE_FUNC);

        /*
         * If we got the address of the PCI Initialization function, then
         * initialize the entrypoints structure and call the initialize function
         * to return the list of devices being implemented and the entrypoints
         * implementing the PCI functions.
         */
        if (pci_device_init != NULL)
        {

            /*
             * Initialize the structured used to get information out of the
             * PCI device implementation code.
             */
            pci_entrypoints.version_type = AXP_PCI_VER_TYPE;
            pci_entrypoints.version_major = AXP_PCI_VER_MAJ;
            pci_entrypoints.version_minor = AXP_PCI_VER_MIN;
            pci_entrypoints.version_patch = AXP_PCI_VER_PAT;
            memset(pci_entrypoints.args, 0,
                   sizeof(AXP_PCI_Arg) * AXP_PCI_MAX_ARGS);
            pci_entrypoints.args[ii].type = FileName;
            pci_entrypoints.args[ii++].name = "cdrom.iso";
            pci_entrypoints.args[ii].type = Type;
            pci_entrypoints.args[ii++].devType = 42;
            pci_entrypoints.args[ii].type = ReadOnly;
            pci_entrypoints.args[ii++].readOnly = false;

            retVal = pci_device_init(&pci_entrypoints);

            /*
             * If the function call returned successfully, then we got the
             * information we requested.  Otherwise, we got a failure.
             */
            if (retVal == EXIT_SUCCESS)
            {
                printf("Successfully got back the PCI entrypoints\n");
                for (ii = 0; ii < AXP_PCI_MAX_FUNCTIONS; ii++)
                {
                    printf("    BAR[%d]: %u\n", ii,
                           pci_entrypoints.pciBaseAddressRegisters[ii]);
                }
                printf("    ROM BAR: %u\n", ii,
                       pci_entrypoints.pciExpansionROMBAR);
                printf("    PCI Configuration Read: ");
                if (pci_entrypoints.PCIConfigRead != NULL)
                {
                    pci_entrypoints.PCIConfigRead(0, 42, 8);
                }
                else
                {
                    printf("Not present\n");
                }
                printf("    PCI Configuration Write: ");
                if (pci_entrypoints.PCIConfigWrite != NULL)
                {
                    pci_entrypoints.PCIConfigWrite(1, 0, 8, 0xdeadbeef);
                }
                else
                {
                    printf("Not present\n");
                }
                printf("    PCI Memory Read: ");
                if (pci_entrypoints.PCIMemRead != NULL)
                {
                    pci_entrypoints.PCIMemRead(2, 0x7ffe, 2);
                }
                else
                {
                    printf("Not present\n");
                }
                printf("    PCI Memory Write: ");
                if (pci_entrypoints.PCIMemWrite != NULL)
                {
                    pci_entrypoints.PCIMemWrite(3, 0x80087355, 8, 0x0dadbe51);
                }
                else
                {
                    printf("Not present\n");
                }
                printf("    PCI I/O Read: ");
                if (pci_entrypoints.PCIIORead != NULL)
                {
                    pci_entrypoints.PCIIORead(4, 21, 4);
                }
                else
                {
                    printf("Not present\n");
                }
                printf("    PCI I/O Write: ");
                if (pci_entrypoints.PCIIOWrite != NULL)
                {
                    pci_entrypoints.PCIIOWrite(5, 21, 4, 0);
                }
                else
                {
                    printf("Not present\n");
                }
                printf("    PCI Reset: ");
                if (pci_entrypoints.PCIReset != NULL)
                {
                    pci_entrypoints.PCIReset();
                }
                else
                {
                    printf("Not present\n");
                }
            }
            else
            {
                printf("Failed to get back the PCI entrypoints\n");
            }
        }
    }
    else
    {
        retVal = EXIT_FAILURE;
    }

    /*
     * Return back to the caller with a success or failure indication.
     */
    return (retVal);
}
