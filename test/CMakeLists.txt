#
# Copyright (C) Jonathan D. Belanger 2019-2020.
# All Rights Reserved.
#
# This software is furnished under a license and may be used and copied only
# in accordance with the terms of such license and with the inclusion of the
# above copyright notice.  This software or any other copies thereof may not
# be provided or otherwise made available to any other person.  No title to
# and ownership of the software is hereby transferred.
#
# The information in this software is subject to change without notice and
# should not be construed as a commitment by the author or co-authors.
#
# The author and any co-authors assume no responsibility for the use or
# reliability of this software.
#
# Description:
#
#   This CMake file is used to build the test executables for the DECaxp
#   project.
#
# Revision History:
#   V01.000 28-Apr-2019 Jonathan D. Belanger
#   Initially written, based off of the original Makefile..
#
#   V01.001 09-Jun-2019 Jonathan D. Belanger
#   Added a define to the compile flags to specify the path the the directory
#   containing the test data.
#
#	V01.002	22-Dec-2019	Jonathan D. Belanger
#	Changing the code layout so that includes are in the same folder as the
#	source file associated with them.
#
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DAXP_TEST_DATA_FILES=\\\"${CMAKE_CURRENT_SOURCE_DIR}/dat\\\"")

add_executable(AXP_21264_Cache_Test
    AXP_21264_Cache_Test.c)

target_include_directories(AXP_21264_Cache_Test PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

target_link_libraries(AXP_21264_Cache_Test PRIVATE
    cache
    cbox
    ibox
    mbox
    ebox
    fbox
    comutl
    ethernet
    sysinter
    -lxml2
    -lm
    -lpthread
    -lpcap
    ${compiler-rt})

add_executable(AXP_21264_Dump_Test
    AXP_21264_Dump_Test.c)

target_link_libraries(AXP_21264_Dump_Test PRIVATE
    comutl
    ethernet
    -lxml2
    -lm
    -lpthread
    -lpcap)

target_include_directories(AXP_21264_Dump_Test PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

add_executable(AXP_21264_FloatingPointTest_Test
    AXP_21264_FloatingPointTest.c)

target_link_libraries(AXP_21264_FloatingPointTest_Test PRIVATE
    fbox
    comutl
    ethernet
    -lxml2
    -lm
    -lpthread
    -lpcap)

target_include_directories(AXP_21264_FloatingPointTest_Test PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

add_executable(AXP_21264_Icache_Test
    AXP_21264_Icache_Test.c)

target_link_libraries(AXP_21264_Icache_Test PRIVATE
    cache
    cbox
    ibox
    mbox
    ebox
    fbox
    sysinter
    comutl
    ethernet
    -lxml2
    -lm
    -lpthread
    -lpcap
    ${compiler-rt})

target_include_directories(AXP_21264_Icache_Test PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

add_executable(AXP_21264_IntegerLoadTest
    AXP_21264_IntegerLoadTest.c)

target_include_directories(AXP_21264_IntegerLoadTest PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

target_link_libraries(AXP_21264_IntegerLoadTest PRIVATE
    ebox
    mbox
    cbox
    ibox
    ebox
    fbox
    sysinter
    cache
    comutl
    ethernet
    -lxml2
    -lm
    -lpthread
    -lpcap
    ${compiler-rt})

add_executable(AXP_21264_Prediction_Test
    AXP_21264_Prediction_Test.c)

target_link_libraries(AXP_21264_Prediction_Test PRIVATE
    ibox
    comutl
    ethernet
    -lxml2
    -lm
    -lpthread
    -lpcap)

target_include_directories(AXP_21264_Prediction_Test PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

add_executable(AXP_Disk_Test
    AXP_Disk_Test.c)

target_include_directories(AXP_Disk_Test PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

target_link_libraries(AXP_Disk_Test PRIVATE
    vhd
    comutl
    ethernet
    -lxml2
    -luuid
    -lm
    -lpthread
    -lpcap)

add_executable(AXP_DS12887A_Test
    AXP_DS12887A_Test.c)

target_include_directories(AXP_DS12887A_Test PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

target_link_libraries(AXP_DS12887A_Test PRIVATE
    toyclock
    comutl
    ethernet
    -lxml2
    -lm
    -lpthread
    -lpcap
    -lrt)

add_executable(AXP_Test_Telnet
    AXP_Test_Telnet.c)

target_include_directories(AXP_Test_Telnet PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

target_link_libraries(AXP_Test_Telnet PRIVATE
    ali-m1543c
    comutl
    ethernet
    -lxml2
    -lm
    -lpthread
    -lpcap)

add_executable(AXP_Test_Queues
    AXP_Test_Queues.c)

target_include_directories(AXP_Test_Queues PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

add_executable(AXP_Test_Structure_Sizes
    AXP_Test_Structure_Sizes.c)

target_include_directories(AXP_Test_Structure_Sizes PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

add_executable(AXP_Test_Npcap
    AXP_Test_Npcap.c)

target_link_libraries(AXP_Test_Npcap PRIVATE
    -lpcap)

target_include_directories(AXP_Test_Npcap PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

add_executable(AXP_21274_Memory_Test
    AXP_21274_Memory_Test.c)

target_link_libraries(AXP_21274_Memory_Test PRIVATE
    memory
    comutl
    ethernet
    -lxml2
    -lpthread
    -lpcap)

target_include_directories(AXP_21274_Memory_Test PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

add_executable(AXP_Test_SysData
    AXP_Test_SysData.c)

target_link_libraries(AXP_Test_SysData PRIVATE
    comutl
    ethernet
    -lxml2
    -lm
    -lpthread
    -lpcap)

target_include_directories(AXP_Test_SysData PRIVATE
    ${PROJECT_SOURCE_DIR}/src)

add_executable(AXP_Test_PCI
    AXP_Test_PCI.c)

target_link_libraries(AXP_Test_PCI PRIVATE
    -ldl)

target_include_directories(AXP_Test_PCI PRIVATE
    ${PROJECT_SOURCE_DIR}/src)
